<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_informations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->default(0);
            $table->integer('user_id')->default(0);
            $table->double('overallarea')->default(0);
            $table->string('type_of_building', 200)->nullable();
            $table->string('airport', 200)->nullable();
            $table->string('hospitals', 200)->nullable();
            $table->string('fire_services', 200)->nullable();
            $table->string('slums', 200)->nullable();
            $table->string('industrial', 200)->nullable();
            $table->string('railway_tracks', 200)->nullable();
            $table->string('distance_fm_mainroad', 200)->nullable();
            $table->string('build_config', 200)->nullable();
            $table->string('reception_hours', 200)->nullable();
            $table->integer('no_of_lifts')->default(0);
            $table->integer('capasity_of_lifts')->default(0);
            $table->string('make_of_lifts', 200)->nullable();
            $table->string('amenities_avilable', 200)->nullable();
            $table->string('seismic_standard', 200)->nullable();
            $table->string('structure_type', 200)->nullable();
            $table->string('slabs_design', 200)->nullable();
            $table->string('floor_height', 200)->nullable();
            $table->integer('food_court_seating')->default(0);
            $table->string('food_court_working_hours', 200)->nullable();
            $table->string('management_operational_hours', 200)->nullable();
            $table->string('bms_system', 200)->nullable();
            $table->string('high_side_equipments', 200)->nullable();
            $table->string('security_surveillance', 200)->nullable();
            $table->string('cctv_installation', 200)->nullable();
            $table->string('demand_load', 200)->nullable();
            $table->string('load_calculation', 200)->nullable();
            $table->string('transformer_feeding', 200)->nullable();
            $table->string('ventilation_transformer', 200)->nullable();
            $table->string('dg_power', 200)->nullable();
            $table->string('generators', 200)->nullable();
            $table->string('dg_kick_off_time', 200)->nullable();
            $table->string('earthing_pits', 200)->nullable();
            $table->string('lift_ac', 200)->nullable();
            $table->string('facade_glass', 200)->nullable();
            $table->string('chillers_configuration', 200)->nullable();
            $table->string('chiller_timing', 200)->nullable();
            $table->string('water_supply', 200)->nullable();
            $table->string('floor_capacity', 200)->nullable();
            $table->string('state_design_condition', 200)->nullable();
            $table->string('ventilation_operational_hours', 200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_informations');
    }
}
