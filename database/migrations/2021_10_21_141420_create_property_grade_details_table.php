<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyGradeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_grade_details', function (Blueprint $table) {
            $table->increments('id');
            $table->double('area_building')->default(0);
            $table->string('type_of_building')->nullable();
            $table->string('airport')->nullable();
            $table->string('hospital')->nullable();
            $table->string('fireservice')->nullable();
            $table->string('slums')->nullable();
            $table->string('industrial')->nullable();
            $table->string('railway_tracks')->nullable();
            $table->string('standoff_distance')->nullable();
            $table->string('building_configuration')->nullable();
            $table->string('operation_hours')->nullable();
            $table->string('lifts')->nullable();
            $table->string('lift_capacity')->nullable();
            $table->string('lift_make')->nullable();
            $table->string('amenities_available')->nullable();
            $table->string('seismic_standard')->nullable();
            $table->string('type_of_structure')->nullable();
            $table->string('load_slabs')->nullable();
            $table->string('ceiling_height')->nullable();
            $table->string('footcourt_seating_capacity')->nullable();
            $table->string('foodcourt_opening_hourse')->nullable();
            $table->boolean('parking_floors')->default(0);
            $table->boolean('differently_abled')->default(0);
            $table->boolean('parking_washroom')->default(0);
            $table->string('monitoring_system')->nullable();
            $table->string('data_storage')->nullable();
            $table->string('equipments_control')->nullable();
            $table->string('security_surveillance')->nullable();
            $table->boolean('vistors_management')->default(0);
            $table->string('cctv_camera_installed')->nullable();
            $table->string('access_control_system')->nullable();
            $table->boolean('dedicated_security_control')->default(0);
            $table->boolean('fire_emergencies_lifts')->default(0);
            $table->boolean('fire_exists_space')->default(0);
            $table->string('demand_load')->nullable();
            $table->string('load_calculation')->nullable();
            $table->string('capacity_of_transformation')->nullable();
            $table->string('ventilation_room')->nullable();
            $table->string('DG_power_backup')->nullable();
            $table->string('generators_feeding')->nullable();
            $table->string('DG_kick_off_time')->nullable();
            $table->string('earthing_pits')->nullable();
            $table->string('hvac_system')->nullable();
            $table->string('ac_system')->nullable();
            $table->string('glass_installed')->nullable();
            $table->string('pumps_model')->nullable();
            $table->string('chiller_timing')->nullable();
            $table->string('chilled_water_supply')->nullable();
            $table->string('csu_floor_capacity')->nullable();
            $table->string('internl_office_space')->nullable();
            $table->string('ventilation_system_operational_hours')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_grade_details');
    }
}
