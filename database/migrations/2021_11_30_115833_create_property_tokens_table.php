<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_tokens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->default(0);
            $table->integer('user_id')->default(0);
            $table->string('token_name', 200)->nullable();
            $table->string('token_symbol', 200)->nullable();
            $table->double('token_value')->default(0);
            $table->string('token_supply', 200)->default(0);
            $table->integer('token_decimal')->default(0);
            $table->string('token_image')->nullable();
            $table->boolean('deploy_status')->default(0);
            $table->string('contract_address')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_tokens');
    }
}
