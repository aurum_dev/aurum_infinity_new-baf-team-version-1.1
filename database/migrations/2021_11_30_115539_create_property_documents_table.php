<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->default(0);
            $table->integer('user_id')->default(0);
            $table->string('prospectus')->nullable();
            $table->string('title_report')->nullable();
            $table->string('subscription')->nullable();
            $table->string('due_diligence')->nullable();
            $table->string('updates')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_documents');
    }
}
