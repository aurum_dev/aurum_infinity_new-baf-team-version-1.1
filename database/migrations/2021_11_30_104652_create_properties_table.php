<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->default(0);
            $table->string('propertyName', 200)->nullable();
            $table->string('propertyLocation', 200)->nullable();
            $table->string('propertyLogo', 200)->nullable();           
            $table->string('propertyType', 200)->nullable();
            $table->double('rera_number')->default(0);
            $table->string('dev_name', 200)->nullable();
            $table->integer('property_age')->default(0);
            $table->string('tenant_name', 200)->nullable();
            $table->float('rent_escalation')->default(0);
            $table->integer('notice_period')->default(0);
            $table->double('carpet_area')->default(0);
            $table->double('built_up_area')->default(0);
            $table->float('gross_yield')->default(0);
            $table->double('annual_rent')->default(0);
            $table->double('totalDealSize')->default(0);
            $table->double('expectedIrr')->default(0);
            $table->double('initialInvestment')->default(0);
            $table->float('propertyEquityMultiple')->default(0);
            $table->float('holdingPeriod')->default(0);
            $table->double('total_sft')->default(0);
            $table->boolean('feature')->default(0);
            $table->float('dividend')->default(0);
            $table->enum('status', ['pending', 'live', 'soldout']);
            $table->timestamps();*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
