<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->integer('leased_period')->nullable()->after('propertyType');
            $table->string('rera_number')->nullable()->after('leased_period');
            $table->string('dev_name')->nullable()->after('rera_number');
            $table->integer('property_age')->nullable()->after('dev_name');
            $table->enum('property_status',['ONGOING','COMPLETED'])->nullable()->after('property_age');
            $table->string('tenant_name')->nullable()->after('property_status');
            $table->double('rent_escalation')->default(0)->after('tenant_name');
            $table->integer('notice_period')->nullable()->after('rent_escalation');
            $table->double('carpet_area')->default(0)->after('notice_period');
            $table->double('built_up_area')->default(0)->after('carpet_area');
            $table->double('gross_yield')->default(0)->after('built_up_area');
            $table->double('annual_rent')->default(0)->after('gross_yield');

            $table->string('geo_location')->nullable()->after('propertyLocationOverview');
            $table->string('street_address')->nullable()->after('geo_location');
            $table->string('floor_number')->nullable()->after('street_address');
            $table->string('area_name')->nullable()->after('floor_number');
            $table->string('pincode')->nullable()->after('area_name');
            
            $table->string('due_diligence_report')->nullable()->after('termsheet');

            $table->string('facilities')->nullable()->after('propertyParking');
            $table->integer('facilitation_fees')->default(0)->after('facilities');
            $table->double('psf_rate')->default(0)->after('facilitation_fees');
            $table->string('custodian')->nullable()->after('psf_rate');
            $table->double('registration_fees')->default(0)->after('custodian');
            $table->double('property_gross_yield')->default(0)->after('registration_fees');
            $table->double('total_cost_psf')->default(0)->after('property_gross_yield');
            $table->double('net_operating_yield_sale')->default(0)->after('total_cost_psf');
            $table->double('net_realizable_yield_sale')->default(0)->after('net_operating_yield_sale');
            $table->double('net_realizable_yield_purchase')->default(0)->after('net_realizable_yield_sale');
            $table->integer('stamp_duty')->default(0)->after('net_realizable_yield_purchase');
            $table->integer('sale_price')->default(0)->after('stamp_duty');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->dropColumn('leased_period');
            $table->dropColumn('rera_number');
            $table->dropColumn('dev_name');
            $table->dropColumn('property_age');
            $table->dropColumn('property_status');
            $table->dropColumn('tenant_name');
            $table->dropColumn('rent_escalation');
            $table->dropColumn('notice_period');
            $table->dropColumn('carpet_area');
            $table->dropColumn('built_up_area');
            $table->dropColumn('gross_yield');
            $table->dropColumn('annual_rent');

            $table->dropColumn('geo_location');
            $table->dropColumn('street_address');
            $table->dropColumn('floor_number');
            $table->dropColumn('area_name');
            $table->dropColumn('pincode');

            $table->dropColumn('due_diligence_report');
        });
    }
}
