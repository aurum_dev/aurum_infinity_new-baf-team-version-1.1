@extends('issuer.layout.base')
@section('content')

    <style>
        #landmark_remove_2, #landmark_remove_3, #landmark_remove_4, #landmark_remove_5, #landmark_remove_6, #landmark_remove_7, #landmark_remove_8 {
            margin-top: 38px !important;
        }
        .error {
            color: red;
        }
    </style>

    <div class="content-page">

        <!-- Header Banner Start -->
        <div class="header-breadcrumbs">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6"><h1>@lang('admin.tokencreate')</h1></div>
                    <div class="col-sm-6">
                        <div class="breadcrumb-four" style="text-align: right;">
                            <ul class="breadcrumb">
                                <li><a href="{{url('/dashboard')}}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-box">
                                            <path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path>
                                            <polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline>
                                            <line x1="12" y1="22.08" x2="12" y2="12"></line>
                                        </svg>
                                        <span>@lang('user.dashboard')</span></a></li>
                                <li class="active"><a href="javscript:void(0);">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-cpu">
                                            <rect x="4" y="4" width="16" height="16" rx="2" ry="2"></rect>
                                            <rect x="9" y="9" width="6" height="6"></rect>
                                            <line x1="9" y1="1" x2="9" y2="4"></line>
                                            <line x1="15" y1="1" x2="15" y2="4"></line>
                                            <line x1="9" y1="20" x2="9" y2="23"></line>
                                            <line x1="15" y1="20" x2="15" y2="23"></line>
                                            <line x1="20" y1="9" x2="23" y2="9"></line>
                                            <line x1="20" y1="14" x2="23" y2="14"></line>
                                            <line x1="1" y1="9" x2="4" y2="9"></line>
                                            <line x1="1" y1="14" x2="4" y2="14"></line>
                                        </svg>
                                        <span>@lang('admin.tokencreate')</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header Banner Start -->


        <div class="content">
            <!-- Start container-fluid -->
            <div class="container-fluid wizard-border">
                <!-- start  -->
                <h5>@lang('admin.fund.create_fund')</h5>
                <form class="form-horizontal" id="property-create" action="{{route('propertyStore')}}" method="POST" enctype="multipart/form-data" role="form">
                    @csrf
                    <input type="hidden" name="token_type" value="2">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="propertyName">@lang('admin.fund.create_fund')<span class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="propertyName" required="" id="propertyName" placeholder="@lang('admin.enter') @lang('admin.fund.create_fund')" maxlength="200">
                            </div>
                        </div>
                        {{--                        <div class="col-md-3">--}}
                        {{--                            <div class="form-group">--}}
                        {{--                                <label for="propertyLocation">@lang('admin.property.propertylocation')<span class="text-danger">*</span></label>--}}
                        {{--                                <input class="form-control" type="text" name="propertyLocation" required id="propertyLocation" placeholder="@lang('admin.enter') @lang('admin.property.propertylocation')" maxlength="200">--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="propertyLogo">@lang('admin.fund.fund_logo')<span class="text-danger">*</span></label>
                                <input class="form-control" type="file" name="propertyLogo" accept="image/png,image/jpeg" required id="propertyLogo" placeholder="@lang('admin.fund.fund_logo')">
                            </div>
                        </div>
                        {{--                        <div class="col-sm-3">--}}
                        {{--                            <div class="form-group">--}}
                        {{--                                <label for="propertyType">@lang('admin.property.type')<span class="text-danger">*</span></label>--}}
                        {{--                                <select class="form-control" name="propertyType" id="propertyType" required>--}}
                        {{--                                    <option value="">Select</option>--}}
                        {{--                                    @foreach ($assetType as $value)--}}
                        {{--                                        <option value="{{ @$value->type }}">{{ @$value->type }}</option>--}}
                        {{--                                    @endforeach--}}
                        {{--                                </select>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="totalDealSize">@lang('admin.totaldealsize')<span class="text-danger">*</span></label>
                                <input class="form-control" type="number" name="totalDealSize" required id="totalDealSize" placeholder="@lang('admin.placeholders.total_deal_size')" min="0">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="expectedIrr">@lang('admin.inputs.expected irr')</label>
                                <input class="form-control" type="number" name="expectedIrr" id="expectedIrr" placeholder="@lang('admin.placeholders.expected irr')" min="0">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="fundedMembers">@lang('admin.fund.fundedMembers')</label>
                                <input class="form-control" type="number" name="fundedMembers" id="fundedMembers" placeholder="Enter Funded Members" min="0">
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="initialInvestment">@lang('admin.fund.min_investment')<span class="text-danger">*</span></label>
                                <input class="form-control" type="number" name="initialInvestment" required id="initialInvestment" placeholder="@lang('admin.enter') @lang('admin.fund.min_investment')" min="0">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="propertyEquityMultiple">@lang('admin.inputs.property_equity_multiple')</label>
                                <input class="form-control" type="number" name="propertyEquityMultiple" id="propertyEquityMultiple" placeholder="@lang('admin.placeholders.property_equity_multiple')" step="any" min="0">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group ">
                                <label for="holdingPeriod">@lang('admin.inputs.total_holding_period')<span class="text-danger">*</span></label>
                                <input class="form-control" type="number" name="holdingPeriod" required id="holdingPeriod" placeholder="@lang('admin.placeholders.total_holding_period')" min="0">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="propertyVideo">Insert Video File</label>
                                <input class="form-control" type="file" name="propertyVideo" accept="video/mp4,video/x-m4v,video/*" id="propertyVideo" placeholder="Enter Property Logo">
                            </div>
                        </div>
                        {{--                        <div class="col-md-3">--}}
                        {{--                            <div class="form-group">--}}
                        {{--                                <label for="total_sft">@lang('admin.inputs.total_sft')<span class="text-danger">*</span></label>--}}
                        {{--                                <input class="form-control" type="number" name="total_sft" required id="total_sft" placeholder="@lang('admin.placeholders.total_sft')" step="any" min="0">--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                    </div>
                    <h5>@lang('admin.inputs.overview_details')</h5>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="propertyOverview">@lang('admin.fund.detail_overview')</label>
                                <textarea class="form-control" type="text" name="propertyOverview" required id="propertyOverview" placeholder="@lang('admin.enter') @lang('admin.fund.detail_overview')"></textarea>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="propertyHighlights">@lang('admin.fund.detail_highlights')</label>
                                <textarea class="form-control" type="text" name="propertyHighlights" required id="propertyHighlights" placeholder="@lang('admin.enter') @lang('admin.fund.detail_highlights')"></textarea>
                            </div>
                        </div>
                    </div>
                    {{--                    <h5>@lang('admin.location_details')</h5>--}}
                    {{--                    <div class="row">--}}
                    {{--                        <div class="col-md-3">--}}
                    {{--                            <div class="form-group">--}}
                    {{--                                <label for="propertyLocationOverview">@lang('admin.inputs.location_overview')</label>--}}
                    {{--                                <textarea class="form-control" type="text" name="propertyLocationOverview" required id="propertyLocationOverview" placeholder="Enter Property Location Overview"></textarea>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    {{--                    <h5>@lang('admin.comparables')</h5>--}}
                    {{--                    <div class="row">--}}
                    {{--                        <div class="" id="comparables_section">--}}
                    {{--                            <div id="comparables_block_1" class="form-group row">--}}
                    {{--                                <div class="col-md-3">--}}
                    {{--                                    <div class="form-group">--}}
                    {{--                                        <label for="" class="col-form-label">@lang('admin.inputs.property_address')</label>--}}
                    {{--                                        <input class="form-control" type="text" name="comparables[1][property]" required id="property_1" placeholder="@lang('admin.inputs.property_address')">--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="col-md-3">--}}
                    {{--                                    <div class="form-group">--}}
                    {{--                                        <label for="" class="col-form-label">Sale Date</label>--}}
                    {{--                                        <input class="form-control" type="text" name="comparables[1][type]" required id="type_1" placeholder="Sale Date">--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="col-md-3">--}}
                    {{--                                    <div class="form-group">--}}
                    {{--                                        <label for="" class="col-form-label">Location</label>--}}
                    {{--                                        <input class="form-control" type="text" name="comparables[1][location]" required id="location_1" placeholder="Location">--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="col-md-3">--}}
                    {{--                                    <div class="form-group">--}}
                    {{--                                        <label for="" class="col-form-label">@lang('admin.inputs.yearofbuild')</label>--}}
                    {{--                                        <input class="form-control" type="text" name="comparables[1][distance]" required id="distance_1" placeholder="@lang('admin.inputs.yearofbuild')">--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="col-md-3">--}}
                    {{--                                    <div class="form-group">--}}
                    {{--                                        <label for="" class="col-form-label">@lang('admin.inputs.total_sft')</label>--}}
                    {{--                                        <input class="form-control" type="text" name="comparables[1][rent]" required id="rent_1" placeholder="@lang('admin.inputs.total_sft')">--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="col-md-3">--}}
                    {{--                                    <div class="form-group">--}}
                    {{--                                        <label for="" class="col-form-label">@lang('admin.inputs.price/sft')</label>--}}
                    {{--                                        <input class="form-control" type="text" name="comparables[1][saleprice]" required id="saleprice_1" placeholder="@lang('admin.inputs.price/sft')">--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}


                    {{--                                <div class="col-md-3">--}}
                    {{--                                    <div class="form-group">--}}
                    {{--                                        <label for="propertyLogo">Property Logo</label>--}}
                    {{--                                        <input class="form-control" type="file" name="propertylogoimage" accept="image/png,image/jpeg" required id="propertyLogo" placeholder="Enter Property Logo">--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="col-md-3">--}}
                    {{--                                    <div class="form-group">--}}
                    {{--                                        <label for="propertyLogo">Map</label>--}}
                    {{--                                        <input class="form-control" type="file" name="map" accept=".pdf" required id="propertyLogo" placeholder="Enter Property Logo">--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="col-md-3">--}}
                    {{--                                    <div class="form-group">--}}
                    {{--                                        <label for="propertyLogo">Comparables Details</label>--}}
                    {{--                                        <input class="form-control" type="file" name="comparabledetails" accept=".pdf" required id="propertyLogo" placeholder="Enter Property Logo">--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}

                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                        <button type="button" class="btn btn-info" id="add_comparables">+ @lang('admin.addcomparables')</button>--}}
                    {{--                    </div>--}}
                    {{--                    <br>--}}
                    <h5>@lang('admin.doc/reports')</h5>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="" class="col-form-label">@lang('admin.labels.investordos')</label>
                                <input class="form-control" type="file" name="investor" accept="application/pdf" required id="propertyInvestor">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="" class="col-form-label">@lang('admin.labels.titlereport')</label>
                                <input class="form-control" type="file" name="titlereport" accept="application/pdf" id="propertyTitlereport">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="" class="col-form-label">Subscription Agreement</label>
                                <input class="form-control" type="file" name="termsheet" accept="application/pdf" id="termsheet">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="" class="col-form-label">Updates</label>
                                <input class="form-control" type="file" accept="application/pdf" name="propertyUpdatesDoc" id="propertyUpdatesDoc">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 d-none">
                            <div class="form-group">
                                <label for="" class="col-form-label">Management Team</label>
                                <input class="form-control" type="file" accept="application/pdf" name="propertyManagementTeam" id="propertyManagementTeam">
                            </div>
                        </div>
                    </div>
                    {{--                    <h5>@lang('admin.property_details')</h5>--}}
                    {{--                    <div class="row">--}}
                    {{--                        <div class="col-md-3">--}}
                    {{--                            <div class="form-group">--}}
                    {{--                                <label for="" class="col-form-label">@lang('admin.inputs.locality')</label>--}}
                    {{--                                <input class="form-control" type="text" name="locality" required id="locality" placeholder="@lang('admin.placeholders.locality')" required maxlength="200">--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="col-md-3">--}}
                    {{--                            <div class="form-group">--}}
                    {{--                                <label for="" class="col-form-label">@lang('admin.inputs.year_of_build')</label>--}}
                    {{--                                <input class="form-control" type="number" name="yearOfConstruction" required id="yearOfConstruction" placeholder="@lang('admin.placeholders.year_of_build')" min="0">--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="col-md-3">--}}
                    {{--                            <div class="form-group">--}}
                    {{--                                <label for="" class="col-form-label">@lang('admin.inputs.community')</label>--}}
                    {{--                                <input class="form-control" type="text" name="storeys" required id="storeys" placeholder="@lang('admin.placeholders.storeys')" min="0">--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="col-md-3">--}}
                    {{--                            <div class="form-group">--}}
                    {{--                                <label for="" class="col-form-label">@lang('admin.inputs.no_of_bed')</label>--}}
                    {{--                                <input class="form-control" type="text" name="propertyParking" required id="propertyParking" placeholder="@lang('admin.placeholders.parking')">--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="col-md-3">--}}
                    {{--                            <div class="form-group">--}}
                    {{--                                <label for="" class="col-form-label">Number of Bathrooms</label>--}}
                    {{--                                <input class="form-control" type="number" name="floorforSale" required id="floorforSale" placeholder="Number of Bathrooms" min="0">--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="col-md-3">--}}
                    {{--                            <div class="form-group">--}}
                    {{--                                <label for="" class="col-form-label">@lang('admin.inputs.total_area')</label>--}}
                    {{--                                <input class="form-control" type="number" name="propertyTotalBuildingArea" required id="propertyTotalBuildingArea" placeholder="@lang('admin.placeholders.total_area')" min="0">--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="col-md-3">--}}
                    {{--                            <div class="form-group">--}}
                    {{--                                <label for="" class="col-form-label">@lang('admin.inputs.details_highlight')</label>--}}
                    {{--                                <textarea class="form-control" type="text" name="propertyDetailsHighlights" required id="propertyDetailsHighlights" placeholder="@lang('admin.placeholders.details_highlight')"></textarea>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="col-md-3">--}}
                    {{--                            <div class="form-group">--}}
                    {{--                                <label for="" class="col-form-label">@lang('admin.placeholders.floorplan')</label>--}}
                    {{--                                <input class="form-control" type="file" name="floorplan" accept=".pdf" required id="propertyFloorPlan">--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="col-md-3">--}}
                    {{--                            <div class="form-group">--}}
                    {{--                                <label for="" class="col-form-label">@lang('admin.placeholders.propertyimage')</label>--}}
                    {{--                                <input class="form-control" type="file" name="propertyimages[]" accept="image/png,image/jpeg" required id="propertyimages" multiple>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    <h5>Token Details</h5>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="" class="col-form-label">@lang('admin.coin.tokenname')</label>
                                <input class="form-control" type="text" name="token_name" required id="tokenName" placeholder="@lang('admin.coin.tokenname')">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="" class="col-form-label">@lang('admin.coin.tokensymbol')</label>
                                <input class="form-control" type="text" name="token_symbol" required id="tokenSymbol" placeholder="@lang('admin.coin.tokensymbol')">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="" class="col-form-label">@lang('admin.coin.tokenvalue') ({{ Setting::get('default_currency') }})</label>
                                <input class="form-control" type="text" name="token_value" required id="tokenValue" placeholder="@lang('admin.coin.tokenvalue')">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="" class="col-form-label">@lang('admin.coin.tokensupply')</label>
                                <input class="form-control" type="text" name="token_supply" required id="tokenSupply" placeholder="@lang('admin.coin.tokensupply')">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="" class="col-form-label">@lang('admin.coin.tokendecimal')</label>
                                <input class="form-control" type="text" name="token_decimal" required id="tokenDecimal" placeholder="@lang('admin.coin.tokendecimal')">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="" class="col-form-label">@lang('admin.token_to_be_issued')</label>
                                <br>
                                <input type="radio" name="tokentype" class="tokentype" value="ERC20" checked> @lang('admin.erc20_utility')<br>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="" class="col-form-label">@lang('admin.coin.tokenimage')</label>
                                <input class="form-control" type="file" name="token_image" required id="tokenImage">
                            </div>
                        </div>
                    </div>
                    <!-- end row -->

                    <h5>Management Team</h5>
                    <div class="row form-group">
                        <div class="col-md-6">
                            <label for="" class="col-form-label">Management Team Description</label>
                            <textarea class="form-control" type="text" name="ManagementTeamDescription" required id="propertyOverview" placeholder="Enter Management Team Description"></textarea>
                        </div>
                    </div>
                    <h6>Management Members</h6>
                    <div id="divManagementMembers" data-id="0"></div>
                    <button type="button" class="btn btn-info" id="AddMember">+ Add Member</button>

                    <div class="row next-btn" style="padding:25px 0px;">
                        <div class="col-sm-12">
                            <div class="form-group text-center mb-0">
                                <input type="submit" class="btn btn-primary waves-effect waves-light mr-1" value="Create Token">
                                {{-- <button class="btn btn-primary waves-effect waves-light mr-1" type="submit">Previous</button>
                                <button class="btn btn-primary waves-effect waves-light mr-1" type="submit">Create Token</button> --}}
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- end container-fluid -->
        @endsection
        <!-- Start Page Content here -->
            @section('scripts')
                <script type="text/javascript" src="{{asset('main/vendor/jquery/jquery-1.12.3.min.js')}}"></script>
                <script src="{{ asset('main/assets/js/validate.min.js') }}"></script>
                <script src="{{ asset('main/assets/js/additional-method.js') }}"></script>
                <script type="text/javascript">
                    $(document).ready(function () {
                        var n = 2;
                        $("#add_comparables").click(function (e) {
                            e.preventDefault();
                            var html = '<div id="comparables_block_' + n + '" class="form-group row"><div class="form-group col-md-3"><label for="" class="col-form-label">Property Address</label><input class="form-control" type="text" name="comparables[' + n + '][property]" required id="property_' + n + '" placeholder="Property Address"></div><div class="form-group col-md-3"><label for="" class="col-form-label">Sale Date</label><input class="form-control" type="text" name="comparables[' + n + '][type]" required id="type_' + n + '" placeholder="Sale Date"></div><div class="form-group col-md-3"><label for="" class="col-form-label">Location</label><input class="form-control" type="text" name="comparables[' + n + '][location]" required id="location_' + n + '" placeholder="Location"></div><div class="form-group col-md-3"><label for="" class="col-form-label">Year of Build</label><input class="form-control" type="number" name="comparables[' + n + '][distanaccept="image/png,image/jpeg"ce]" required id="distance_' + n + '" placeholder="Year of Build" min="0" step="any"></div><div class="form-group col-md-3"><label for="" class="col-form-label">Total Sft</label><input class="form-control" type="number" name="comparables[' + n + '][rent]" required id="rent_' + n + '" placeholder="Total Sft" min="0" step="any"></div><div class="form-group col-md-3"><label for="" class="col-form-label">Sale Price</label><input class="form-control" type="number" name="comparables[' + n + '][saleprice]" required id="saleprice_' + n + '" placeholder="Sale Price" min="0" step="any"></div><div class="col-md-3"><div class="form-group"><label for="propertyLogo">Property Logo</label><input class="form-control" type="file" name="propertyVideo" accept="image/png,image/jpeg"  required id="propertyLogo" placeholder="Enter Property Logo"></div></div><div class="col-md-3"><div class="form-group"><label for="propertyLogo">Map</label><input class="form-control" type="file" name="map" accept=".pdf"  required id="propertyLogo" placeholder="Enter Property Logo"></div></div><div class="col-md-3"><div class="form-group"><label for="propertyLogo">Comparables Details</label><input class="form-control" type="file" name="comparabledetails" accept=".pdf"  required id="propertyLogo" placeholder="Enter Property Logo"></div></div><div class="form-group col-md-3"><button type="button" class="btn btn-danger landmark_remove" id="landmark_remove_' + n + '" onclick="comparables_remove(' + n + ');" >X</button></div></div>';
                            $("#comparables_section").append(html);
                            n = n + 1;
                        });

                        $("#add_landmark").click(function (e) {
                            e.preventDefault();
                            var html = '<div id="landmark_block_' + n + '" class="form-group row"><div class="form-group col-md-3"><label for="" class="col-form-label">Landmark Name</label><input class="form-control" type="text" name="landmarks[' + n + '][landmarkName]" required id="landmarkName_' + n + '" placeholder="Enter landmark name"></div><div class="form-group col-md-3"><label for="" class="col-form-label">Landmark Distance</label><input class="form-control" type="number" name="landmarks[' + n + '][landmarkDist]" required id="landmarkDist_' + n + '" placeholder="Enter landmark distance" min="0" step="any"></div><div class="form-group col-md-3"><button type="button" class="btn btn-danger landmark_remove" id="landmark_remove_' + n + '" onclick="landmark_remove(' + n + ');" >X</button></div></div>';
                            $("#landmark_section").append(html);
                            n = n + 1;
                        });
                    });
                    function landmark_remove(n) {
                        $("#landmark_block_" + n).remove();
                    }

                    function comparables_remove(n) {
                        $("#comparables_block_" + n).remove();
                    }

                    $(document).ready(function () {
                        addManagementMember(0);
                    });

                    $('#AddMember').click(function (e) {
                        var index = parseInt($('#divManagementMembers').attr('data-id'));
                        addManagementMember(index + 1);
                    });

                    function removeMember(index) {
                        $('#MemberBlock_' + index).remove();
                    }

                    function addManagementMember(index) {
                        var removeBtn = '<button type="button" class="btn btn-danger" style="margin-top: 34px;" onclick="removeMember(' + index + ');">X</button>';
                        var temp      = '<div class="row" id="MemberBlock_' + index + '">' +
                            '	<div class="col-md-3 form-group">' +
                            '		<label for="" class="col-form-label">Member Name</label>' +
                            '		<input class="form-control" type="text" name="member[' + index + '][name]" required id="MemberName_' + index + '" placeholder="Enter Member Name">' +
                            '	</div>' +
                            '	<div class="col-md-3 form-group">' +
                            '		<label for="" class="col-form-label">Member Position</label>' +
                            '		<input class="form-control" type="text" name="member[' + index + '][position]" required id="MemberName_' + index + '" placeholder="Enter Member Position">' +
                            '	</div>' +
                            '	<div class="col-md-3 form-group">' +
                            '		<label for="" class="col-form-label">Member Image</label>' +
                            '		<input class="form-control" type="file" name="member[' + index + '][pic]" required id="MemberPic_' + index + '" accept="image/png,image/jpeg" placeholder="Select Member Picture">' +
                            '	</div>' +
                            '	<div class="col-md-3 form-group">' +
                            '		<label for="" class="col-form-label">Member Description</label>' +
                            '		<input class="form-control" type="text" name="member[' + index + '][description]" required id="MemberDescription_' + index + '" placeholder="Enter Description">' +
                            '	</div>' +
                            '	<div class="col-md-1 form-group">' +
                            '       ' + ((index > 0) ? removeBtn : '') +
                            '   </div>' +
                            '</div>';
                        $('#divManagementMembers').attr('data-id', index).append(temp);
                    }

                </script>
                <script src="https://cdn.jsdelivr.net/npm/js-cookie@rc/dist/js.cookie.min.js"></script>
                <script>
                    $(document).ready(function () {
                        $("input[type=text], input[type=radio], input[type=file], textarea, select, input[type=number]").blur(function () {
                            if ($(this).val() != "") {
                                Cookies.set('token_asset_' + $(this).attr('name'), $(this).val(), {expires: 1});
                                console.log($(this).val());
                                console.log($(this).attr('name'));
                            }
                        });

                        $("input[type=text], textarea, select, input[type=radio],  input[type=number]").each(function () {

                            var cookval = Cookies.get('token_asset_' + $(this).attr('name'));
                            if ($(this).attr('type') == "radio") {
                                var inpname = $(this).attr('name');
                                $("input[name=" + inpname + "][value=" + cookval + "]").prop("checked", true);
                            } else {
                                $(this).val(cookval);
                            }
                        });
                    });

                </script>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $("#property-create").validate({
                                                           rules        : {
                                                               "propertyName"             : {
                                                                   required : true,
                                                                   maxlength: 200,
                                                               },
                                                               "propertyLocation"         : {
                                                                   required : true,
                                                                   maxlength: 200,
                                                               },
                                                               "propertyLogo"             : {
                                                                   required: true,
                                                                   accept  : 'image/jpeg, image/png, image/jpg, image/bmp',
                                                               },
                                                               "propertyType"             : {
                                                                   required: true,
                                                               },
                                                               "totalDealSize"            : {
                                                                   required: true,
                                                                   number  : true,
                                                                   min     : 0,
                                                               },
                                                               "expectedIrr"              : {
                                                                   required : false,
                                                                   number   : true,
                                                                   min      : 0,
                                                                   maxlength: 3,
                                                               },
                                                               "initialInvestment"        : {
                                                                   required: true,
                                                                   number  : true,
                                                                   min     : 0,
                                                               },
                                                               "propertyEquityMultiple"   : {
                                                                   required: false,
                                                                   number  : true,
                                                                   min     : 0,
                                                               },
                                                               "holdingPeriod"            : {
                                                                   required: true,
                                                                   number  : true,
                                                                   min     : 0,
                                                               },
                                                               "total_sft"                : {
                                                                   required: true,
                                                                   number  : true,
                                                                   min     : 0,
                                                               },
                                                               "propertyOverview"         : {
                                                                   required: true,
                                                               },
                                                               "propertyHighlights"       : {
                                                                   required: true,
                                                               },
                                                               "propertyLocationOverview" : {
                                                                   required: true,
                                                               },
                                                               "locality"                 : {
                                                                   required : true,
                                                                   maxlength: 200,
                                                               },
                                                               "yearOfConstruction"       : {
                                                                   required : true,
                                                                   number   : true,
                                                                   min      : 0,
                                                                   maxlength: 4,
                                                               },
                                                               "storeys"                  : {
                                                                   required: true,
                                                               },
                                                               "propertyParking"          : {
                                                                   required : true,
                                                                   maxlength: 200,
                                                               },
                                                               "floorforSale"             : {
                                                                   required: true,
                                                                   number  : true,
                                                                   min     : 0,
                                                               },
                                                               "propertyTotalBuildingArea": {
                                                                   required: true,
                                                                   number  : true,
                                                                   min     : 0,
                                                               },
                                                               "propertyDetailsHighlights": {
                                                                   required: true,
                                                               },
                                                               "floorplan"                : {
                                                                   required: true,
                                                                   accept  : 'pdf',
                                                               },
                                                               "investor"                 : {
                                                                   required: true,
                                                                   accept  : 'pdf',
                                                               },
                                                               "titlereport"              : {
                                                                   required: false,
                                                                   accept  : 'pdf',
                                                               },
                                                               "termsheet"                : {
                                                                   required: false,
                                                                   accept  : 'pdf',
                                                               },
                                                               "propertyimages"           : {
                                                                   required: true,
                                                                   accept  : 'image/jpeg, image/png, image/jpg, image/bmp',
                                                               },
                                                               "management"               : {
                                                                   required: true,
                                                               },
                                                               "updates"                  : {
                                                                   required: true,
                                                               },
                                                               "propertylogoimage"        : {
                                                                   required: true,
                                                                   accept  : 'image/jpeg, image/png, image/jpg, image/bmp',
                                                               },
                                                               "propertyVideo"            : {
                                                                   required: false,
                                                                   accept  : 'video/mp4,video/x-m4v',
                                                               },
                                                               "propertyManagementTeam"   : {
                                                                   required: false,
                                                                   accept  : 'pdf',
                                                               },
                                                               "propertyUpdatesDoc"       : {
                                                                   required: false,
                                                                   accept  : 'pdf',
                                                               },
                                                               "map"                      : {
                                                                   required: true,
                                                                   accept  : 'pdf',
                                                               },
                                                               "comparabledetails"        : {
                                                                   required: true,
                                                                   accept  : 'pdf',
                                                               },
                                                               "token_name"               : {
                                                                   required: true,
                                                               },
                                                               "token_symbol"             : {
                                                                   required : true,
                                                                   minlength: 3,
                                                                   maxlength: 4,
                                                               },
                                                               "token_value"              : {
                                                                   required: true,
                                                                   number  : true,
                                                               },
                                                               "token_decimal"            : {
                                                                   required : true,
                                                                   digits   : true,
                                                                   maxlength: 2,
                                                               }
                                                           },
                                                           messages     : {
                                                               "propertyName"             : {
                                                                   required: "Please Enter Fund Name",
                                                               },
                                                               "propertyLocation"         : {
                                                                   required: "Please Enter Property Location",
                                                               },
                                                               "propertyLogo"             : {
                                                                   required: "Please Choose Fund Logo",
                                                                   accept  : "Please Upload Valid format (jpeg|png|jpg|bmp)",
                                                               },
                                                               "propertyVideo"            : {
                                                                   required: "Please Choose Fund Logo",
                                                                   accept  : "Please Upload Valid format (mp4|x-m4v)",
                                                               },
                                                               "management"               : {
                                                                   required: "Please Upload Management",
                                                               },
                                                               "updates"                  : {
                                                                   required: "Please Upload Updates",
                                                               },
                                                               "propertylogoimage"        : {
                                                                   required: "Please Choose Fund Logo",
                                                                   accept  : "Please Upload Valid format (jpeg|png|jpg|bmp)",
                                                               },
                                                               "propertyimages"           : {
                                                                   required: "Please Choose Property Logo",
                                                                   accept  : "Please Upload Valid format (jpeg|png|jpg|bmp)",
                                                               },
                                                               "totalDealSize"            : {
                                                                   required: "Please Enter Total Deal Size",
                                                               },
                                                               "expectedIrr"              : {
                                                                   required: "Please Enter Expected IRR",
                                                               },
                                                               "initialInvestment"        : {
                                                                   required: "Please Enter InitialInvestment",
                                                               },
                                                               "propertyEquityMultiple"   : {
                                                                   required: "Please Enter Property Equity Multiple",
                                                               },
                                                               "holdingPeriod"            : {
                                                                   required: "Please Enter Holding Period",
                                                               },
                                                               "total_sft"                : {
                                                                   required: "Please Enter Total SFT",
                                                               },
                                                               "propertyOverview"         : {
                                                                   required: "Please Enter Fund Overview",
                                                               },
                                                               "propertyHighlights"       : {
                                                                   required: "Please Enter Fund Highlights",
                                                               },
                                                               "propertyLocationOverview" : {
                                                                   required: "Please Enter Location Overview",
                                                               },
                                                               "locality"                 : {
                                                                   required: "Please Enter Locality",
                                                               },
                                                               "yearOfConstruction"       : {
                                                                   required: "Please Enter Year of Construction",
                                                               },
                                                               "propertyParking"          : {
                                                                   required: "Please Enter Parking",
                                                               },
                                                               "floorforSale"             : {
                                                                   required: "Please Enter Floor for Sale",
                                                               },
                                                               "propertyTotalBuildingArea": {
                                                                   required: "Please Enter Total Building Area",
                                                               },
                                                               "propertyDetailsHighlights": {
                                                                   required: "Please Enter Details Highlights",
                                                               },
                                                               "floorplan"                : {
                                                                   required: "Please Choose Floor Plan",
                                                                   accept  : "Please Upload Valid format (pdf)",
                                                               },
                                                               "investor"                 : {
                                                                   required: "Please Choose Investor Document",
                                                                   accept  : "Please Upload Valid format (pdf)",
                                                               },
                                                               "titlereport"              : {
                                                                   required: "Please Choose Title Report",
                                                                   accept  : "Please Upload Valid format (pdf)",
                                                               },
                                                               "termsheet"                : {
                                                                   required: "Please Choose Term Sheet",
                                                                   accept  : "Please Upload Valid format (pdf)",
                                                               },
                                                               "comparabledetails"        : {
                                                                   required: "Please Choose Term Sheet",
                                                                   accept  : "Please Upload Valid format (pdf)",
                                                               },
                                                               "propertyManagementTeam"   : {
                                                                   required: "Please Choose Management Team Report",
                                                                   accept  : "Please Upload Valid format (pdf)",
                                                               },
                                                               "propertyUpdatesDoc"       : {
                                                                   required: "Please Choose Updates Report",
                                                                   accept  : "Please Upload Valid format (pdf)",
                                                               },
                                                               "map"                      : {
                                                                   required: "Please Choose Term Sheet",
                                                                   accept  : "Please Upload Valid format (pdf)",
                                                               },
                                                           },
                                                           submitHandler: function (form) {
                                                               form.submit();
                                                           }
                                                       });
                    });
                </script>
@endsection