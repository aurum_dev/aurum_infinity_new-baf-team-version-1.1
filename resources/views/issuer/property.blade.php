@extends('issuer.layout.base')
@section('content')
<style>
    #example_length{
margin-top: 5px !important;
    float: right !important;
    margin-right: 266px !important;
}

    
</style>
          <!-- breadcrumb -->
                    <div class="breadcrumb-header justify-content-between">
                        <div class="my-auto">
                            <div class="d-flex">
                                <h4 class="content-title mb-0 my-auto"> Issuer Property List</h4><span class="text-muted mt-1 tx-13 ms-2 mb-0">/ Property List</span>
                            </div>
                        </div>
                        <div class="d-flex my-xl-auto right-content">
                            
                            
                        </div>
                    </div>
                    <!-- breadcrumb -->



<div class="row">
<div class="col-xl-12">
                        <div class="card mg-b-20">
                           
                            <div class="card-body">
                                <div class="table-responsive">

                                    <table id="example" class="table key-buttons text-md-nowrap">
                                        <thead>
                                            <tr>
                                                <th class="border-bottom-0">S.NO</th>
                                                <th class="border-bottom-0">Property Name</th>
                                                <th class="border-bottom-0">Property Image</th>
                                                <th class="border-bottom-0">State/City</th>
                                                <th class="border-bottom-0">Asset Type</th>
                                                <th class="border-bottom-0">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($properties as $key => $item)  
                                            <tr>
                                                <td>{{ $key+1}}</td>
                                                <td>{{$item->propertyName}}</td>
                                                <td>
                                                    
<?php 

$gallery=explode("#",$item->galleryImage);

?>   
                                      <img src="../{{$gallery[0]}}" class="attachment-full size-full wp-post-image" alt="Blog-Image-001" style="width:100px;" >              
                                                </td>
                                                
                                                <td>{{$item->propertyState}} / {{$item->propertyCity}}</td>
                                                <td>{{$item->propertyType}}</td>
                                                <td>
                                                    @if($item->tokenStatus=="live")
                                                    <a href="{{ url('issuer/tokenList/'.$item->id) }}" class="btn btn-danger btn-block">Token Acquired</a>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
</div>

@endsection


