@extends('issuer.layout.base')
@section('content')
@section('style')
<style>
    .my_highlights ul li {
        margin-bottom: 10px;
        color: #929292;
        font-size: 14px;
        line-height: 1.6;
        letter-spacing: .2px;
    }
    dl, ol, ul {
        margin-top: 0;
        margin-bottom: 1rem;
    }
    .nav-pills > li > a, .nav-tabs > li > a {
        padding: 15px;
    }
    .product-slider {
        padding: 25px;
    }
    .product-slider #carousel {
        /* border: 4px solid #1089c0; */
        margin: 0;
    }
    .product-slider #thumbcarousel {
        margin: 12px 0 0;
        padding: 0 10px;
    }
    .product-slider #thumbcarousel .item {
        text-align: center;
    }
    .product-slider #thumbcarousel .item .thumb {
        width: 15%;
        margin: 0 2%;
        display: inline-block;
        vertical-align: middle;
        cursor: pointer;
        max-width: 98px;
    }
    .product-slider #thumbcarousel .item .thumb:hover {
        border-color: #1089c0;
    }
    .product-slider .item img {
        width: 100%;
        height: 75px;
        object-fit: cover;
        border-radius: 5px !important;
    }
    .carousel-control {
        color: #0284b8;
        text-align: center;
        text-shadow: none;
        font-size: 30px;
        width: 30px;
        height: 30px;
        line-height: 20px;
        top: 23%;
    }
    .carousel-control:hover,
    .carousel-control:focus,
    .carousel-control:active {
        color: #333;
    }
    .carousel-caption,
    .carousel-control .fa {
        /*font: normal normal normal 30px/26px FontAwesome;*/
    }
    .carousel-control {
        background-color: rgba(0, 0, 0, 0);
        bottom: auto;
        font-size: 20px;
        left: 0;
        position: absolute;
        top: 30%;
        width: auto;
    }
    .carousel-control.right,
    .carousel-control.left {
        background-color: rgba(0, 0, 0, 0);
        background-image: none;
    }
    /* .showing {
        border: 2px solid #4a28f5;
    } */
    .product-slider .carousel-inner {
        border-radius: 5% !important;
        box-shadow: 0px 0px 15px 1px rgb(33 33 33 / 30%);
        object-fit: cover;
    }
    div#thumbcarousel .carousel-inner {
        box-shadow: none !important;
    }
    div#carousel .carousel-inner .item img {
        height: 400px !important;
        width: 100%;
    }
    .fa-angle-right:before {
        content: "\f105";
        top: 0;
        bottom: 0;
        right: 0;
        left: -15px;
        position: relative;
    }
    .fa-angle-left:before {
        content: "\f104";
        top: 0;
        bottom: 0;
        right: 0;
        left: 0;
        position: relative;
    }
    .product-slider {
        padding: 25px;
    }
    .product-slider #carouselSecond {
        /* border: 4px solid #1089c0; */
        margin: 0;
    }
    .product-slider #thumbcarouselSecond {
        margin: 12px 0 0;
        padding: 0 10px;
    }
    .product-slider #thumbcarouselSecond .item {
        text-align: center;
    }
    .product-slider #thumbcarouselSecond .item .thumb {
        width: 15%;
        margin: 0 2%;
        display: inline-block;
        vertical-align: middle;
        cursor: pointer;
        max-width: 98px;
    }
    .product-slider #thumbcarouselSecond .item .thumb:hover {
        border-color: #1089c0;
    }
    .product-slider .item img {
        width: 100%;
        height: 75px;
        object-fit: cover;
        border-radius: 5px !important;
    }
    .carousel-control {
        color: #0284b8;
        text-align: center;
        text-shadow: none;
        font-size: 30px;
        width: 30px;
        height: 30px;
        line-height: 20px;
        top: 23%;
    }
    .carousel-control:hover,
    .carousel-control:focus,
    .carousel-control:active {
        color: #333;
    }
    .carousel-caption,
    .carousel-control .fa {
        /*font: normal normal normal 30px/26px FontAwesome;*/
    }
    .carousel-control {
        background-color: rgba(0, 0, 0, 0);
        bottom: auto;
        font-size: 20px;
        left: 0;
        position: absolute;
        top: 30%;
        width: auto;
    }
    .carousel-control.right,
    .carousel-control.left {
        background-color: rgba(0, 0, 0, 0);
        background-image: none;
    }
    /* .showing {
        border: 2px solid #4a28f5;
    } */
    .product-slider .carousel-inner {
        border-radius: 5% !important;
        box-shadow: 0px 0px 15px 1px rgb(33 33 33 / 30%);
        object-fit: cover;
    }
    div#thumbcarouselSecond .carousel-inner {
        box-shadow: none !important;
    }
    div#carouselSecond .carousel-inner .item img {
        height: 400px !important;
        width: 100%;
    }
    .fa-angle-right:before {
        content: "\f105";
        top: 0;
        bottom: 0;
        right: 0;
        left: -15px;
        position: relative;
    }
    .fa-angle-left:before {
        content: "\f104";
        top: 0;
        bottom: 0;
        right: 0;
        left: 0;
        position: relative;
    }
    @media (max-width: 767px) {
        .product-slider #thumbcarousel .item .thumb, .product-slider #thumbcarouselSecond .item .thumb {
            max-width: 30px !important;
        }
        .product-slider .item img {
            height: 50px !important;
        }
    }
    /* Third Carasol Start */
    .product-slider #carouselThird {
        /* border: 4px solid #1089c0; */
        margin: 0;
    }
    .product-slider #thumbcarouselThird {
        margin: 12px 0 0;
        padding: 0 10px;
    }
    .product-slider #thumbcarouselThird .item {
        text-align: center;
    }
    .product-slider #thumbcarouselThird .item .thumb {
        width: 15%;
        margin: 0 2%;
        display: inline-block;
        vertical-align: middle;
        cursor: pointer;
        max-width: 98px;
    }
    .product-slider #thumbcarouselThird .item .thumb:hover {
        border-color: #1089c0;
    }
    div#thumbcarouselThird .carousel-inner {
        box-shadow: none !important;
    }
    div#carouselThird .carousel-inner .item img {
        height: 400px !important;
        width: 100%;
    }
    @media (max-width: 767px) {
        .product-slider #thumbcarouselThird .item .thumb {
            max-width: 30px !important;
        }
    }
    /* Third Carasol End */
    .carousel-indicators {
        right: 50%;
        top: auto;
        bottom: -10px;
        margin-right: -19px;
    }
    .team_slide {
        padding: 0 40px 30px 40px;
    }
    .carousel-indicators li {
        background: #cecece;
    }
    .carousel-indicators .active {
        background: #428bca;
    }
    a.left.carousel-control, a.right.carousel-control {
        font: normal normal normal 30px/26px FontAwesome;
    }
    
        .provider-details {padding-bottom: 20px;border-bottom: 1px dashed grey;}
        .property-img-sec {padding: 20px 0px 0px;}
        .target-txt {font-size: 23px;font-weight: 700;color: #000102;}
        .pro-progress-block {height: 10px;background-color: #e3e9e9;margin: 20px 0px 15px;border-radius: 30px;position: relative;}
        .progress-txt b {color: #6aa1ff;margin-right: 15px;}
        .pro-detail-box {border-bottom: 1px dashed grey;}
        .progress-txt {display: inline-block;color: #333;padding: 10px 0px;margin-top: -10px !important;font-weight: 800;}
        .pro-detail-txt {margin: 0px;padding: 10px 0px;font-size: 13px;}
        /* ul.nav.nav-tabs.property-nav-tabs li {padding: 25px 50px !important;} */
        .property-content {padding: 40px 0px;}
        .pro-tab-wrap {border-bottom: 1px solid #cecece;background: #fafafa;}
        .nav-tabs > li > a.active {border: 1px dashed #000;border-bottom: 3px solid #000;background: #ffffff;box-shadow: none;padding: 25px 50px !important;}
        .team-member-image img:hover {filter: grayscale(100%);}
        .team-member-image img {width: 100px;height: 100px;border-radius: 50%;}
        .team_member {text-align: center;}
        .p40 {padding: 40px 0px;}
        .tab-content {padding: 0px 0 0 0;}
        .nav-tabs {border-bottom: 1px dashed grey;}
        video.banner_video {
            width: 100%;
            height: 400px;
        }
        video.banner_video .vid {
            height: auto;
        }
    </style>
    <link rel="stylesheet" href="{{asset('public/asset/package/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/asset/package/css/owl.theme.default.css')}}">
    @endsection
    <div class="content-page"><!-- START content-page -->

        <!-- Header Banner Start -->
        <div class="header-breadcrumbs">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6"><h1>Property Details</h1></div>
                    <div class="col-sm-6">
                        <div class="breadcrumb-four" style="text-align: right;">
                            <ul class="breadcrumb">
                                <li><a href="{{url('/dashboard')}}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-box">
                                            <path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path>
                                            <polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline>
                                            <line x1="12" y1="22.08" x2="12" y2="12"></line>
                                        </svg>
                                        <span>Dashboard</span></a></li>
                                <li class="active"><a href="">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-cpu">
                                            <rect x="4" y="4" width="16" height="16" rx="2" ry="2"></rect>
                                            <rect x="9" y="9" width="6" height="6"></rect>
                                            <line x1="9" y1="1" x2="9" y2="4"></line>
                                            <line x1="15" y1="1" x2="15" y2="4"></line>
                                            <line x1="9" y1="20" x2="9" y2="23"></line>
                                            <line x1="15" y1="20" x2="15" y2="23"></line>
                                            <line x1="20" y1="9" x2="23" y2="9"></line>
                                            <line x1="20" y1="14" x2="23" y2="14"></line>
                                            <line x1="1" y1="9" x2="4" y2="9"></line>
                                            <line x1="1" y1="14" x2="4" y2="14"></line>
                                        </svg>
                                        <span>Property Details</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header Banner Start -->


        <!-- Breadcrumb -->
        <div class="page-content">
            <div class="content"><!-- Start cont inner page -->
                <div class="container-fluid wizard-border">
                    <div class="property-content">
                        <div class="container-fluid">
                            <!-- Provider Details Starts -->
                            <div class="provider-details">
                                <div class="row">
                                    <div class="col-md-8 left-col">
                                    </div>
                                    <div class="col-md-4 right-col">
                                        <div class="apply-btn-wrap text-right">
                                            <a href="{{ route('property') }}" class="apply-btn btn btn-info">Back to Properties</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Provider Details Ends -->
                            <!-- Property Image Section Starts -->
                            <div class="property-img-sec row">
                                <!-- Property-img-box Starts -->
                                <div class="col-md-6 left-col">
                                    <div class="property-img-box">
                                        <img src="{{ @img($property->propertyLogo) }}" width="100%">
                                    </div>
                                </div>
                                <!-- Property-img-box Ends -->
                                <!-- Property Image Right Starts -->
                                <div class="col-md-6 right-col">
                                    <div class="property-img-right">
                                        <div>
                                            <h5 class="target-txt">Targets</h5>

                                            <div class="property-progress">
                                                <div class="pro-progress-block">
                                                    <div class="progress-value" style="width: {{ @$property->accuired_percentage }}%;">
                                                    </div>
                                                </div>
                                                <span class="progress-txt"><b>{{ @$property->accuired_percentage }}% FUNDED</b> ${{ @$property->accuired_usd }} OF ${{ @$property->totalDealSize }}</span>
                                            </div>
                                        </div>
                                        <div class="pro-detail-box">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-1"><b>Leased Period</b></p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-2">{{ @$property->leased_period }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pro-detail-box">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-1"><b>RERA Number</b></p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-2">{{ @$property->rera_number }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pro-detail-box">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-1"><b>Developer Name</b></p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-2">{{ @$property->dev_name }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pro-detail-box">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-1"><b>Property Age</b></p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-2">{{ @$property->property_age }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pro-detail-box">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-1"><b>Tenant Name</b></p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-2">{{ @$property->tenant_name }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pro-detail-box">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-1"><b>Rent Escalation</b></p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-2">{{ @$property->rent_escalation }}%</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pro-detail-box">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-1"><b>Notice Period</b></p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-2">{{ @$property->notice_period }}1</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pro-detail-box">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-1"><b>Carpet Area</b></p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-2">{{ @$property->carpet_area }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pro-detail-box">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-1"><b>Built Up Area</b></p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-2">{{ @$property->built_up_area }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pro-detail-box">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-1"><b>Gross Yield</b></p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-2">{{ @$property->gross_yield }}%</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pro-detail-box">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-1"><b>Annual Rent</b></p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-2">{{ @$property->annual_rent }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Property Detail Box Starts -->
                                        <div class="pro-detail-box">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-1"><b>Expected IRR</b></p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-2">{{ @$property->expectedIrr }}%</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Property Detail Box Ends -->
                                        <!-- Property Detail Box Starts -->
                                        <div class="pro-detail-box">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-1"><b>Equity Multiple</b></p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-2">{{ @$property->propertyEquityMultiple }}x</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Property Detail Box Ends -->
                                        <!-- Property Detail Box Starts -->
                                        <div class="pro-detail-box">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-1"><b>Expected Holding period</b></p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-2">{{ @$property->holdingPeriod }} years</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Property Detail Box Ends -->
                                        <!-- Property Detail Box Starts -->
                                        <div class="pro-detail-box">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-1"><b>Minimum Investment</b></p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-2">${{ @$property->initialInvestment }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Property Detail Box Ends -->
                                        <!-- Property Detail Box Starts -->
                                        <div class="pro-detail-box">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-1"><b>Funded members</b></p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-2">0</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Property Detail Box Ends -->
                                        <!-- Property Detail Box Starts -->
                                        <div class="pro-detail-box">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-1"><b>Total Deal size</b></p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-2">${{ @$property->totalDealSize  }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Property Detail Box Ends -->
                                        <!-- Property Detail Box Starts -->
                                        <div class="pro-detail-box">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-1"><b>Total Sft</b></p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-2">{{ @$property->total_sft }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Property Detail Box Ends -->
                                        <!-- Property Detail Box Starts -->
                                        <div class="pro-detail-box">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-1"><b>Asset Type</b></p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="pro-detail-txt pro-detail-txt-2">{{ @$property->propertyType }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Property Detail Box Ends -->
                                    </div>
                                </div>
                                <!-- Property Image Right Ends -->
                            </div>
                            <!-- Property Image Section Ends -->
                        </div>
                    </div>
                    <!-- Property Content Ends -->
                    <!-- Property Tab Starts -->





<div class="property-tab">
                        <div class="pro-tab-wrap">
                            <div class="">
                                <div class="overview-menu-wrap">
                                    <div class="res-overview row m-0">
                                     
                                    </div>
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs property-nav-tabs" role="tablist">
                                        <li><a href="#overview" role="tab" data-toggle="tab">Overview</a></li>
                                        <li><a href="#property-details" role="tab" data-toggle="tab">Property Details</a></li>
                                        <li><a href="#property-documents" role="tab" data-toggle="tab">Property Documents</a></li>
                                        <li><a href="#management-team" role="tab" data-toggle="tab">Management Team</a></li>
                                        <li><a href="#property-information" role="tab" data-toggle="tab">Property Information</a></li>
                                    </ul>
                                    <!-- End Nav tabs -->
                                </div>
                            </div>
                        </div>
                        <!-- Tab panes -->
                        <div class="pro-content-tab-wrap p40">
                            <div class="container-fluid">
                                <div class="tab-content">
                                    <!-- Overview Tab Starts -->
                                    <div role="tabpanel" class="tab-pane   active" id="overview">
                                        <div class="row">
                                            <!-- Left Column Starts -->
                                            <div class="col-md-6 left-col">
                                                <div class="tab-box">
                                                    <h5 class="tab-tit">Overview</h5>
                                                    <p class="tab-txt">{{ @$property->propertyDetails->propertyOverview }}</p>
                                                </div>
                                                <div class="tab-box my_highlights">
                                                    <h5 class="tab-tit">Highlights</h5>
                                                    <p class="tab-txt">{{ @$property->propertyDetails->propertyDetailsHighlights }}</p>
                                                </div>
                                            </div>
                                            <!-- Left Column Ends -->


                                            <!-- New Slider Design Start -->
                                            <div class="col-md-6 right-col">
                                                <div class="product-slider">
                                                    @if ($property->propertyImages)
                                                        <div id="carousel" class="carousel slide" data-ride="carousel">
                                                            <div class="carousel-inner">

                                                                @foreach ($property->propertyImages as $index => $image)
                                                                    <div class="item @if($index ==0) active @endif" data-thumb="{{$index}}"><img src="{{img($image->image)}}"/></div>
                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    @endif

                                                    <div class="clearfix">
                                                        <div id="thumbcarousel" class="carousel slide" data-interval="false">
                                                            <div class="carousel-inner">

                                                                @if($property->propertyImages)
                                                                    @foreach($property->propertyImages->chunk(5) as $index => $newImg)

                                                                        <div class="item @if($index == 0) active @endif">
                                                                            @foreach($newImg as $i => $img)

                                                                                <div data-target="#carousel" data-slide-to="{{$i}}" class="thumb showing"><img src="{{img($img->image)}}"/></div>
                                                                            @endforeach
                                                                        </div>

                                                                    @endforeach
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- New Slider Design End -->
                                        <div class="col-md-6 left-col">
                                            <h5 class="tab-tit">Location Details</h5>
                                            <h6 class="tab-tit1"> <i class="fas fa-map-marker-alt"></i> {{ @$property->propertyLocation }}</h6>
                                            <p class="tab-txt">{{ @$property->propertyLocationOverview }}</p>
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3886.9174864882493!2d80.23560251488352!3d13.040924016881664!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a526731ce8a4d87%3A0x4ea3363d1636abf6!2sOnePlus%20Experience%20Store!5e0!3m2!1sen!2sin!4v1608363527820!5m2!1sen!2sin" width="100%" height="450px" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                        </div>
                                        <br>
                                        
                                    </div>
                                    <!-- Overview Tab Ends -->
                                    <!-- Property Details Tab Starts -->
                                    <div role="tabpanel" class="tab-pane fade" id="property-details">
                                        <div class="row">
                                            <!-- Left Column Starts -->
                                            <div class="col-md-6 left-col">
                                                <!-- Documents Starts -->
                                                <table class="table due-table">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Geo Location</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyDetails->geo_location }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Street Address</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyDetails->street_address }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Floor Number</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyDetails->floor_number }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Area Name </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyDetails->area_name }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Pincode </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyDetails->pincode }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Year Of Construction</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyDetails->yearOfConstruction }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Community</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyDetails->community }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">No of bed</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyDetails->no_of_bed }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Facilities</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyDetails->facilities }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Facilitation fees </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyDetails->facilitation_fees }} %</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">PSF Rate </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyDetails->psf_rate }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Custodian</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyDetails->custodian }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Registration Fees </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyDetails->registration_fees }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Total Cost PSF</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyDetails->total_cost_psf }} sft</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Net Operating Yield Sale</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyDetails->net_operating_yield_sale }}%</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Net Realizable Yield Sale</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyDetails->net_realizable_yield_sale }} %</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Net Realizable Yield Purchase</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyDetails->net_realizable_yield_purchase }} %</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Stamp Duty </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyDetails->stamp_duty }} </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Sale Price </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyDetails->sale_price }}</a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!-- Documents Ends -->
                                            </div>

                                                <!-- New Slider Design Start -->
                                                <div class="col-md-6 right-col">
                                                    <video width="400" class="thumbnail" controls="controls">
                                                        <source src="{{ @img($property->propertyDetails->propertyVideo) }}">
                                                    </video>
                                                </div>


                                            </div>
                                            <!-- New Slider Design End -->
                                            <div class="tab-box col-md-12 my_highlights">
                                                <h5 class="tab-tit">Floor Plan</h5>
                                                <a href="{{ @img($property->propertyDetails->floorplan) }}" target="_blank"><i class="far fa-file-pdf fa-2x" target="_blank"></i> View</a>
                                            </div>
                                    </div>
                                    <!-- Property Details Tab End -->
                                    <!-- Property Documents Tab Starts -->
                                    <div role="tabpanel" class="tab-pane fade" id="property-documents">
                                        <div class="tab-box col-md-12 my_highlights">
                                            <h5 class="tab-tit">Prospectus</h5>
                                            <a href="{{ @img($property->propertyDocuments->prospectus) }}" target="_blank"><i class="far fa-file-pdf fa-2x" target="_blank"></i> View</a>
                                        </div>
                                        <div class="tab-box col-md-12 my_highlights">
                                            <h5 class="tab-tit">Title Report</h5>
                                            <a href="{{ @img($property->propertyDocuments->title_report) }}" target="_blank"><i class="far fa-file-pdf fa-2x" target="_blank"></i> View</a>
                                        </div>
                                        <div class="tab-box col-md-12 my_highlights">
                                            <h5 class="tab-tit">Subscription</h5>
                                            <a href="{{ @img($property->propertyDocuments->subscription) }}" target="_blank"><i class="far fa-file-pdf fa-2x" target="_blank"></i> View</a>
                                        </div>
                                        <div class="tab-box col-md-12 my_highlights">
                                            <h5 class="tab-tit">Due Diligence</h5>
                                            <a href="{{ @img($property->propertyDocuments->due_diligence) }}" target="_blank"><i class="far fa-file-pdf fa-2x" target="_blank"></i> View</a>
                                        </div>
                                        <div class="tab-box col-md-12 my_highlights">
                                            <h5 class="tab-tit">Updates</h5>
                                            <a href="{{ @img($property->propertyDocuments->updates) }}" target="_blank"><i class="far fa-file-pdf fa-2x" target="_blank"></i> View</a>
                                        </div>
                                        
                                    </div>
                                    <!-- Property Documents Tab End -->
                                    <!-- Property Team Tab Starts -->
                                    <div role="tabpanel" class="tab-pane fade" id="management-team">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h5 class="tab-tit">Management Team</h5>
                                                <p>{{ @$property->propertyDetails->ManagementTeamDescription }}</p>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row team-member-image">
                                            @foreach(@$property->propertyTeams as $member)
                                                <div class="col-sm-3 form-group">
                                                    <img src="{{ img(@$member->image) }}">
                                                    <div class="caption">
                                                        <h5 class="user-tit">{{ @$member->name }}</h5>
                                                        <p>{{ @$member->position }}</p>
                                                        <p>{{ @$member->description }}</p>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <!-- Property Team Tab End -->
                                    <!-- Property Information Tab Start -->
                                    <div role="tabpanel" class="tab-pane fade" id="property-information">
                                        <div class="row">
                                            <!-- Left Column Starts -->
                                            <div class="col-md-6 left-col">
                                                <!-- Documents Starts -->
                                                <table class="table due-table">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Over all area</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->overallarea }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Type of building</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->type_of_building }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Airport</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->airport }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Hospitals </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->hospitals }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Fire Services </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->fire_services }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Slums</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->slums }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Industrial</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->industrial }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Railway Tracks</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->railway_tracks }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Distance From Mainroad</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->distance_fm_mainroad }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Build Configuration </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->build_config }} </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Reception Hours </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->reception_hours }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">No of lifts</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->no_of_lifts }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Capasity of lifts </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->capasity_of_lifts }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Make of lifts</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->make_of_lifts }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Amenities avilable</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->amenities_avilable }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Seismic standard</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->seismic_standard }} </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Structure type</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->structure_type }} </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Slabs design </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->slabs_design }} </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Floor height </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->floor_height }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Food court seating</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->food_court_seating }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Food court working hours</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->food_court_working_hours }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Management operational hours</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->management_operational_hours }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">BMS System </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->bms_system }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">High side equipments</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->high_side_equipments }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Security surveillance</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->security_surveillance }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">CCTV installation</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->cctv_installation }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Demand load</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->demand_load }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Load calculation</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->load_calculation }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Transformer feeding </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->transformer_feeding }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Ventilation transformer </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->ventilation_transformer }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">DG Power</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->dg_power }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Generators </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->generators }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">DG kick off time</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->dg_kick_off_time }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Earthing Pits</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->earthing_pits }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Lift AC</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->lift_ac }} </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Facade Glass</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->facade_glass }} </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Chillers configuration </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->chillers_configuration }} </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Chiller timing </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->chiller_timing }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Water supply</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->water_supply }} </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Floor capacity</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->floor_capacity }} </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">State design condition </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->state_design_condition }} </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">Ventilation operational hours </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">{{ @$property->propertyInformation->ventilation_operational_hours }}</a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!-- Documents Ends -->
                                            </div>
                                    </div>
                                    <!-- Property Information Tab End -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Property Tab Ends -->
        </div>
    </div><!-- End cont inner page -->

    <!-- Footer Start -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <ul class="social">
                        <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="javascript:void(0)"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                    <p>{!! setting('site_copyright') !!}</p>
                </div>
            </div>
        </div>
    </footer>
    <!-- end Footer -->
    </div> <!-- End content-page -->

@endsection


@section('scripts')
    <!-- Owl -->
    <script src="https://direxct.com/asset/package/js/wizard/wizard.js"></script>
    <script src="/asset/package/js/owl.carousel.js"></script>
    <script>
        //$(".pro-slider-wrap").hide();
        $(window).load(function () {
            $(".loader").delay(2000).fadeOut(2000);
            $(".pro-slider-wrap").delay(2500).fadeIn(2500);
            //$(".pro-slider-wrap").delay(2000).show();

        });


        $(function () {
            $("#map").googleMap({
                                    zoom  : 15, // Initial zoom level (optional)
                                    coords: [17.438136, 78.395246], // Map center (optional)
                                    type  : "ROADMAP" // Map type (optional)
                                });
        })

    </script>

    <script>
        $('#carousel').on('slid.bs.carousel', function (e) {
            var active = $(this).find('.active').index();
            var to     = $(e.relatedTarget).index();

            $('#thumbcarousel').find('.showing').removeClass('showing');
            $('#thumbcarousel').find('[data-slide-to=' + active + ']').addClass('showing');

        });

        $('#carousel').on('slide.bs.carousel', function (e) {
            var active = $(this).find('.active').index();
            var to     = $(e.relatedTarget).index();

            if (active == 4 && to == 5) {
                $('#thumbcarousel').carousel('next');
            }
            if (active == 9 && to == 0) {
                $('#thumbcarousel').carousel('next');
            }
        });


        $('#carouselSecond').on('slid.bs.carousel', function (e) {
            var active = $(this).find('.active').index();
            var to     = $(e.relatedTarget).index();

            $('#thumbcarouselSecond').find('.showing').removeClass('showing');
            $('#thumbcarouselSecond').find('[data-slide-to=' + active + ']').addClass('showing');

        });

        $('#carouselSecond').on('slide.bs.carousel', function (e) {
            var active = $(this).find('.active').index();
            var to     = $(e.relatedTarget).index();

            if (active == 4 && to == 5) {
                $('#thumbcarouselSecond').carousel('next');
            }
            if (active == 9 && to == 0) {
                $('#thumbcarouselSecond').carousel('next');
            }
        });

        $('#carouselThird').on('slid.bs.carousel', function (e) {
            var active = $(this).find('.active').index();
            var to     = $(e.relatedTarget).index();

            $('#thumbcarouselThird').find('.showing').removeClass('showing');
            $('#thumbcarouselThird').find('[data-slide-to=' + active + ']').addClass('showing');

        });

        $('#carouselThird').on('slide.bs.carousel', function (e) {
            var active = $(this).find('.active').index();
            var to     = $(e.relatedTarget).index();

            if (active == 4 && to == 5) {
                $('#thumbcarouselThird').carousel('next');
            }
            if (active == 9 && to == 0) {
                $('#thumbcarouselThird').carousel('next');
            }
        });


    </script>
@endsection
