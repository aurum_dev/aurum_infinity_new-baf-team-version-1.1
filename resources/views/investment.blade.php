@extends('layout.appHome')

@section('content')

<style>
    .rt-fancy-text-box.element-one>.holder>.icon i{
        font-size: 55px !important;
    }
   .wraper_inner_banner {
    background-color: #f2f2f2;
    background-position: center top;
    background-image: url('public/asset/login/wp-content/uploads/2018/07/Blog-Banner-Background-Image.png');
    background-size: cover;
}
.rt-fancy-text-box.element-one>.holder {
  
    padding: 15px 6px 4px 16px !important;
    }
    .rt-fancy-text-box.element-one>.holder>.data .subtitle:before{
        width: 0 !important;
    }
    .rt-fancy-text-box.element-one>.holder>.data .subtitle{
        font-size: 40px !important;
        line-height: 88px !important;
    }
button.dt-button:first-child, div.dt-button:first-child, a.dt-button:first-child, input.dt-button:first-child {
    color: #ffffff !important;
    line-height: 25px !important;
    font-size: 14px !important;
    border: 2px solid #251d59 !important;
    background: #251d59 !important;
    margin-left: 0 !important;
}
button.dt-button, div.dt-button, a.dt-button, input.dt-button{
    color: #ffffff !important;
    line-height: 25px !important;
    font-size: 14px !important;
    border: 2px solid #251d59 !important;
    background: #251d59 !important;
    margin-left: 0 !important;
}
button.dt-button span.dt-down-arrow, div.dt-button span.dt-down-arrow, a.dt-button span.dt-down-arrow, input.dt-button span.dt-down-arrow{
    color: #fff !important;
}
table.dataTable thead th, table.dataTable tfoot th {
    font-weight: 500 !important;
}
.dataTables_wrapper .dataTables_paginate .paginate_button.disabled, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
    cursor: default;
    color: #251d59 !important;
    border: 1px solid #251d59 !important;
    background: #ebebeb !important;
    box-shadow: none;
    font-family: 'Poppins' !important;
    margin-bottom: 10px !important;
}
.rt-fancy-text-box.element-one>.holder{
   background-color: #dfdfdf !important;
}

</style>
 <style>
                                .neclass .rt-fancy-text-box.element-one>.holder:before{
    width: 80px !important;
    height: 109px !important;
    border-bottom-left-radius: 0 !important;
}

                            </style>
  <?php
$tokenCount=0;
$tokenAmt=0;
$property=array();
$tokenactive=0;
$propertyName=array();
foreach($wallet as $index => $value){
$tokenCount+=$value->total_tokens;
$tokenAmt+=$value->paybyinvestor;
$property[]=$value->property_id;

$propertyName[]=$value->propertyName."#".$value->property_id;
if($value->txnhash!=''){
   $tokenactive+=$value->total_tokens;
    
}

}


        /*  Property ID Uniqueness*/ 
        if(count($propertyName)!=0){
              $finlProp=array_unique($propertyName);  
              $finlimplode=implode(",",$finlProp);
              $finlExp=explode(",",$finlimplode); 
        }else{
               $finlExp=array();
        }  
       

             ?> 

             
           


               
<!-- Breadcrumb -->
<div class="page-content">


<div class="wraper_inner_banner">
   <div class="wraper_inner_banner_main">
      <div class="container">
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="inner_banner_main">
                  <p class="title" style="text-align:left;font-size:36px;">
                    Dashboard
                  </p>
                  <p class="subtitle" style="text-align:left;font-size:16px;color:#fff !important;text-align: left;">                                              
                                    <span style="font-size:25px;">Welcome {{@$user->name}}, </span> you now have access to greater financial details related<br> to our offerings, so you can make an informed investment decision.                                 
                   
                  </p>
                   <p class="subtitle" style="text-align:left;font-size:15px;color:#fff !important;text-align: left;">
                  
                           
                                
                       <a href="{{url('/')}}" class="pro-breadcrumbs-item" style="color:#cbc6c6;font-style:italic">Home</a>
                <span>/</span>
                <a href="#" class="pro-breadcrumbs-item" style="color:#cbc6c6;font-style:italic">Dashboard</a>
               
                
                  </p>
               </div>
            </div>
         </div>
      </div>
   </div>
</div> 
    

<div class="wpb_column vc_column_container vc_col-sm-12" style="padding:0 100px;margin-top:90px;">
      <div class="vc_column-inner">
         <div class="wpb_wrapper">
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
            <div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid vc_custom_1528367210886" style="opacity:1 !important;">
               <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4 vc_col-xs-12" style="margin-bottom:30px;">
                  <div class="vc_column-inner vc_custom_1528279916690">
                     <div class="wpb_wrapper">
                        <div class="rt-fancy-text-box element-one  rt1743060351 ">
                           <div class="holder matchHeight  vc_custom_1528785688438" style="height: 140px !important;">
                              <div class="icon"><i class="fas fa-building"></i> </div>
                              <div class="data">
                                 <p class="title"><a href="#" style="font-weight:500;">ASSETS</a></p>
                                 <p class="subtitle">{{count(array_unique($property))}}</p>
                                 
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4 vc_col-xs-12" style="margin-bottom:30px;">
                  <div class="vc_column-inner vc_custom_1528279934387">
                     <div class="wpb_wrapper">
                        <div class="rt-fancy-text-box element-one  rt1904688430 ">
                           <div class="holder matchHeight  vc_custom_1528785734363" style="height: 140px !important;">
                              <div class="icon"><i class="fa fa-money"></i> </div>
                              <div class="data">
                                 <p class="title"><a href="#" style="font-weight:500;">NET INVESTMENT</a></p>
                                 <p class="subtitle"><i class="fas fa-rupee-sign" style="margin-right:5px;"></i>{{$tokenAmt}}</p>
                                 
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               
               <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4 vc_col-xs-12" style="margin-bottom:30px;">
                  <div class="vc_column-inner vc_custom_1528279952057">
                     <div class="wpb_wrapper">
                        <div class="rt-fancy-text-box element-one  rt105464081 ">
                           <div class="holder matchHeight  vc_custom_1528785722203" style="height: 140px !important;">
                              <div class="icon"><i class="fas fa-coins"></i> </div>
                              <div class="data">
                                 <p class="title"><a href="#" style="font-weight:500;">TOTAL TOKENS</a></p>
                                 <p class="subtitle">{{$tokenCount}}</p>

                                
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
              
               
                              <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4 vc_col-xs-12" style="margin-bottom:30px;">
                  <div class="vc_column-inner vc_custom_1528279952057">
                     <div class="wpb_wrapper">
                        <div class="rt-fancy-text-box element-one  rt105464081 ">
                           <div class="holder matchHeight  vc_custom_1528785722203" style="height: 140px !important;">
                              <div class="icon"><i class="fas fa-coins"></i> </div>
                              <div class="data">
                                 <p class="title"><a href="#" style="font-weight:500;">ACTIVE TOKENS</a></p>
                                 <p class="subtitle">{{$tokenactive}}</p>

                                
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
        </div></div></div>

    <!-- Property Tab Starts -->
  
</div>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.1.0/css/buttons.dataTables.min.css">
    <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.html5.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.colVis.min.js"></script>

 <section class="container table-property">
  <p class="title" style="text-align:center;font-size:30px;padding-bottom:50px;color:#251d59;">
                   YOUR PORTFOLIO
                  </p>
                            <table id="example2" class="datatable-full table table-striped table-bordered custom-table-style" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>PORTFOLIO DETAILS</th>
                                        
                                    </tr>
                                </thead>


<?php 


for($i=0;$i<count($finlExp);$i++){

$propname=explode("#",$finlExp[$i]);
$totalT=0;
$totalA=0;
$totalAct=0;
$totalPending=0;
foreach($wallet as $index => $value){
            if($propname[1]==$value->property_id){
                $totalT+=$value->total_tokens;
                $totalA+=$value->paybyinvestor;
                if($value->txnhash!=''){
                    $totalAct+=$value->total_tokens;
                }else{
                    $totalPending+=$value->total_tokens;
                }
               
            }
    }
 
 ?>
                                <tr>
                                    
                                    <td>
                                         <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12 vc_col-xs-12" style="margin-bottom:30px;">
                  <div class="vc_column-inner vc_custom_1528279952057">
                     <div class="wpb_wrapper neclass">
                        <div class="rt-fancy-text-box element-one  rt105464081 ">
                           
                           <div class="holder matchHeight  vc_custom_1528785722203" style="height: 140px !important;">
                              <div class="icon"><a href="<?php echo "propertyDetail/".$propname[1]; ?>"><i class="fas fa-angle-double-right" style="font-size: 32px !important;
    padding-top: 15px;"></i> </a></div>
                              <div class="data">
                                 <p class="title" style="margin-bottom:10px"><a href="<?php echo "propertyDetail/".$propname[1]; ?>" style="font-weight:500;text-transform:uppercase;">{{$propname[0]}}</a></p>
                               <p class="title" style="text-align:left;font-size:18px;color:#251d59;margin-bottom:5px;text-transform: uppercase;">    <span style="font-size:15px;color:#6e04cf;">Total Tokens :</span>  {{$totalT}}  

                             &nbsp;&nbsp;  | &nbsp;&nbsp;<span style="font-size:15px;color:#6e04cf;">Active Tokens :</span>{{ $totalAct}}   &nbsp;&nbsp;|&nbsp;&nbsp;
                                <span style="font-size:15px;color:#6e04cf;">Pending Tokens :</span>{{ $totalPending}} 

 </p>

  <p class="title" style="text-align:left;font-size:18px;color:#251d59;margin-bottom:5px;text-transform: uppercase;">    <span style="font-size:15px;color:#6e04cf;">Total Amount :</span> <i class="fas fa-rupee-sign"></i> {{$totalA}}
 </p>
 
                                               </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
                                    </td>
                                </tr>

<?php } ?>
                                                          
                            </table>
<script>
    $(document).ready(function() {
    $('#example2').DataTable( {
        dom: 'Bfrtip',
       
    } );
} );

</script>
                        </section>
<!-- Property Tab Ends -->
@endsection


@section('scripts')

@endsection
