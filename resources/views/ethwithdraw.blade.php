@extends('layout.app')

@section('content') 
<!-- Breadcrumb -->
<div class="page-content">
    <div class="pro-breadcrumbs">
        <div class="container">
            <a href="{{url('/dashboard')}}" class="pro-breadcrumbs-item">Home</a>
            <span>/</span>
            <a href="#" class="pro-breadcrumbs-item">Withdraw ETH </a>
        </div>
    </div>
    <!-- End Breadcrumb -->
    <!-- Property Head Starts -->
    <div class="property-head grey-bg pt30">
        <div class="container">
            <div class="property-head-btm row">
                <div class="col-md-12">
                    <h2 class="pro-head-tit">Withdraw ETH</h2>
                    <p class="pro-head-txt">Hello, {{@ $user->name}}</p>
                </div>
            </div>
        </div>
    </div>
    <!-- Property Head Ends -->

    <section class="container spaceall wallet-full">
        <!-- Top Details -->
        @include('common.notify')
        <div class="container">

            <div class="panel panel-default">
                <div class="panel-body panel-currencies">

                    <div class="row">

                        <!-- Left Side Box-->
                        <div class="col-xs-4">
                            <div class="currencies-container nav nav-tabs">
                               
                                <!-- End Left Side Widget -->

                                <!-- Left Side Widget -->
                                <table class="table table-currencies introactive">
                                    <tbody>
                                        <tr class="currency-item" class="nav-item nav-link" href="#eth_deposit"   data-toggle="tab">

                                            <td class="currency-logo"><img src="{{asset('public/asset/package/images/wallet/icon-eth.png')}}" alt=""><span class="currency-symbol">ETH</span></td>
                                            <td class="currency-balance-col">
                                                <p class="currency-balance"><span class="currency-sign"></span>{{ number_format(@$ethbalance,6) }} ETH</p>
                                                {{-- <span class="currency-balance-locked"><i class="fa fa-lock"></i> 0.59216</span> --}}
                                            </td>
                                           
                                        </tr>
                                    </tbody>
                                </table>
                                <!-- End Left Side Widget -->
                            </div>
                        </div>
                        <!-- End Left Size Box -->

                        <!-- Right Size Box -->
                        <div class="col-xs-8">

                            <div class="details-container tab-content">
                                <!-- ETH WITHDRAWAL Tab -->
                                <div class="tab-pane active" id="btc_deposit">

                                    <h2 class="panel-title">WITHDRAW ETH</h2>
                                    <br>
                                    <!-- withdraw instruction -->
                                    <section class="withdraw-instruction">
                                        <div class="row">
                                            <div class="col-sm-1">
                                                <h4 class="steps">1</h4>
                                            </div>

                                            <div class="col-sm-11">
                                                <p> GWEI :- {{@$gwei}}</p>
                                                <p> CURRENT GAS FEE :- {{@$gasinether}}</p>
                                            </div>
                                        </div>

                                        <hr class="split">

                                        <div class="row">
                                            <div class="col-sm-1">
                                                <h4 class="steps">2</h4>
                                            </div>
                                            <div class="col-sm-11">
                                            <form id="sendCoinForm" action="{{ url('sendETH') }}" method="post">
					                		@csrf
					                		<div class="">								
                                            <div class="form-group">
								            	<label for="amount">Enter Amount</label>
								            	<input type="number" class="form-control" id="amount" name="amount" placeholder="Enter Amount" value="" required min="0" step="any">									
								            </div>
					                			<div class="form-group">
					                				<label for="address">Enter Address</label>
					                				<input type="text" class="form-control" id="address" name="address" placeholder="Enter Address" value="" required>									
					                			</div>
					                			<div class="form-group sectionHide" style="display: none;">
                                                    <label for="withdrawotp">Withdraw OTP</label>
					                				<input type="number" class="form-control withdrawotp" id="withdrawotp" name="withdrawotp" placeholder="Withdraw OTP" onkeydown="limit(this, 6);" onkeyup="limit(this, 6);" onkeyup="this.value = minmax(this.value, 0, 6)" required>
					                			</div>
					                			<div class="form-group">
					                				<button type="button" class="btn btn-theme sectionShow" id="generateOTP">Generate OTP</button>
					                				<input type="submit" class="btn btn-primary sectionHide" id="sendToken" style="display: none;" value="Send">
					                			</div>
					                		</div>														
					                	</form>
                                            <!-- <form method="post" action="{{ url('/sendETH') }}">
                                                @csrf

					                        	<div class="form-group">
					                        		<label for="sendAddr">To Address</label>
					                        		<input type="text" class="form-control" id="sendAddr" name="to_address" placeholder="To Address" required>
					                        	</div>
                                                <div class="form-group" id="withdrawotpsection" style="display: none;">
									            <input type="number" class="form-control withdrawotp" id="withdrawotp" name="withdrawotp" placeholder="Withdraw OTP" onkeydown="limit(this, 6);" onkeyup="limit(this, 6);" onkeyup="this.value = minmax(this.value, 0, 6)" required>
								                </div>
								                <div class="form-group">
								                	<button class="btn btn-theme" id="generateOTP">Generate OTP</button>
								                	<input type="submit" class="btn btn-primary" id="sendToken" style="display: none;" value="Send">
								                </div>
					                        	<button type="submit" class="btn btn-theme">Send ETH</button>
					                            </form>	 -->

                                            </div>
                                        </div>

                                        <hr class="split">

                                        <div class="row">
                                            <div class="col-sm-1">
                                                <h4 class="steps">3</h4></div>
                                            <div class="col-sm-11">
                                                <p>Once you complete sending, you can check the status of your new withdrawal below.</p>
                                            </div>
                                        </div>

                                    </section>

                                    <!-- End withdraw instruction -->

                                    <br>
                                    <h2 class="panel-title">Withdrawal History</h2>    

                                    <!-- Deposit History -->
                                    <table class="datatable-full table table-striped table-bordered custom-table-style" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Time</th>
                                                <th>Transaction ID</th>
                                                <th>Amount</th>
                                                <th>Status</th> 
                                            </tr>
                                        </thead> 
                                        <tbody>
                                        @foreach($history as $eth_history)
                                            <tr>
                                                <td>{{$eth_history->created_at}}</td>
                                                <td>
                                                    <a href="https://ropsten.etherscan.io/tx/{{ $eth_history->tx_hash }}" target="_blank">{{substr($eth_history->tx_hash, 0, 8).'****'}}</a>
                                                </td>
                                                <td>{{number_format($eth_history->amount,6)}}</td>
                                                <td>Success</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                     <!-- End Deposit History -->

                                </div>
                                <!-- End ETH WITHDRAWAL Tab -->

                            </div>
                        </div>

                    </div>
                    <!-- .row -->

                </div>
            </div>

        </div>

    </section>
    <div class="alert-container">
  
</div>

</div>
@endsection

@section('styles')
<style type="text/css">
	.introactive {
	    background: #232020 !important;
            background: #140749 !important; 
	    color: #FFF;
	}
	.introactive .nav-link {
	    color: #FFF;
	}



div#deposit_address {
    font-size: 13px;
    font-weight: 500;
    padding: 11px;
    color: #000;
}
.custom-table-style thead tr th {
    vertical-align: middle;
    font-size: 12px;
    padding: 8px !important;
}
.custom-table-style tbody tr td {
    vertical-align: middle;
    font-size: 12px;
    padding: 8px !important;
    letter-spacing: .1px;
}
.send-code-button button.btn.btn-primary {
    padding: 10px 20px;
    background: #5f56e0;
    border: 1px solid #5f56e0;
}
.selectable{
    -webkit-touch-callout: all; /* iOS Safari */
    -webkit-user-select: all; /* Safari */
    -khtml-user-select: all; /* Konqueror HTML */
    -moz-user-select: all; /* Firefox */
    -ms-user-select: all; /* Internet Explorer/Edge */
    user-select: all; /* Chrome and Opera */

} 
.alert-container { 
  position: fixed;
    bottom: 5px;
    left: 11%;
    width: 32%;
    margin: 0 25% 0 25%;
    background-color: #e0afaf;
    z-index: 1;
}

  .alert {
    text-align: center;
    padding: 17px 0 20px 0;
    margin: 0 25% 0 25%;
    height: 54px;
    font-size: 20px;
  }
</style>
@endsection

@section('scripts')

<script type="text/javascript" src="{{asset('js/jquery.qrcode.js')}}"></script>
<script type="text/javascript" src="{{asset('js/qrcode.js')}}"></script>
<script type="text/javascript">
    $('#generateOTP').click(function(e){
        $.ajax({
                   url: "{{url('/generate/withdrawOTP')}}?type=ETH",
                   type: "GET",
               }).done(function(response){
                   $('.sectionShow').hide();
                   $('.sectionHide').show();
                   alert(response.success.msg);
        }).fail(function(jqXhr,status){
        });
    });

    {{--    $('#generateOTP').click(function(e){--}}
    {{--        $('#generateOTP').attr("disabled", true);--}}
    {{--        $('alert').hide();--}}
    {{--        e.preventDefault();--}}
    {{--        $.ajaxSetup({--}}
    {{--            headers: {--}}
    {{--              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
    {{--            }--}}
    {{--        });--}}
    {{--        $.ajax({--}}
    {{--            url: "{{url('/generate/ethwithdraw')}}" ,--}}
    {{--            type: "POST",--}}
    {{--            data: $('#sendCoinForm').serialize(),--}}
    {{--            success: function(response, textStatus, xhr) {--}}
    {{--                $('.alert-danger').hide();--}}
    {{--                $('.alert-danger').html('');--}}
    {{--                $('#withdrawotpsection').show();--}}
    {{--                $('#generateOTP').hide();--}}
    {{--                $('#sendToken').show();--}}
    {{--                $('#sendCoinForm').attr('method','POST');--}}
    {{--                $('#sendCoinForm').attr('action',"{{ url('/sendETH') }}");--}}
    {{--                $('.alert-success').show();--}}
    {{--                $('.alert-success').html('');--}}
    {{--                $('.alert-success').html('<p><i class="fa fa-check"></i>'+response.success.msg+'</p>');--}}
    {{--            },--}}
    {{--            error: function (jqXHR, textStatus, errorThrown) {--}}
    {{--                $('.alert-danger').html('');--}}

    {{--                console.log(jqXHR);--}}

    {{--                console.log(jqXHR.status);--}}
    {{--                console.log(textStatus);--}}

    {{--                console.log(errorThrown);--}}
    {{--                if(jqXHR.status == 400){--}}
    {{--                    var errors = $.parseJSON(jqXHR.responseText);--}}
    {{--                    console.log(errors);--}}
    {{--                    console.log(errors.error);--}}
    {{--                    console.log(errors.error.msg);--}}


    {{--                    $('#alert-danger').html(errors.error.msg);--}}
    {{--                    $('#generateOTP').attr("disabled", false);--}}
    {{--                }else{--}}
    {{--                    var errors = $.parseJSON(jqXHR.responseText);--}}
    {{--                    console.log(errors);--}}
    {{--                    console.log(errors.errors);--}}
    {{--                    var errorString = '<ul>';--}}
    {{--                    $.each(errors.errors, function( key, value) {--}}
    {{--                        errorString += '<li>' + value + '</li>';--}}
    {{--                    });--}}
    {{--                    errorString += '</ul>';--}}
    {{--                    // alert(errorString);--}}
    {{--                    $('#alert-danger').append(errorString);--}}
    {{--                    $('#alert-danger').show()--}}
    {{--                    $('#generateOTP').attr("disabled", false);--}}
    {{--                }--}}
    {{--            }--}}
    {{--        });--}}
	{{--});--}}

</script>
@endsection
