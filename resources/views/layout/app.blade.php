<!DOCTYPE html>
<html>
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="keywords" content="Real Estate" />
    <meta name="description" content="Real Estate">
    <meta name="author" content="STO">
    <title>Real Estate STO | Build Your Real Estate Portfolio</title>
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Google Webfonts -->

    <!-- Style -->
    @include('include.styles')
    <!-- End Style -->

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">


    <div class="body black_menu_me">
        <!-- Header -->
        @include('include.header')
        <!-- End Header -->

        @yield('content')  
    </div> 
    <!-- Footer -->
    @include('include.footer-new')

    <!-- End Footer -->

  <!-- Scripts -->
  @include('include.scripts')
</body>
</html>