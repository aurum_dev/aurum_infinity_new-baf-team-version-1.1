
<!-- Footer content -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 <footer class="wraper_footer style-six">
            <!-- wraper_footer_main -->
            <div class="wraper_footer_main">
               <div class="container">
                  <!-- row -->
                  <div class="row footer_main">
                     <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12" style="    color: #fff;
    font-size: 32px;line-height: 40px;
    padding-bottom: 60px;
    font-family: 'Poppins';
    text-align: center;">Democratizing the Real Estate</div>
                     <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="footer_main_item matchHeight">
                           <section id="nav_menu-8" class="widget widget_nav_menu">
                              <h5 class="widget-title">About Us</h5>
                              <div class="menu-important-links-container">
                                 <ul id="menu-important-links" class="menu rt-mega-menu-transition-slide">
                                 <li id="menu-item-344" class="menu-item menu-item-type-post_type menu-item-object-page menu-flyout rt-mega-menu-hover item-344"><a href="https://www.aurumproptech.com" target="_blank">- Aurum Premium Listings </a></li>
                                    <li id="menu-item-1012" class="menu-item menu-item-type-post_type menu-item-object-page menu-flyout rt-mega-menu-hover item-1012"><a target="_blank" href="https://www.aurumventures.in" target="_blank">- Aurum Ventures </a></li>
                                    <li id="menu-item-345" class="menu-item menu-item-type-post_type menu-item-object-page menu-flyout rt-mega-menu-hover item-345"><a target="_blank" href="https://aurumproptech.in" target="_blank">- Aurum PropTech </a></li>
                                                                    </ul>
                              </div>
                           </section>
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="footer_main_item matchHeight">
                           <section id="nav_menu-8" class="widget widget_nav_menu">
                              <h5 class="widget-title">Latest Launch</h5>
                              <div class="menu-important-links-container">
                                 <ul id="menu-important-links" class="menu rt-mega-menu-transition-slide">
                                    <li id="menu-item-338" class="menu-item menu-item-type-post_type menu-item-object-page menu-flyout rt-mega-menu-hover item-338"><a  href="https://seolounge.radiantthemes.com/about-us/" target="_blank">- Aurum Q5</a></li>
                                        </ul>
                              </div>
                           </section>
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="footer_main_item matchHeight">
                           <section id="nav_menu-9" class="widget widget_nav_menu">
                              <h5 class="widget-title">Our Solutions</h5>
                              <div class="menu-featured-services-container">
                                 <ul id="menu-featured-services" class="menu rt-mega-menu-transition-slide">
                                    <li id="menu-item-344" class="menu-item menu-item-type-post_type menu-item-object-page menu-flyout rt-mega-menu-hover item-344"><a target="_blank" href="https://www.sell.do/" target="_blank">- Sell.do</a></li>
                                    <li id="menu-item-1012" class="menu-item menu-item-type-post_type menu-item-object-page menu-flyout rt-mega-menu-hover item-1012"><a target="_blank" href="https://kylas.io/" target="_blank">- Kylas</a></li>
                                    <li id="menu-item-345" class="menu-item menu-item-type-post_type menu-item-object-page menu-flyout rt-mega-menu-hover item-345"><a target="_blank" href="https://www.aurumproptech.com/crex/" target="_blank">- CREX</a></li>
                                    <li id="menu-item-343" class="menu-item menu-item-type-post_type menu-item-object-page menu-flyout rt-mega-menu-hover item-343"><a target="_blank" href="http://www.integrowamc.com/" target="_blank">- Integrow Asset Management</a></li>
                                  
                                 </ul>
                              </div>
                           </section>
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="footer_main_item matchHeight">
                           <section id="text-5" class="widget widget_text">
                              <h5 class="widget-title">Contact Us</h5>
                              <div class="textwidget">
                                 <p>
                                    Aurum Q Parć, Thane Belapur Road,<br>Ghansoli, Navi Mumbai, 400710
                                 </p>
                                                        
                                                                                                                                                                                                                                                                                                                                                                                                                            </ul>
                        
                    </div>
                    
                              </div>
                           </section>
                        </div>
                     </div>
                  </div>
                  <!-- row -->
               </div>
            </div>
            <!-- wraper_footer_main -->
            <!-- wraper_footer_copyright -->
            <div class="wraper_footer_copyright" style="background: #1a2024;">
               <div class="container">
                  <!-- row -->
                  <div class="row footer_copyright">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
                    <!-- footer_copyright_item -->
                    <div class="footer_copyright_item" style="text-align:center;">
                        <p>Copyright © 2021 {{Setting::get('site_title','Aurum')}} | All Rights Reserved | <a href="privacy" style="color:#8b22cd;">Privacy Policy</a> | <a href="terms_conditions" style="color:#8b22cd;">Terms & Conditions</a></p>
                    </div>
                    <!-- footer_copyright_item -->
                </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
                    
                    <div class="footer_copyright_item">
                                <style>
                                .footer_copyright_item ul.social li a:hover{
                                font-size: 20px;
    border-radius: 94%;
    padding: 1em;
    background-color: rgba(255,255,255,.1);
}

                                }
                                
                                </style>            
                        <ul class="social" style="text-align:center;">
                                                            <li class="google-plus"><a href="https://www.instagram.com/aurumproptech" rel="publisher" target="_blank"><i class="fa fa-instagram" style="font-size:30px;"></i></a></li>
                                                                                        <li class="facebook"><a href="https://www.facebook.com/AurumPropTech" target="_blank"><i class="fa fa-facebook-square" style="font-size:30px;"></i></a></li>
                                                                                        <li class="twitter"><a href="https://www.linkedin.com/company/71413509/admin" target="_blank"><i class="fa fa-linkedin-square" style="font-size:30px;"></i></a></li>-->
                                                                                       <!-- <li class="vimeo"><a href="https://vimeo.com/" target="_blank"><i class="fa fa-twitter-square" style="font-size:30px;"></i></a></li>-->
                                                                                                                                                                                                                                                                                                                                                                                                                                </ul>
                        
                    </div>
                    
                </div>
            </div>
                  <!-- row -->
               </div>
            </div>
            <!-- wraper_footer_copyright -->
         </footer>

           </div>
      <!-- radiantthemes-website-layout -->
      <script type="text/html" id="wpb-modifications"></script> <script type="text/javascript">
         (function () {
            var c = document.body.className;
            c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
            document.body.className = c;
         })();
      </script>
      <link rel='stylesheet' id='radiantthemes-addons-custom-css'  href="{{asset('public/asset/login/wp-content/plugins/radiantthemes-addons/assets/css/radiantthemes-addons-custom.min9f97.css?ver=1638778739')}}" type='text/css' media='all' />
      <style id='radiantthemes-addons-custom-inline-css' type='text/css'>
         .radiantthemes-custom-button.rt810073130 .radiantthemes-custom-button-main{ background:linear-gradient(to right, #251d59 0%, #4a3259 100%);}
         .radiantthemes-custom-button.rt172900896 .radiantthemes-custom-button-main{ background:linear-gradient(to right, #251d59 0%, #4a3259 100%);}
         .radiantthemes-custom-button.rt537002223 .radiantthemes-custom-button-main{ background:linear-gradient(to right, #251d59 0%, #4a3259 100%);}
         .radiantthemes-custom-button.rt636586625 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt2116519540 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt239276426 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt1033353997 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt143729880 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt638223915 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt729187609 .radiantthemes-custom-button-main{ background:linear-gradient(to right, #251d59 0%, #4a3259 100%);}
         .checked { color: orange;}
         .rt-pricing-table.element-nine.rt842858723 > .holder > .spotlight-tag > .spotlight-tag-text,
         .rt-pricing-table.element-nine.spotlight.rt842858723 > .holder > .more .btn{
         background: linear-gradient( to right, #ff7816 0%, #ff590f 100% ) ;
         }
         .rt-pricing-table.element-nine.rt842858723 > .holder > .more .btn{
         border-color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt842858723 > .holder > .more .btn{
         color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt1296861313 > .holder > .spotlight-tag > .spotlight-tag-text,
         .rt-pricing-table.element-nine.spotlight.rt1296861313 > .holder > .more .btn{
         background: linear-gradient( to right, #ff7816 0%, #ff590f 100% ) ;
         }
         .rt-pricing-table.element-nine.rt1296861313 > .holder > .more .btn{
         border-color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt1296861313 > .holder > .more .btn{
         color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt1087964127 > .holder > .spotlight-tag > .spotlight-tag-text,
         .rt-pricing-table.element-nine.spotlight.rt1087964127 > .holder > .more .btn{
         background: linear-gradient( to right, #ff7816 0%, #ff590f 100% ) ;
         }
         .rt-pricing-table.element-nine.rt1087964127 > .holder > .more .btn{
         border-color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt1087964127 > .holder > .more .btn{
         color: #ff7816 ;
         }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row, .radiant-contact-form.rt5d04c30e5c56e06 div.wpcf7-response-output{
         }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=submit], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=button],.radiant-contact-form.rt5d04c30e5c56e06 .form-row button[type=submit] {   linear-gradient(to right, #251d59 0%, #4a3259 100%); color:#ffffff; border-top:0px #465579; border-right:0px #465579; border-bottom:0px #465579; border-left:0px #465579; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=submit]:hover, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=button]:hover, .radiant-contact-form.rt5d04c30e5c56e06 .form-row button[type=submit]:hover { background:linear-gradient(to right, #251d59 0%, #4a3259 100%)  }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file], .radiant-contact-form.rt5d04c30e5c56e06 .form-row select, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea {   background-color: #ffffff; padding-top:40px; padding-right:40px; padding-bottom:18px; padding-left:10px; border-radius: 0px 0px 0px 0px; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file], .radiant-contact-form.rt5d04c30e5c56e06 .form-row select, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea {   color  : #b8b8b8; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row select:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea:focus {   color:#252525; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row select:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea:focus {   border-top:0px solid #f5f4f4; border-right:0px solid #f5f4f4; border-bottom:2px solid #f5f4f4; border-left:0px solid #f5f4f4; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file], .radiant-contact-form.rt5d04c30e5c56e06 .form-row select, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea {   border-top:0px solid #f5f4f4; border-right:0px solid #f5f4f4; border-bottom:2px solid #f5f4f4; border-left:0px solid #f5f4f4; }
      </style>
      <link rel='stylesheet' id='vc_animate-css-css'  href="{{asset('public/asset/login/wp-content/plugins/js_composer/assets/lib/bower/animate-css/animate.mine6df.css?ver=6.5.0')}}" type='text/css' media='all' />
      <script type='text/javascript' src="{{asset('public/asset/login/wp-includes/js/dist/vendor/wp-polyfill.min89b1.js?ver=7.4.4')}}" id='wp-polyfill-js'></script>
     <script type='text/javascript' id='wp-polyfill-js-after'>
         ( 'fetch' in window ) || document.write( '<script src="asset/login/wp-includes/js/dist/vendor/wp-polyfill-fetch.min6e0e.js?ver=3.0.0"></scr' + 'ipt>' );( document.contains ) || document.write( '<script src="wp-includes/js/dist/vendor/wp-polyfill-node-contains.min2e00.js?ver=3.42.0"></scr' + 'ipt>' );( window.DOMRect ) || document.write( '<script src="asset/login/wp-includes/js/dist/vendor/wp-polyfill-dom-rect.min2e00.js?ver=3.42.0"></scr' + 'ipt>' );( window.URL && window.URL.prototype && window.URLSearchParams ) || document.write( '<script src="asset/login/wp-includes/js/dist/vendor/wp-polyfill-url.min5aed.js?ver=3.6.4"></scr' + 'ipt>' );( window.FormData && window.FormData.prototype.keys ) || document.write( '<script src="asset/login/wp-includes/js/dist/vendor/wp-polyfill-formdata.mine9bd.js?ver=3.0.12"></scr' + 'ipt>' );( Element.prototype.matches && Element.prototype.closest ) || document.write( '<script src="asset/login/wp-includes/js/dist/vendor/wp-polyfill-element-closest.min4c56.js?ver=2.0.2"></scr' + 'ipt>' );
      </script>
   
      <script type='text/javascript' src="{{asset('public/asset/login/wp-content/plugins/radiantthemes-addons/assets/js/radiantthemes-addons-core.min9f97.js?ver=1638778739')}}" id='radiantthemes-addons-core-js'></script>
      <script type='text/javascript' src="{{asset('public/asset/login/wp-content/plugins/radiantthemes-addons/assets/js/radiantthemes-addons-custom.min9f97.js?ver=1638778739')}}" id='radiantthemes-addons-custom-js'></script>
      <script type='text/javascript' src="{{asset('public/asset/login/wp-content/plugins/radiantthemes-mega-menu/assets/js/rt-megamenu3f90.js?ver=5.6.6')}}" id='rt-megamenu-front-end-js-js'></script>
      <script type='text/javascript' src="{{asset('public/asset/login/wp-content/themes/seolounge/js/radiantthemes-core.min3f90.js?ver=5.6.6')}}" id='radiantthemes-core-js'></script>
      <script type='text/javascript' src="{{asset('public/asset/login/wp-content/plugins/js_composer/assets/lib/bower/isotope/dist/isotope.pkgd.mine6df.js?ver=6.5.0')}}" id='isotope-js'></script>
      <script type='text/javascript' src="{{asset('public/asset/login/wp-content/themes/seolounge/js/radiantthemes-blog-style3f90.js?ver=5.6.6')}}" id='radiantthemes-blog-style-js'></script>
      <script type='text/javascript' src="{{asset('public/asset/login/wp-content/themes/seolounge/js/radiantthemes-custom3f90.js?ver=5.6.6')}}" id='radiantthemes-custom-js'></script>
      <script type='text/javascript' src="{{asset('public/asset/login/wp-includes/js/wp-embed.min3f90.js?ver=5.6.6')}}" id='wp-embed-js'></script>
      <script type='text/javascript' src="{{asset('public/asset/login/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.mine6df.js?ver=6.5.0')}}" id='wpb_composer_front_js-js'></script>
      <script type='text/javascript' src="{{asset('public/asset/login/wp-content/plugins/js_composer/assets/lib/vc_waypoints/vc-waypoints.mine6df.js?ver=6.5.0')}}" id='vc_waypoints-js'></script>
    <script>
   var owl = $('.owl-carousel');
owl.owlCarousel({
    items:4,
    loop:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:6000,
    autoplayHoverPause:true
});
$('.play').on('click',function(){
    owl.trigger('play.owl.autoplay',[6000])
})
$('.stop').on('click',function(){
    owl.trigger('stop.owl.autoplay')
})
    </script>

