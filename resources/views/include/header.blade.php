<header class="wraper_header style-twelve static-header">
            <!-- wraper_header_top -->
           
            <!-- wraper_header_top -->
            <!-- wraper_header_main -->
            <!-- wraper_header_main -->
            <div class="wraper_header_main i-am-sticky">
               <div class="container">
                  <!-- header_main -->
                  <div class="header_main">
                     <!-- brand-logo -->
                     <div class="brand-logo">
                        <div class="brand-logo-table">
                           <div class="brand-logo-table-cell">
                              <a href="{{url('/')}}"><img src="{{asset('public/asset/login/wp-content/uploads/2018/07/logo.png')}}" alt=""></a>
                           </div>
                        </div>
                     </div>
                     <!-- brand-logo -->
                     <!-- responsive-nav -->
                     <div class="responsive-nav hidden-lg hidden-md visible-sm visible-xs">
                        <i class="fa fa-bars"></i>
                     </div>
                     <!-- responsive-nav -->
                     <!-- header_main_action -->
                     <!-- header_main_action -->
                     <!-- nav -->
                     <nav class="nav visible-lg visible-md hidden-sm hidden-xs">
                        <div class="menu-header-menu-container">
                           <ul id="menu-header-menu" class="menu rt-mega-menu-transition-slide">
                               @if(Auth::check())

                              <li id="menu-item-4564" class="home-megamenu-new menu-item menu-item-type-custom menu-item-object-custom 
                                 rt-mega-menu-full-width rt-mega-menu-hover item-4564"><a href="{{ url('/dashboard') }}">Dashboard</a>
                              </li>
                              <li id="menu-item-4564" class="home-megamenu-new menu-item menu-item-type-custom menu-item-object-custom 
                                 rt-mega-menu-full-width rt-mega-menu-hover item-4564"><a href="{{ url('/home#works') }}">How It Works</a>
                              </li>
                              <li id="menu-item-4564" class="home-megamenu-new menu-item menu-item-type-custom menu-item-object-custom 
                                 rt-mega-menu-full-width rt-mega-menu-hover item-4564"><a href="{{url('/propertyList')}}">Offerings</a>
                              </li>
                          
                              <li id="menu-item-899" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-flyout rt-mega-menu-hover item-899">
                                 <a href="#">My Account</a>
                                 <ul class="">
                                    <li id="menu-item-16" class="menu-item menu-item-type-post_type menu-item-object-page rt-mega-menu-hover item-16"><a href="{{url('/profile')}}"><b style="color:#8b36cb;">Welcome,</b> {{Auth::user()->name}} </a></li>
                                    <li id="menu-item-16" class="menu-item menu-item-type-post_type menu-item-object-page rt-mega-menu-hover item-16"><a href="{{url('/profile')}}"> <i class="fa fa-user"></i>&nbsp;Profile</a></li>
                                     <li id="menu-item-16" class="menu-item menu-item-type-post_type menu-item-object-page rt-mega-menu-hover item-16"><a href="{{url('/payment_history')}}"><i class="fa fa-money"></i>&nbsp;Payment History</a></li>
                                     <li id="menu-item-16" class="menu-item menu-item-type-post_type menu-item-object-page rt-mega-menu-hover item-16"><a href="{{url('/help')}}"><i class="fa fa-headphones"></i>&nbsp;Help</a></li>
                                   <!--   <li id="menu-item-16" class="menu-item menu-item-type-post_type menu-item-object-page rt-mega-menu-hover item-16"><a href="{{url('/investment')}}">Investments</a></li>-->
                                    <li id="menu-item-4564" class="home-megamenu-new menu-item menu-item-type-custom menu-item-object-custom 
                                 rt-mega-menu-full-width rt-mega-menu-hover item-4564"><a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i>&nbsp;Logout</a>
                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>

                              </li>
                                 </ul>

                              </li>
                             
                              @else

                              <li id="menu-item-4564" class="home-megamenu-new menu-item menu-item-type-custom menu-item-object-custom 
                                 rt-mega-menu-full-width rt-mega-menu-hover item-4564"><a href="{{ url('/') }}">Home</a>
                              </li>
                              <li id="menu-item-4564" class="home-megamenu-new menu-item menu-item-type-custom menu-item-object-custom 
                                 rt-mega-menu-full-width rt-mega-menu-hover item-4564"><a href="{{ url('/#works') }}">How It Works</a>
                              </li>
                              <li id="menu-item-4564" class="home-megamenu-new menu-item menu-item-type-custom menu-item-object-custom 
                                 rt-mega-menu-full-width rt-mega-menu-hover item-4564"><a href="{{ url('/#properties') }}">Offerings</a>
                              </li>
                            
                              <li id="menu-item-4564" class="home-megamenu-new menu-item menu-item-type-custom menu-item-object-custom 
                                 rt-mega-menu-full-width rt-mega-menu-hover item-4564"><a href="{{url('issuer/login')}}">Issuer Login</a>
                              </li>
                              <li id="menu-item-4564" class="home-megamenu-new menu-item menu-item-type-custom menu-item-object-custom 
                                 rt-mega-menu-full-width rt-mega-menu-hover item-4564"><a href="{{url('/login')}}">Investor Login</a>
                              </li>
                               
                            @endif
                           </ul>
                        </div>
                     </nav>
                     <!-- nav -->
                     <div class="clearfix"></div>
                  </div>
                  <!-- header_main -->
               </div>
            </div>
            <!-- wraper_header_main -->
         </header>

          <!-- mobile-menu -->
         <div class="mobile-menu hidden">
            <!-- mobile-menu-main -->
            <div class="mobile-menu-main">
               <!-- mobile-menu-close -->
               <div class="mobile-menu-close">
                  <i class="fa fa-times"></i>
               </div>
               <!-- mobile-menu-close -->
               <!-- mobile-menu-nav -->
               <nav class="mobile-menu-nav">
                  <div class="menu-header-menu-container">
                     <ul id="menu-header-menu-1" class="menu rt-mega-menu-transition-slide">
                        <li class="home-megamenu-new menu-item menu-item-type-custom menu-item-object-custom rt-mega-menu-full-width rt-mega-menu-hover item-4564">
                           <a  href="#">Home</a>
                        </li>
                        <li class="home-megamenu-new menu-item menu-item-type-custom menu-item-object-custom rt-mega-menu-full-width rt-mega-menu-hover item-4564">
                           <a  href="#">How It Works</a>
                        </li>
                        <li class="home-megamenu-new menu-item menu-item-type-custom menu-item-object-custom rt-mega-menu-full-width rt-mega-menu-hover item-4564">
                           <a  href="#">Offerings</a>
                        </li>
                        <li class="home-megamenu-new menu-item menu-item-type-custom menu-item-object-custom rt-mega-menu-full-width rt-mega-menu-hover item-4564">
                           <a  href="#">Issuer Login</a>
                        </li>
                        <li class="home-megamenu-new menu-item menu-item-type-custom menu-item-object-custom rt-mega-menu-full-width rt-mega-menu-hover item-4564">
                           <a  href="#">Investor Login</a>
                        </li>
                     </ul>
                  </div>
               </nav>
               <!-- mobile-menu-nav -->
            </div>
            <!-- mobile-menu-main -->
         </div>
         <!-- mobile-menu -->
         <!-- wraper_header_bannerinner -->
          <div class="wraper_inner_banner">
         </div>
         <!-- wraper_header_bannerinner -->
         <!-- wraper_header_bannerinner -->
