<!-- @if(Auth::check())
    <footer class="footer">
@else
    <footer class="footer loginFooter">
@endif


        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <ul class="list-inline">
                        <li><a href="#">Disclaimer</a></li>
                        <li><a href="#">Terms</a></li>
                        <li><a href="#">Privacy</a></li>
                        <li><a href="#">Consent Notice</a></li>
                    </ul>
                </div>
                <div class="col-12 text-center">
                    <p class="copy-right">Copyright &copy; Liquid Token <?php echo date('Y'); ?></p>
			</div>
		</div>
	</div>
</footer> -->

<link rel="stylesheet" type="text/css" href="{{asset('public/asset/login/css/chat_style.css')}}">

<div class="section footer-classic">
    <div class="innfoter-bg"></div>
    <div class="container">
        <div class="row row-30">
            <div class="col-md-3">
                <h5>About</h5>
                <ul class="nav-list">
                    <li><a href="#">Our platform</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Terms of use</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <h5>Get Started</h5>
                <ul class="nav-list">
                    <li><a href="#">Create profile</a></li>
                    <li><a href="#">Contact us</a></li>
                <!-- <li><a href="{{url('/issuer/login')}}" target="_blank">Issuers</a></li> -->
                </ul>
            </div>
            <div class="col-md-3">
                <h5>Resources</h5>
                <ul class="nav-list">
                    <li><a href="#">White paper</a></li>
                    <li><a href="#">TransX</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <h5>Invest</h5>
                <ul class="nav-list">
                    <li><a href="{{url('login')}}">Become a member</a></li>
                    <li><a href="#">Featured Property</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Footer content -->
<footer>
    <ul class="social">
        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
        <li><a href="#"><i class="fab fa-dribbble"></i></a></li>
        <li><a href="#"><i class="fab fa-skype"></i></a></li>
    </ul>
    <p class="copy">{!! setting('site_copyright') !!}<a href="#"></a></p>
</footer>


<!-- Chat Ui Start -->
<div id="chat-circle" class="btn btn-raised" style="display:none;">
    <div id="chat-overlay"></div>
    <i class="fa fa-comments" style="font-size:15px;"></i>
</div>

<div class="chat-box animated bounceIn" style="display:none;">
    <div class="chat-box-header">Online<span class="chat-box-toggle"><i class="fa fa-times"></i></span>
    </div>
    <div class="chat-box-body">
        <div class="chat-box-overlay">
        </div>
        <div class="chat-logs">

        </div><!--chat-log -->
    </div>
    <div class="chat-input">
        <form>
            <input type="text" id="chat-input" placeholder="Send a message..."/>
            <button type="submit" class="chat-submit" id="chat-submit"><i class="fa fa-paper-plane"></i></button>
        </form>
    </div>
</div>

<!--Start of Tawk.to Script-->
{{--<script type="text/javascript">--}}
{{--    var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();--}}
{{--    (function () {--}}
{{--        var s1     = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];--}}
{{--        s1.async   = true;--}}
{{--        s1.src     = 'https://embed.tawk.to/6017feccc31c9117cb749b13/1etermjsm';--}}
{{--        s1.charset = 'UTF-8';--}}
{{--        s1.setAttribute('crossorigin', '*');--}}
{{--        s0.parentNode.insertBefore(s1, s0);--}}
{{--    })();--}}
{{--</script>--}}
<!--End of Tawk.to Script-->
<!-- Chat Ui End -->

<script type="text/javascript" src="{{asset('public/asset/login/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/asset/login/js/chat_script.js')}}"></script>
<!-- Footer content -->
