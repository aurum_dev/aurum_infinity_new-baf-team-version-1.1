@extends('admin.layout.base')

@section('title', 'Change Password ')

@section('content')
<script>
	function RestrictSpace() {
    if (event.keyCode == 32) {
        return false;
    }
}

</script>
<div class="breadcrumb-header justify-content-between">
                    <div class="my-auto">
                        <div class="d-flex">
                            <h4 class="content-title mb-0 my-auto">Change Password</h4>
                        </div>
                    </div>                    
                </div>
<div class="content-area py-1" style="background:#fff;padding-top: 40px !important;">
    <div class="container-fluid">
    	<div class="box box-block bg-white">

            <form class="form-horizontal" action="{{route('admin.password.update')}}" method="POST" role="form">
            	{{csrf_field()}}

            	<div class="form-group row">
					<label for="old_password" class="col-xs-12 col-form-label">@lang('admin.account.old_password')</label>
					<div class="col-xs-10">
						<input class="form-control" type="password" name="old_password" id="old_password" placeholder="Old Password" onkeypress="return RestrictSpace()">
					</div>
				</div>

				<div class="form-group row">
					<label for="password" class="col-xs-12 col-form-label">@lang('admin.account.password')</label>
					<div class="col-xs-10">
						<input class="form-control" type="password" name="password" id="password" placeholder="New Password" onkeypress="return RestrictSpace()">
					</div>
				</div>

				<div class="form-group row">
					<label for="password_confirmation" class="col-xs-12 col-form-label">@lang('admin.account.password_confirmation')</label>
					<div class="col-xs-10">
						<input class="form-control" type="password" name="password_confirmation" id="password_confirmation" placeholder="Re-type New Password" onkeypress="return RestrictSpace()">
					</div>
				</div>

				<div class="form-group row">
					<div class="col-xs-12">
						<button type="submit" style="float:right;" class="btn btn-primary">@lang('admin.account.change_password')</button>
					</div>
				</div>

			</form>
		</div>
    </div>
</div>

@endsection
@section('scripts')
<script type="text/javascript">
$(document).on("ready", function(){
	$('input').keypress(function( e ) {
		if(e.which == 32)
			return false;
	});
});
</script>
@endsection