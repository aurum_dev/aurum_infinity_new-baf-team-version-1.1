@extends('admin.layout.base')

@section('title', 'Support')

@section('content')

<style type="text/css">
    .status_btn:hover{ opacity: 1 !important; }
</style>

<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
         
            <h3> Support</h3>
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>Id</th>                        
                        <th>Email</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Status/Action</th>                        
                    </tr>
                </thead>
                <tbody>
                    @foreach($support as $index=>$support)
                        <tr>                        
                            <td>{{ $index + 1 }}</td>                        
                            <td>@if($support->user) {{$support->user->email}} @endif</td>
                            <td>{{$support->title}}</td>
                            <td>{{$support->description}}</td>
                            <td>
                                
                                <a href="{{url('/admin/supportdone')}}/{{$support->id}}/0"  class="btn btn-warning status_btn" @if($support->status == "0") style="padding:15px;font-weight:bold;" @else style="opacity:0.5;" @endif >Opened</a>
                                
                                <a href="{{url('/admin/supportdone')}}/{{$support->id}}/1"  class="btn btn-success status_btn" @if($support->status == "1") style="padding:15px;font-weight:bold;" @else style="opacity:0.5;" @endif >Closed</a>
                                
                                <a href="{{url('/admin/supportdone')}}/{{$support->id}}/2" class="btn btn-danger status_btn" @if($support->status == "2") style="padding:15px;font-weight:bold;" @else style="opacity:0.5;" @endif >Reopened</a>
                                
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>Id</th>                        
                        <th>Email</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Status/Action</th>                     
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection