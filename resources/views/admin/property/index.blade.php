@extends('admin.layout.base')

@section('title', 'Property List')

@section('content')
<style>
     
#example_length{
margin-top: 5px !important;
    float: right !important;
    margin-right: 250px !important;
}


</style>


<div class="breadcrumb-header justify-content-between">
                    <div class="my-auto">
                        <div class="d-flex">
                            <h4 class="content-title mb-0 my-auto">Properties</h4><span
                                class="text-muted mt-1 tx-13 ms-2 mb-0">/ Properties List</span>
                        </div>
                    </div>                    
                </div>
    <div class="content-area py-1" style="background:#fff;padding-top: 40px !important;">


  
        <div class="container-fluid">

            <div class="box box-block bg-white">

               <!--  <a href="{{ route('admin.property.create',['type'=>'asset-fund']) }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add Asset Fund</a> -->
                <a href="{{ route('admin.property.create',['type'=>'property']) }}" style="margin-left: 1em;float: right;margin-bottom: 20px;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add Property</a>
<div class="table-responsive"> 
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.1.0/css/buttons.dataTables.min.css" type="text/css"  />
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" type="text/css" />
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.1.0/css/select.dataTables.min.css" type="text/css" />

<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="https://datatables.net/release-datatables/media/js/jquery.js"></script>
<script type="text/javascript" src="https://datatables.net/release-datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" scr="https://datatables.net/release-datatables/extensions/TableTools/js/dataTables.tableTools.js"></script>

<script src="https://cdn.datatables.net/buttons/1.1.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.1.0/js/buttons.html5.min.js"></script>

<script src="https://jquery-datatables-column-filter.googlecode.com/svn/trunk/media/js/jquery.dataTables.columnFilter.js" type="text/javascript"></script>

                <table id="example" class="display" style="width: 100% !important">
                    <thead>
                    <tr>
                        <th style="width:20px;">@lang('admin.id')</th>
                        <th style="width:70px;">Property Name</th>
                        <th style="width:70px;">Property Location</th>
                        <th style="width:70px;">Property Type</th>
                        
                        <th style="width:70px;">Feature Property</th>
                        <th style="width:70px;"> Property Tags</th>
                        <th style="width:70px;">Status</th>
                        <th style="width:100px;">@lang('admin.action')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($property as $index => $value)
                        <tr>
                            <td>{{ $index + 1 }}</td>
                            <td>{{ $value->propertyName }}</td>
                            <td>{{ $value->propertyState }}</td>
                            <td>{{ $value->propertyType }}</td>
                        
                            <td>

<div class="custom-control custom-switch">
      <input @if($value->feature == 1) checked @endif type="checkbox" name="feature" class="custom-control-input addFeature" data-color="#43b968" data-id="{{ @$value->id}}" id="switch{{ $index + 1 }}">
      <label class="custom-control-label" for="switch{{ $index + 1 }}">Featured</label>
    </div>
                               


                            </td>
                            <td>
                                  <input @if($value->trusted == 1) checked @endif type="checkbox" name="trusted" class=" addTrusted" data-color="#43b968" data-id="{{ @$value->id}}" id="switch{{ $index + 1 }}">
     Trusted
<br>
      <input @if($value->highgrowth == 1) checked @endif type="checkbox" name="highgrowth" class="addGrowth" data-color="#43b968" data-id="{{ @$value->id}}" id="switch{{ $index + 1 }}">
      High Growth
<br>
        <input @if($value->office == 1) checked @endif type="checkbox" name="office" class="office" data-color="#43b968" data-id="{{ @$value->id}}" id="switch{{ $index + 1 }}">
      Office
      <br>
        <input @if($value->gradea == 1) checked @endif type="checkbox" name="gradea" class="gradeA" data-color="#43b968" data-id="{{ @$value->id}}" id="switch{{ $index + 1 }}"> Grade A

                            </td>
                            <td>
                                @if($value->status == 'pending')
                                    <a href="{{ url('admin/issuertokencontract/'.$value->id) }}" class="btn btn-primary" style="width:40px;padding:9px 10px;float:left;display:block;margin-top:0;margin-left:5px;" title="Approve"><i class="fas fa-thumbs-up"></i></a>
                                @else
                                    <select name="status" id="status" class="form-control status" data-id="{{ @$value->id }}">
                                        <option value="">Update Status</option>
                                        <option value="live" @if($value->status == 'live') selected @endif>Live</option>
                                        <option value="soldout" @if($value->status == 'soldout') selected @endif>Sold Out</option>
                                    </select>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('admin.property.edit', $value->id) }}"class="btn btn-secondary btn-block" style="width:40px;padding:9px 10px;float:left;display:block;margin-top:0;" title="Edit"><i class="fas fa-edit"></i>
                                </a>
                                <a href="javascript:void();" onclick="DelProp('{{$value->id}}');" class="btn btn-info btn-block" style="width:40px;padding:9px 10px;float:left;display:block;margin-top:0;margin-left:5px;"> <i class="fa fa-trash" aria-hidden="true"></i> </a>
                            </td>
                        </tr>
                    @empty
                    @endforelse
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>@lang('admin.id')</th>
                        <th>Property Name</th>
                        <th>Property Location</th>
                        <th>Property Type</th>
                      
                        <th>Feature Property</th>
                        <th style="width:70px;"> Property Tags</th>
                        <th>Status</th>
                        <th>@lang('admin.action')</th>
                    </tr>
                    </tfoot>
                </table></div>
            </div>

        </div>
    </div>

    <form name="delprop" action="" id="delprop" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="propid" id="propid" value=""/>
        <input type="hidden" name="_method" value="DELETE">
    </form>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

 <script type="text/javascript">
     $(document).ready(function(){
        $("#status").on("change", function () {
           
            var id  = $(this).data("id");
            var url = "{{ url('/admin/propertyStatus') }}/" + id;
            if ($(this).val() != '') {
                swal({
                         title     : "Are you sure?",
                         text      : "You want to change the property status",
                         icon      : "info",
                         buttons   : true,
                         dangerMode: false,
                         buttons   : ["No", "Yes"],
                     })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                       type   : "POST",
                                       url    : url,
                                       data   : "_token={{ csrf_token() }}&status=" + $(this).val(),
                                       success: function (data) {
                                           if (data.status == 'success') {
                                               swal('Property status updated successfully');
                                           }
                                       }
                                   });
                        }
                    });
            }
        });
         });

</script>
 <script type="text/javascript">
        function DelProp(propid) {
            swal({
                     title     : "Are you sure?",
                     text      : "You want to delete this property ?",
                     icon      : "info",
                     buttons   : true,
                     dangerMode: false,
                     buttons   : ["No", "Yes"],
                 })
                .then((willDelete) => {
                    if (willDelete) {
                        $("#delprop").attr('action', "<?php echo url('/'); ?>/admin/property/" + propid);
                        $("#delprop").submit();
                    }
                });
        }


        $(".addFeature").on("change", function () {
            var id  = $(this).attr("data-id");
             var mainid=$(this).attr("id");
            var url = "{{ url('/admin/propertyFeature') }}/" + id
            if ($(this).is(":checked")) {
                swal({
                         title     : "Are you sure?",
                         text      : "You want to add this property in feature category ?",
                         icon      : "info",
                         buttons   : true,
                         dangerMode: false,
                         buttons   : ["No", "Yes"],
                          html      :"<h4>Loading...</h4>",
                          showSpinner: true,
                     })

                    .then((willDelete) => {
                        if (willDelete) {

 swal({
                title: 'Please Wait..!',
                text: 'Is working..',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                onOpen: () => {
                    swal.showLoading()
                }
            })
                            $.ajax({
                                       type   : "GET",
                                       url    : url,
                                       success: function (data) {
                                           if (data.status == 'success') {
                                               swal('Feature updated successfully');
                                           }
                                       }
                                   });
                        }else{
                           // $('.addFeature').each(function(){ this.checked = false; });
                          
                              $("#"+mainid).prop("checked" , false);
                        }
                    });
            } else {
                $.ajax({
                           type   : "GET",
                           url    : url,
                           success: function (data) {
                               if (data.status == 'success') {
                                   swal('Feature status Disabled successfully');
                               }
                           }
                       });
            }
        });

$(".addTrusted").on("change", function () {
            var id  = $(this).attr("data-id");

            var url = "{{ url('/admin/propertyTrusted') }}/" + id
            if ($(this).is(":checked")) {
                swal({
                         title     : "Are you sure?",
                         text      : "You want to add this Trusted Tag in property?",
                         icon      : "info",
                         buttons   : true,
                         dangerMode: false,
                         buttons   : ["No", "Yes"],
                          html      :"<h4>Loading...</h4>",
                          showSpinner: true,
                     })

                    .then((willDelete) => {
                        if (willDelete) {

                    swal({
                            title: 'Please Wait..!',
                            text: 'Is working..',
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            allowEnterKey: false,
                            onOpen: () => {
                                swal.showLoading()
                            }
                        })
                            $.ajax({
                                       type   : "GET",
                                       url    : url,
                                       success: function (data) {
                                           if (data.status == 'success') {
                                               swal('Trusted Tag Enabled Successfully');
                                           }else{
                                            
                                           }
                                       }
                                   });
                        }else{
                            $('.addTrusted').prop('checked', false);
                        }
                    });
            } else {
                $.ajax({
                           type   : "GET",
                           url    : url,
                           success: function (data) {
                               if (data.status == 'success') {
                                   swal('Trusted Tag Disabled Successfully');
                               }

                           }
                       });
            }
        });
$(".addGrowth").on("change", function () {
            var id  = $(this).attr("data-id");
            var url = "{{ url('/admin/propertyGrowth') }}/" + id
            if ($(this).is(":checked")) {
                swal({
                         title     : "Are you sure?",
                         text      : "You want to add this Growth Tag in property?",
                         icon      : "info",
                         buttons   : true,
                         dangerMode: false,
                         buttons   : ["No", "Yes"],
                          html      :"<h4>Loading...</h4>",
                          showSpinner: true,
                     })

                    .then((willDelete) => {
                        if (willDelete) {

                    swal({
                            title: 'Please Wait..!',
                            text: 'Is working..',
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            allowEnterKey: false,
                            onOpen: () => {
                                swal.showLoading()
                            }
                        })
                            $.ajax({
                                       type   : "GET",
                                       url    : url,
                                       success: function (data) {
                                           if (data.status == 'success') {
                                               swal('Growth Tag Enabled Successfully');
                                           }
                                       }
                                   });
                        }else{
                            $('.addGrowth').prop('checked', false);
                        }
                    });
            } else {
                $.ajax({
                           type   : "GET",
                           url    : url,
                           success: function (data) {
                               if (data.status == 'success') {
                                   swal('Growth Tag Disabled Successfully');
                               }
                           }
                       });
            }
        });

  $(".office").on("change", function () {
            var id  = $(this).attr("data-id");
            var url = "{{ url('/admin/propertyOffice') }}/" + id
            if ($(this).is(":checked")) {
                swal({
                         title     : "Are you sure?",
                         text      : "You want to add this Office Tag in property?",
                         icon      : "info",
                         buttons   : true,
                         dangerMode: false,
                         buttons   : ["No", "Yes"],
                          html      :"<h4>Loading...</h4>",
                          showSpinner: true,
                     })

                    .then((willDelete) => {
                        if (willDelete) {

                    swal({
                            title: 'Please Wait..!',
                            text: 'Is working..',
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            allowEnterKey: false,
                            onOpen: () => {
                                swal.showLoading()
                            }
                        })
                            $.ajax({
                                       type   : "GET",
                                       url    : url,
                                       success: function (data) {
                                           if (data.status == 'success') {
                                               swal('Office Tag Enabled Successfully');
                                           }
                                       }
                                   });
                        }else{
                            $('.office').prop('checked', false);
                        }
                    });
            } else {
                $.ajax({
                           type   : "GET",
                           url    : url,
                           success: function (data) {
                               if (data.status == 'success') {
                                   swal('Office Tag Disabled Successfull');
                               }
                           }
                       });
            }
        });
  $(".gradeA").on("change", function () {
            var id  = $(this).attr("data-id");
            var url = "{{ url('/admin/propertyGradea') }}/" + id
            if ($(this).is(":checked")) {
                swal({
                         title     : "Are you sure?",
                         text      : "You want to add this Grade A in property?",
                         icon      : "info",
                         buttons   : true,
                         dangerMode: false,
                         buttons   : ["No", "Yes"],
                          html      :"<h4>Loading...</h4>",
                          showSpinner: true,
                     })

                    .then((willDelete) => {
                        if (willDelete) {

                    swal({
                            title: 'Please Wait..!',
                            text: 'Is working..',
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            allowEnterKey: false,
                            onOpen: () => {
                                swal.showLoading()
                            }
                        })
                            $.ajax({
                                       type   : "GET",
                                       url    : url,
                                       success: function (data) {
                                           if (data.status == 'success') {
                                               swal('Grade A Tag Enabled Successfully');
                                           }
                                       }
                                   });
                        }else{
                            $('.gradeA').prop('checked', false);
                        }
                    });
            } else {
                $.ajax({
                           type   : "GET",
                           url    : url,
                           success: function (data) {
                               if (data.status == 'success') {
                                   swal('Grade A Tag Disabled Successfully');
                               }
                           }
                       });
            }
        });
    </script>
    
      
@endsection
@section('scripts')
   
@endsection