@extends('admin.layout.base')

@section('title', 'Create Fund')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            <div class="box box-block bg-white">

                <a href="{{ route('admin.property.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> @lang('admin.back')</a>
                <h5 style="margin-bottom: 2em;">Create Fund</h5>
                <form class="form-horizontal" id="property-create" action="{{route('admin.property.store')}}" method="POST" enctype="multipart/form-data" role="form">
                    {{csrf_field()}}
                    <input type="hidden" name="token_type" value="2">
                    <div class="row">
                        <div class="col-md-3 form-group">
                            <label for="" class="col-form-label">Fund Client</label>
                            <select name="property_issuer" id="property_issuer" class="form-control" required>
                                <option value="">Select Issuer</option>
                                @foreach($users as $user)
                                    <option value="{{ @$user->id }}">{{ @$user->email }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="propertyName">@lang('admin.fund.create_fund')<span class="text-danger">*</span></label>
                            <input class="form-control" type="text" name="propertyName" required="" id="propertyName" placeholder="@lang('admin.enter') @lang('admin.fund.create_fund')" maxlength="200">
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="propertyLogo">@lang('admin.fund.fund_logo')<span class="text-danger">*</span></label>
                            <input class="form-control" type="file" name="propertyLogo" accept="image/png,image/jpeg" required id="propertyLogo" placeholder="@lang('admin.fund.fund_logo')">
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="totalDealSize">@lang('admin.totaldealsize')<span class="text-danger">*</span></label>
                            <input class="form-control" type="number" name="totalDealSize" required id="totalDealSize" placeholder="@lang('admin.placeholders.total_deal_size')" min="0">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 form-group">
                            <label for="expectedIrr">@lang('admin.inputs.expected irr')</label>
                            <input class="form-control" type="number" name="expectedIrr" id="expectedIrr" placeholder="@lang('admin.placeholders.expected irr')" min="0">
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="initialInvestment">@lang('admin.fund.min_investment')<span class="text-danger">*</span></label>
                            <input class="form-control" type="number" name="initialInvestment" required id="initialInvestment" placeholder="@lang('admin.enter') @lang('admin.fund.min_investment')" min="0">
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="fundedMembers">@lang('admin.fund.fundedMembers')<span class="text-danger">*</span></label>
                            <input class="form-control" type="number" name="fundedMembers" required id="fundedMembers" placeholder="@lang('admin.enter') @lang('admin.fund.fundedMembers')" min="0">
                        </div>
                        
                        <div class="col-md-3 form-group">
                            <label for="propertyEquityMultiple">@lang('admin.inputs.property_equity_multiple')</label>
                            <input class="form-control" type="number" name="propertyEquityMultiple" id="propertyEquityMultiple" placeholder="@lang('admin.placeholders.property_equity_multiple')" step="any" min="0">
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="holdingPeriod">@lang('admin.inputs.total_holding_period')<span class="text-danger">*</span></label>
                            <input class="form-control" type="number" name="holdingPeriod" required id="holdingPeriod" placeholder="@lang('admin.placeholders.total_holding_period')" min="0">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 form-group">
                            <label for="propertyLogo">Insert Video File</label>
                            <input class="form-control" type="file" name="propertyVideo" accept="video/mp4,video/x-m4v,video/*" id="propertyLogo" placeholder="Enter Property Logo">
                        </div>
                    </div>
                    <h5 class="mt-2">@lang('admin.inputs.overview_details')</h5>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="propertyOverview">@lang('admin.fund.detail_overview')</label>
                                <textarea class="form-control" type="text" name="propertyOverview" required id="propertyOverview" placeholder="@lang('admin.enter') @lang('admin.fund.detail_overview')"></textarea>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="propertyHighlights">@lang('admin.fund.detail_highlights')</label>
                                <textarea class="form-control" type="text" name="propertyHighlights" required id="propertyHighlights" placeholder="@lang('admin.enter') @lang('admin.fund.detail_highlights')"></textarea>
                            </div>
                        </div>
                    </div>
                    <h5 class="mt-2">@lang('admin.doc/reports')</h5>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="" class="col-form-label">@lang('admin.labels.investordos')</label>
                                <input class="form-control" type="file" name="investor" accept="application/pdf" required id="propertyInvestor">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="" class="col-form-label">@lang('admin.labels.titlereport')</label>
                                <input class="form-control" type="file" name="titlereport" accept="application/pdf" id="propertyTitlereport">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="" class="col-form-label">Subscription Agreement</label>
                                <input class="form-control" type="file" name="termsheet" accept="application/pdf" id="termsheet">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="" class="col-form-label">Updates</label>
                                <input class="form-control" type="file" accept="application/pdf" name="propertyUpdatesDoc" id="propertyUpdatesDoc">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 d-none" style="display: none">
                            <div class="form-group">
                                <label for="" class="col-form-label">Management Team</label>
                                <input class="form-control" type="file" accept="application/pdf" name="propertyManagementTeam" id="propertyManagementTeam">
                            </div>
                        </div>

                    </div>
                    <h5 class="mt-2">Token Details</h5>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="" class="col-form-label">@lang('admin.coin.tokenname')</label>
                                <input class="form-control" type="text" name="token_name" required id="tokenName" placeholder="@lang('admin.coin.tokenname')">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="" class="col-form-label">@lang('admin.coin.tokensymbol')</label>
                                <input class="form-control" type="text" name="token_symbol" required id="tokenSymbol" placeholder="@lang('admin.coin.tokensymbol')">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="" class="col-form-label">@lang('admin.coin.tokenvalue') ({{ Setting::get('default_currency') }})</label>
                                <input class="form-control" type="text" name="token_value" required id="tokenValue" placeholder="@lang('admin.coin.tokenvalue')">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="" class="col-form-label">@lang('admin.coin.tokensupply')</label>
                                <input class="form-control" type="text" name="token_supply" required id="tokenSupply" placeholder="@lang('admin.coin.tokensupply')">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="" class="col-form-label">@lang('admin.coin.tokendecimal')</label>
                                <input class="form-control" type="text" name="token_decimal" required id="tokenDecimal" placeholder="@lang('admin.coin.tokendecimal')">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="" class="col-form-label">@lang('admin.token_to_be_issued')</label>
                                <br>
                                <input type="radio" name="tokentype" class="tokentype" value="ERC20" checked> @lang('admin.erc20_utility')<br>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="" class="col-form-label">@lang('admin.coin.tokenimage')</label>
                                <input class="form-control" type="file" name="token_image" required id="tokenImage">
                            </div>
                        </div>
                    </div>

                    <h5>Management Team</h5>
                    <div class="row form-group">
                        <div class="col-md-6">
                            <label for="" class="col-form-label">Management Team Description</label>
                            <textarea class="form-control" type="text" name="ManagementTeamDescription" required id="propertyOverview" placeholder="Enter Management Team Description"></textarea>
                        </div>
                    </div>
                    <h6>Management Members</h6>
                    <div id="divManagementMembers" data-id="0"></div>
                    <button type="button" class="btn btn-info" id="AddMember">+ Add Member</button>

                    <div class="form-group row">
                        <label for="" class="col-xs-2 col-form-label"></label>
                        <div class="col-xs-10">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="{{route('admin.property.index')}}" class="btn btn-default">@lang('admin.cancel')</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        var n = 2;
        $("#add_landmark").click(function () {
            var html = '<div id="landmark_block_' + n + '" class="form-group row"><div class="form-group col-md-5"><label for="" class="col-form-label">Landmark Name</label><input class="form-control" type="text" name="landmarks[' + n + '][landmarkName]" required id="landmarkName_' + n + '" placeholder="Enter landmark name"></div><div class="form-group col-md-5"><label for="" class="col-form-label">Landmark Distance</label><input class="form-control" type="number" name="landmarks[' + n + '][landmarkDist]" required id="landmarkDist_' + n + '" placeholder="Enter landmark distance" min="0" step="any"></div><div class="form-group col-md-2"><button type="button" class="btn btn-danger landmark_remove" id="landmark_remove_' + n + '" onclick="landmark_remove(' + n + ');" >X</button></div></div>';
            $("#landmark_section").append(html);
            n = n + 1;
        });
        function landmark_remove(n) {
            $("#landmark_block_" + n).remove();
        }

        $("#add_comparables").click(function () {
            var html = '<div id="comparables_block_' + n + '" class="form-group row"><div class="form-group col-md-3"><label for="" class="col-form-label">Property Address</label><input class="form-control" type="text" name="comparables[' + n + '][property]" required id="property_' + n + '" placeholder="Property Address"></div><div class="form-group col-md-3"><label for="" class="col-form-label">Sale Date</label><input class="form-control" type="text" name="comparables[' + n + '][type]" required id="type_' + n + '" placeholder="Sale Date"></div><div class="form-group col-md-3"><label for="" class="col-form-label">Location</label><input class="form-control" type="text" name="comparables[' + n + '][location]" required id="location_' + n + '" placeholder="Location"></div><div class="form-group col-md-3"><label for="" class="col-form-label">Year of Build</label><input class="form-control" type="number" name="comparables[' + n + '][distanaccept="image/png,image/jpeg"ce]" required id="distance_' + n + '" placeholder="Year of Build" min="0" step="any"></div><div class="form-group col-md-3"><label for="" class="col-form-label">Total Sft</label><input class="form-control" type="number" name="comparables[' + n + '][rent]" required id="rent_' + n + '" placeholder="Total Sft" min="0" step="any"></div><div class="form-group col-md-3"><label for="" class="col-form-label">Sale Price</label><input class="form-control" type="number" name="comparables[' + n + '][saleprice]" required id="saleprice_' + n + '" placeholder="Sale Price" min="0" step="any"></div><div class="col-md-3"><div class="form-group"><label for="propertyLogo">Property Logo</label><input class="form-control" type="file" name="propertyVideo" accept="image/png,image/jpeg"  required id="propertyLogo" placeholder="Enter Property Logo"></div></div><div class="col-md-3"><div class="form-group"><label for="propertyLogo">Map</label><input class="form-control" type="file" name="map" accept=".pdf"  required id="propertyLogo" placeholder="Enter Property Logo"></div></div><div class="col-md-3"><div class="form-group"><label for="propertyLogo">Comparables Details</label><input class="form-control" type="file" name="comparabledetails" accept=".pdf"  required id="propertyLogo" placeholder="Enter Property Logo"></div></div><div class="form-group col-md-3"><button type="button" class="btn btn-danger landmark_remove" id="landmark_remove_' + n + '" onclick="comparables_remove(' + n + ');" >X</button></div></div>';
            $("#comparables_section").append(html);
            n = n + 1;
        });

        function comparables_remove(n) {
            $("#comparables_block_" + n).remove();
        }

        $(document).ready(function () {
            addManagementMember(0);
        });

        $('#AddMember').click(function (e) {
            var index = parseInt($('#divManagementMembers').attr('data-id'));
            addManagementMember(index + 1);
        });

        function removeMember(index) {
            $('#MemberBlock_' + index).remove();
        }

        function addManagementMember(index) {
            var removeBtn = '<button type="button" class="btn btn-danger" style="margin-top: 34px;" onclick="removeMember(' + index + ');">X</button>';
            var temp      = '<div class="row" id="MemberBlock_' + index + '">' +
                '	<div class="col-md-3 form-group">' +
                '		<label for="" class="col-form-label">Member Name</label>' +
                '		<input class="form-control" type="text" name="member[' + index + '][name]" required id="MemberName_' + index + '" placeholder="Enter Member Name">' +
                '	</div>' +
                '	<div class="col-md-3 form-group">' +
                '		<label for="" class="col-form-label">Member Position</label>' +
                '		<input class="form-control" type="text" name="member[' + index + '][position]" required id="MemberPosition_' + index + '" placeholder="Enter Member Position">' +
                '	</div>'+
                '	<div class="col-md-3 form-group">' +
                '		<label for="" class="col-form-label">Member Image</label>' +
                '		<input class="form-control" type="file" name="member[' + index + '][pic]" required id="MemberPic_' + index + '" accept="image/png,image/jpeg" placeholder="Select Member Picture">' +
                '	</div>' +
                '	<div class="col-md-3 form-group">' +
                '		<label for="" class="col-form-label">Member Description</label>' +
                '		<input class="form-control" type="text" name="member[' + index + '][description]" required id="MemberDescription_' + index + '" placeholder="Enter Description">' +
                '	</div>' +
                '	<div class="col-md-1 form-group">' +
                '       ' + ((index > 0) ? removeBtn : '') +
                '   </div>' +
                '</div>';
            $('#divManagementMembers').attr('data-id', index).append(temp);
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#property-create").validate({
                                               rules        : {
                                                   "propertyName"             : {
                                                       required : true,
                                                       maxlength: 200,
                                                   },
                                                   "propertyLocation"         : {
                                                       required : true,
                                                       maxlength: 200,
                                                   },
                                                   "propertyLogo"             : {
                                                       required: true,
                                                       accept  : 'image/jpeg, image/png, image/jpg, image/bmp',
                                                   },
                                                   "propertyType"             : {
                                                       required: true,
                                                   },
                                                   "totalDealSize"            : {
                                                       required: true,
                                                       number  : true,
                                                       min     : 0,
                                                   },
                                                   "expectedIrr"              : {
                                                       required : false,
                                                       number   : true,
                                                       min      : 0,
                                                       maxlength: 3,
                                                   },
                                                   "initialInvestment"        : {
                                                       required: true,
                                                       number  : true,
                                                       min     : 0,
                                                   },
                                                   "propertyEquityMultiple"   : {
                                                       required: false,
                                                       number  : true,
                                                       min     : 0,
                                                   },
                                                   "holdingPeriod"            : {
                                                       required: true,
                                                       number  : true,
                                                       min     : 0,
                                                   },
                                                   "total_sft"                : {
                                                       required: true,
                                                       number  : true,
                                                       min     : 0,
                                                   },
                                                   "propertyOverview"         : {
                                                       required: true,
                                                   },
                                                   "propertyHighlights"       : {
                                                       required: true,
                                                   },
                                                   "propertyLocationOverview" : {
                                                       required: true,
                                                   },
                                                   "locality"                 : {
                                                       required : true,
                                                       maxlength: 200,
                                                   },
                                                   "yearOfConstruction"       : {
                                                       required : true,
                                                       number   : true,
                                                       min      : 0,
                                                       maxlength: 4,
                                                   },
                                                   "storeys"                  : {
                                                       required: true,
                                                   },
                                                   "propertyParking"          : {
                                                       required : true,
                                                       maxlength: 200,
                                                   },
                                                   "floorforSale"             : {
                                                       required: true,
                                                       number  : true,
                                                       min     : 0,
                                                   },
                                                   "propertyTotalBuildingArea": {
                                                       required: true,
                                                       number  : true,
                                                       min     : 0,
                                                   },
                                                   "propertyDetailsHighlights": {
                                                       required: true,
                                                   },
                                                   "floorplan"                : {
                                                       required: true,
                                                       accept  : 'pdf',
                                                   },
                                                   "investor"                 : {
                                                       required: true,
                                                       accept  : 'pdf',
                                                   },
                                                   "titlereport"              : {
                                                       required: false,
                                                       accept  : 'pdf',
                                                   },
                                                   "termsheet"                : {
                                                       required: false,
                                                       accept  : 'pdf',
                                                   },
                                                   "propertyimages"           : {
                                                       required: true,
                                                       accept  : 'image/jpeg, image/png, image/jpg, image/bmp',
                                                   },
                                                   "management"               : {
                                                       required: true,
                                                   },
                                                   "updates"                  : {
                                                       required: true,
                                                   },
                                                   "propertylogoimage"        : {
                                                       required: true,
                                                       accept  : 'image/jpeg, image/png, image/jpg, image/bmp',
                                                   },
                                                   "propertyVideo"            : {
                                                       required: false,
                                                       accept  : 'video/mp4,video/x-m4v',
                                                   },
                                                   "map"                      : {
                                                       required: true,
                                                       accept  : 'pdf',
                                                   },
                                                   "comparabledetails"        : {
                                                       required: true,
                                                       accept  : 'pdf',
                                                   },
                                                   "token_name"               : {
                                                       required: true,
                                                   },
                                                   "token_symbol"             : {
                                                       required : true,
                                                       minlength: 3,
                                                       maxlength: 4,
                                                   },
                                                   "token_value"              : {
                                                       required: true,
                                                       number  : true,
                                                   },
                                                   "token_decimal"            : {
                                                       required : true,
                                                       digits   : true,
                                                       maxlength: 2,
                                                   },
                                                   "propertyManagementTeam"   : {
                                                       required: false,
                                                       accept  : 'pdf',
                                                   },
                                                   "propertyUpdatesDoc"       : {
                                                       required: false,
                                                       accept  : 'pdf',
                                                   },
                                               },
                                               messages     : {
                                                   "propertyName"             : {
                                                       required: "Please Enter Fund Name",
                                                   },
                                                   "propertyLocation"         : {
                                                       required: "Please Enter Fund Location",
                                                   },
                                                   "propertyLogo"             : {
                                                       required: "Please Choose Fund Logo",
                                                       accept  : "Please Upload Valid format (jpeg|png|jpg|bmp)",
                                                   },
                                                   "propertyVideo"            : {
                                                       required: "Please Choose Fund Logo",
                                                       accept  : "Please Upload Valid format (mp4|x-m4v)",
                                                   },
                                                   "management"               : {
                                                       required: "Please Upload Management",
                                                   },
                                                   "updates"                  : {
                                                       required: "Please Upload Updates",
                                                   },
                                                   "propertylogoimage"        : {
                                                       required: "Please Choose Fund Logo",
                                                       accept  : "Please Upload Valid format (jpeg|png|jpg|bmp)",
                                                   },
                                                   "propertyimages"           : {
                                                       required: "Please Choose Fund Logo",
                                                       accept  : "Please Upload Valid format (jpeg|png|jpg|bmp)",
                                                   },
                                                   "totalDealSize"            : {
                                                       required: "Please Enter Total Deal Size",
                                                   },
                                                   "expectedIrr"              : {
                                                       required: "Please Enter Expected IRR",
                                                   },
                                                   "initialInvestment"        : {
                                                       required: "Please Enter InitialInvestment",
                                                   },
                                                   "propertyEquityMultiple"   : {
                                                       required: "Please Enter Fund Equity Multiple",
                                                   },
                                                   "holdingPeriod"            : {
                                                       required: "Please Enter Holding Period",
                                                   },
                                                   "total_sft"                : {
                                                       required: "Please Enter Total SFT",
                                                   },
                                                   "propertyOverview"         : {
                                                       required: "Please Enter Fund Overview",
                                                   },
                                                   "propertyHighlights"       : {
                                                       required: "Please Enter Fund Highlights",
                                                   },
                                                   "propertyLocationOverview" : {
                                                       required: "Please Enter Location Overview",
                                                   },
                                                   "locality"                 : {
                                                       required: "Please Enter Locality",
                                                   },
                                                   "yearOfConstruction"       : {
                                                       required: "Please Enter Year of Construction",
                                                   },
                                                   "propertyParking"          : {
                                                       required: "Please Enter Parking",
                                                   },
                                                   "floorforSale"             : {
                                                       required: "Please Enter Floor for Sale",
                                                   },
                                                   "propertyTotalBuildingArea": {
                                                       required: "Please Enter Total Building Area",
                                                   },
                                                   "propertyDetailsHighlights": {
                                                       required: "Please Enter Details Highlights",
                                                   },
                                                   "floorplan"                : {
                                                       required: "Please Choose Floor Plan",
                                                       accept  : "Please Upload Valid format (pdf)",
                                                   },
                                                   "investor"                 : {
                                                       required: "Please Choose Investor Document",
                                                       accept  : "Please Upload Valid format (pdf)",
                                                   },
                                                   "titlereport"              : {
                                                       required: "Please Choose Title Report",
                                                       accept  : "Please Upload Valid format (pdf)",
                                                   },
                                                   "termsheet"                : {
                                                       required: "Please Choose Term Sheet",
                                                       accept  : "Please Upload Valid format (pdf)",
                                                   },
                                                   "comparabledetails"        : {
                                                       required: "Please Choose Term Sheet",
                                                       accept  : "Please Upload Valid format (pdf)",
                                                   },
                                                   "map"                      : {
                                                       required: "Please Choose Term Sheet",
                                                       accept  : "Please Upload Valid format (pdf)",
                                                   },
                                                   "propertyManagementTeam"   : {
                                                       required: "Please Choose Management Team Report",
                                                       accept  : "Please Upload Valid format (pdf)",
                                                   },
                                                   "propertyUpdatesDoc"       : {
                                                       required: "Please Choose Updates Report",
                                                       accept  : "Please Upload Valid format (pdf)",
                                                   },
                                               },
                                               submitHandler: function (form) {
                                                   form.submit();
                                               }
                                           });
        });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@rc/dist/js.cookie.min.js"></script>
    <script>
        $(document).ready(function () {
            $("input[type=text], input[type=radio], input[type=file], textarea, select, input[type=number]").blur(function () {
                if ($(this).val() != "") {
                    Cookies.set('token_asset_' + $(this).attr('name'), $(this).val(), {expires: 1});
                    console.log($(this).val());
                    console.log($(this).attr('name'));
                }
            });

            $("input[type=text], textarea, select, input[type=radio],  input[type=number]").each(function () {
                var cookval = Cookies.get('token_asset_' + $(this).attr('name'));
                if ($(this).attr('type') == "radio") {
                    var inpname = $(this).attr('name');
                    $("input[name=" + inpname + "][value=" + cookval + "]").prop("checked", true);
                } else {
                    $(this).val(cookval);
                }
            });
        });

    </script>
@endsection
