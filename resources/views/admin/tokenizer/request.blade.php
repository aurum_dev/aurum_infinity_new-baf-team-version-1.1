@extends('admin.layout.base')

@section('title', 'Token Request')

@section('content')
<style>
    
#example_length{
margin-top: 5px !important;
    float: right !important;
    margin-right: 266px !important;
}


</style>
<div class="breadcrumb-header justify-content-between">
                    <div class="my-auto">
                        <div class="d-flex">
                            <h4 class="content-title mb-0 my-auto">Requested Tokens</h4><span
                                class="text-muted mt-1 tx-13 ms-2 mb-0">/ Tokens List</span>
                        </div>
                    </div>                    
                </div>
<div class="content-area py-1" style="background:#fff;padding-top: 40px !important;">
    <div class="container-fluid">
        <div class="box box-block bg-white">         
                        
            <table id="example" class="table table-striped table-hover dt-responsive" style="width: 100% !important">
                <thead>
                    <tr>
                        <th>@lang('admin.id')</th>                        
                        <th>User</th>                        
                        <th>Token Name</th>                        
                        <th>Token Symbol</th>                        
                        <th>Token Value</th>                        
                        <th>Token Supply</th> 
                        <th>@lang('admin.action')</th>
                    </tr>
                </thead>
                <tbody>
                   
                    @foreach($contracts as $index => $value)
                    <tr>
                        <td>{{ $index + 1 }}</td>                        
                        <td>{{ @(!is_null($value->user->name)) ? $value->user->name : 'Admin'  }}</td>                        
                        <td>{{ @$value->tokenName }}</td>                        
                        <td>{{ @$value->tokenSymbol }}</td>                       
                        <td>{{ @$value->tokenValue }}</td>                        
                        <td>{{ @$value->tokenSupply }}</td>
                        <td> <a href="{{route('admin.issuertokencontracttest',$value->id)}}" class="btn btn-info"> ADD </a>
                             <a href="{{route('admin.issuertokenreject',$value->id)}}" class="btn btn-warning"> REJECT </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>@lang('admin.id')</th> 
                        <th>User</th>                         
                        <th>Token Name</th>                        
                        <th>Token Symbol</th>                        
                        <th>Token Value</th>                        
                        <th>Token Supply</th>     
                        <th>@lang('admin.action')</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection