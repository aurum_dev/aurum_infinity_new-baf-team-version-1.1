@extends('admin.layout.base')

@section('title', 'Admin Wallet')

@section('content')
<style>
    
#example_length{
margin-top: 5px !important;
    float: right !important;
    margin-right: 266px !important;
}


</style>
<div class="breadcrumb-header justify-content-between">
                    <div class="my-auto">
                        <div class="d-flex">
                            <h4 class="content-title mb-0 my-auto">Admin Wallet</h4><span
                                class="text-muted mt-1 tx-13 ms-2 mb-0">/ Tokens Transaction List</span>
                        </div>
                    </div>                    
                </div>
<div class="content-area py-1" style="background:#fff;padding-top: 40px !important;">
    <div class="container-fluid">
        <div class="box box-block bg-white">         
                        
            <table id="example" class="table table-striped table-hover dt-responsive" style="width: 100% !important">
                <thead>
                    <tr>
                        <th style="width:10px;">S.NO</th> 
                        <th style="width:10px;">WALLET ID</th>   
                                             <th style="width:10px;">Property Name</th> 
                                          <th style="width:10px;">Token Name</th> 
                                        
                                        <th style="width:10px;">Investor </th> 
                                         <th style="width:10px;">Issuer </th> 
                              
                        <th style="width:10px;">Total Tokens</th> 
                        <th style="width:20px;">Total Amount</th>                                              
                       
                       
                        <th style="width:20px;">Status</th>
                        <th style="width:60px;">@lang('admin.action')</th>
                    </tr>
                </thead>
                <tbody>
                   
                  @foreach($contracts as $index => $value)
                    <tr>
                        <td>{{ $index + 1 }}
                            <input type="hidden" id="walletid" value="{{$value->wallet_id}}">
<input type="hidden" id="tokentransid" value="{{$value->usertokentransid}}">
<input type="hidden" id="issuerid" value="{{$value->issuer_id}}">
<input type="hidden" id="investorid" value="{{$value->investor_id}}">
<input type="hidden" id="propertyid" value="{{$value->property_id}}">
<input type="hidden" id="payvalue" value="{{$value->paybyinvestor}}">
                        </td>  
                        <td>{{$value->wallet_id}}</td>         
                        <td>
                            <?php
$tmp = \App\Property::where('id',$value->property_id)->first();
echo $tmp->propertyName;
?></td>             
                                               
                                          <td>{{$value->tokenName}}</td>
                                          
                                            <td>  <?php
$invuser = \App\User::where('id',$value->investor_id)->first();
echo $invuser->name;
?></td>
                                              <td>  <?php
$issuser = \App\User::where('id',$value->issuer_id)->first();
echo $issuser->name;
?></td>
                                        
                        <td>{{$value->total_tokens}}</td>
                        <td><i class="fa fa-rupee-sign"></i> {{$value->paybyinvestor}}</td>
                       
                      
                       
                        <td>
                                @if($value->txnhash !='') 
                                    <span style="align-items: center;
    border: 1px solid #d6dcec;
    border-radius: 6px;
    display: flex;
    height: 2.5rem;
    justify-content: center;
    margin: 4px;
    text-align: center;
    transform: scale(1);
    transition: .3s ease-in-out;
    width: 2.5rem;"><i class="fa fa-check" aria-hidden="true" style="color:#22c03c;"></i></span> 
                                @else  
                                    <span style="align-items: center;
    border: 1px solid #d6dcec;
    border-radius: 6px;
    display: flex;
    height: 2.5rem;
    justify-content: center;
    margin: 4px;
    text-align: center;
    transform: scale(1);
    transition: .3s ease-in-out;
    width: 2.5rem;"><i class="fa fa-times" aria-hidden="true" style="color:#ff0000;"></i></span> 
                                @endif
                            </td>
                        <td> 

                        @if($value->txnhash == '') 

                         {{--<a class="btn btn-info btn-block walletapprove" data-id="{{$value->wallet_id}}" style="width:40px;padding:9px 10px;float:left;display:block;margin-top:0;margin-left:5px;" title="Approve" href="javascript:void(0);"><i class="fas fa-thumbs-up"></i></a>--}}
                       
                           @endif  
                               
                            

                            <a class="btn btn-dark btn-block" style="width:40px;padding:9px 10px;float:left;display:block;margin-top:0;margin-left:5px;" title="Token Details" href="{{ route('admin.walletdetails', $value->wallet_id ) }}"><i class="fas fa-coins"></i></a>
                        </td>
                    </tr>
                    @endforeach 
                </tbody>
               
            </table>
        </div>
    </div>
</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
     
    
      $(".walletapprove").on("click", function () {
            var walletid  = $(this).attr("data-id");                  
            var finalid=walletid;            
alert(walletid);
           var url = "{{ url('/admin/investstoreadmin') }}/" + finalid;

             if (walletid != '') {
                swal({
                         title     : "Are you sure?",
                         html      :"<h4>Loading...</h4>",
                         showSpinner: true,
                         text      : "You want to transfer the token from Issuer to investor",
                         icon      : "info",
                         buttons   : true,
                         dangerMode: false,
                         buttons   : ["No", "Yes"],
                     })
                    .then((willDelete) => {
                        if (willDelete) {
                            swal({
                title: 'Please Wait..!',
                text: 'Is working..',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                onOpen: () => {
                    swal.showLoading()
                }
            })
                            $.ajax({
                                       type   : "GET",
                                       url    : url,                                       
                                       success: function (data) {
                                           if (data.status == 'success') {
                                               swal('Token Transferred Successfully');
                                               location.reload();
                                           }
                                           if (data.status == 'failed') {
                                               swal("We couldn't connect to the server!");
                                             //  location.reload();
                                           }
                                       }
                                   });
                        }
                    });
            }
        });
       /*$(document).ready(function(){
       $(".payby, .no-of-token").on("click", function () {
  
              alert('hello');
 
                /*$.ajax({
                    url    : "{{url('/gettokeninvestvalue')}}",
                    type   : "POST",
                    data   : {'token_id': token_id, 'no_of_token': no_of_token, 'payment_id': payment_id, '_token': '{{ csrf_token() }}'},
                    success: function (result) {
                        if (result.status == 1) {
                            $('#amount').val(result.token_equ_value);
                            $('#bonus_token').val(result.bonus_token);
                            $('#total_token').val(result.total_token);
                        } else {
                            $('#amount').val(0);
                        }
                    }
                });
});
       
        });*/
    </script>
@endsection