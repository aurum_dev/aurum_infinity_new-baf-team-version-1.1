@extends('admin.layout.base')

@section('title', 'Pages ')

@section('content')
<div class="breadcrumb-header justify-content-between">
                    <div class="my-auto">
                        <div class="d-flex">
                            <h4 class="content-title mb-0 my-auto">Privacy Policy</h4>
                        </div>
                    </div>                    
                </div>
<div class="content-area py-1" style="background:#fff;padding-top: 40px !important;">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5>@lang('admin.pages.pages')</h5>

            <div className="row">
                <form action="{{ route('admin.privacy.update') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="page" value="page_privacy">

                    <div class="row">
                        <div class="col-xs-12">
                            <textarea name="content" id="myeditor">{{@$privacy->privacycontent}}</textarea>
                        </div>
                    </div>

                    <br>

                    <div class="row">
                       <div class="col-xs-12">
                            <a href="{{ route('admin.home') }}" style="float:right;margin-left:10px;" class="btn btn-secondary">@lang('admin.cancel')</a>                            
                        
                            <button type="submit" style="float:right;" class="btn btn-primary">@lang('admin.update')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('myeditor');
</script>
@endsection

@section('scripts')

@endsection