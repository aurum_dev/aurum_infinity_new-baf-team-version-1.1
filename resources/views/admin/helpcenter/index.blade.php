@extends('admin.layout.base')

@section('title', 'HelpCenter ')

@section('content')
<style>
    
#example_length{
margin-top: 5px !important;
    float: right !important;
    margin-right: 266px !important;
}


</style>
<div class="breadcrumb-header justify-content-between">
                    <div class="my-auto">
                        <div class="d-flex">
                            <h4 class="content-title mb-0 my-auto">HelpCenters</h4><span
                                class="text-muted mt-1 tx-13 ms-2 mb-0">/ Helpcenters List</span>
                        </div>
                    </div>                    
                </div>
<div class="content-area py-1" style="background:#fff;padding-top: 40px !important;">
    <div class="container-fluid">
        <div class="box box-block bg-white">          
         
             <h5 class="mb-1">HelpCenter</h5>
                <a href="{{ route('admin.helpcenter.create') }}" style="margin-left: 1em;float: right;margin-bottom: 20px;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add Helpcenter</a>
            <table  id="example" class="table table-striped table-hover dt-responsive" id="table-2">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Type</th>
                        <th>Title</th>
                        <th>Description</th>                        
                        <th>@lang('admin.action')</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($helpcenter as $index => $user)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $user->type }}</td>
                                         
                        <td>{{ $user->title }}</td>
                                                
                        <td>
                           {{ $user->description }}
                        </td>   

                       

                        <td>                            
                            <form action="{{ route('admin.helpcenter.destroy', $user->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    @if( Setting::get('demo_mode') == 0)
                                    <a href="{{ route('admin.helpcenter.edit', $user->id) }}" class="btn btn-secondary btn-block" style="width:40px;padding:9px 10px;float:left;display:block;margin-top:0;" title="Edit"><i class="fas fa-edit"></i></a>
                                    <button class="btn btn-info btn-block" onclick="return confirm('Are you sure?')" style="width:40px;padding:9px 10px;float:left;display:block;margin-top:0;margin-left:5px;"><i class="fa fa-trash"></i> </button>
                                    @endif
                                </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                       <th>S.No</th>
                        <th>Type</th>
                        <th>Title</th>
                        <th>Description</th>                        
                        <th>@lang('admin.action')</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection