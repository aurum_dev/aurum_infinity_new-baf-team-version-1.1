@extends('admin.layout.base')

@section('title', 'Create Help Center')

@section('content')

<div class="breadcrumb-header justify-content-between">
                    <div class="my-auto">
                        <div class="d-flex">
                            <h4 class="content-title mb-0 my-auto">Helpcenters</h4><span
                                class="text-muted mt-1 tx-13 ms-2 mb-0">/ Add Helpcenters </span>
                        </div>
                    </div>                    
                </div>
<div class="content-area py-1" style="background:#fff;padding-top: 40px !important;">
    <div class="container-fluid">
        <div class="box box-block bg-white">
   <div class="row">
                    <div class="form-group col-md-12">
            <a href="{{ route('admin.helpcenter.index') }}" style="float:right;" class="btn btn-primary pull-right"><i class="fa fa-angle-left"></i> @lang('admin.back')</a>

           </div></div>
            <form class="form-horizontal" action="{{route('admin.helpcenter.store')}}" method="POST" enctype="multipart/form-data" role="form">              
                {{csrf_field()}}
<div class="form-group col-md-12">
                <div class="row">
                    <div class="form-group col-md-4">
                    <label for="" class="col-form-label">Type</label>                  
                   
                    <select class="form-control" name="type" required id="type">
                        <option value=""> Select type</option>
                        <option value="email"> Email</option>
                        <option value="phone">Phone</option>
                    </select>
                </div> 
                </div>
                 
                <div class="row">

                 <div class="form-group col-md-4">
                    <label for="" class="col-form-label">Title</label>                  
                    <input class="form-control" type="text" name="title" required id="title" placeholder="Enter title">
                </div>   
                </div>           


             <div class="row">
                <div class="form-group col-md-4">
                    <label for="" class="col-form-label">Description</label>                  
                    <input class="form-control" type="text" name="description" required id="description" placeholder="Enter description">
                </div>              
             </div>
             <div class="row">
                    <div class="form-group col-md-12">
                    <br>                
                    <br>                
                        <div class="col-xs-12">
                           
                            <a href="{{route('admin.helpcenter.index')}}" style="float:right;margin-left:10px;" class="btn btn-secondary">@lang('admin.cancel')</a>
                             <button type="submit" class="btn btn-primary" style="float:right;">Submit</button>
                        </div>
                    </div>
             </div>
         </div>
            </form>
        </div>
    </div>
</div>

@endsection