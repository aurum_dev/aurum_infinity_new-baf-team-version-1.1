@extends('admin.layout.base')

@section('title', 'Dashboard ')

@section('styles')


<style>
	.site-header{
		display: none !important;
	}
.qrcode {
    background: #fff;
    border: 1px solid rgba(0, 0, 0, 0.125);
    border-radius: 5px;
	padding: 15px;
	text-align:center;
}
.address_head
{
	padding: 10px 0px;
	margin: 5px 0px;
}
.row.mb-50 {
    margin-bottom: 50px;
}
.form-control
{
	border: 1px solid rgba(0,0,0,.15);
}
.mar-15
{
	margin: 15px 0px;
}

</style>


@endsection

@section('content')
<div class="content-area py-1">
	<div class="container-fluid">	
<div class="breadcrumb-header justify-content-between">
						<div class="left-content">
							<div>
							<h2 class="main-content-title tx-24 mg-b-4 mg-b-lg-1">Hi {{Auth::user()->name}}, Welcome Back!</h2>
							<p class="mg-b-0">Admin Dashboard.</p>


							<br><br>
							<h2 class="main-content-title tx-24 mg-b-4 mg-b-lg-1" style="color:#ff9208;">TOKEN STATS</h2>
							
							</div>
						</div>
						
					</div>

 <?php
 
$propdet=\App\Property::orderBy('created_at','desc')->get();
$totaltoken=0;
$totaltokenreq=0;
$totaltokenapp=0;
foreach($propdet as $index => $value){
  $totaltoken+=$value->tokenSupply;
   if($value->contract_address==''){
    $totaltokenreq+=$value->tokenSupply;
   }
    if($value->contract_address!=''){
    $totaltokenapp+=$value->tokenSupply;
   }
}
$investusers=\App\User::where('user_type','1')->get();
$issuerusers=\App\User::where('user_type','2')->get();
$adminwallet=\App\AdminWallet::orderBy('created_at','desc')->get();
$totalamt=0;
$totaladcom=0;
$totalpayissuer=0;
$totalreqinvestor=0;
$totalappinvestor=0;
foreach($adminwallet as $indexx => $valuee){
  $totalamt+=$valuee->paybyinvestor;
  $totaladcom+=$valuee->admincommission;
	  if($valuee->txnhash!=''){
	  $totalpayissuer+=$valuee->paytoissuer;
	  $totalappinvestor+=$valuee->total_tokens;
	}else if($valuee->txnhash==''){
		$totalreqinvestor+=$valuee->total_tokens;
	}
}

?>
   <div class="row row-sm">


						<div class="col-xl-4 col-lg-6 col-md-6 col-xm-12">
							<div class="card overflow-hidden sales-card btn-light btn-block">
								<div class="ps-3 pt-3 pe-3 pb-2 pt-0">
									<div class="">
										<h6 class="mb-3 tx-20 text-white" style="color:#251d59 !important;"><i class="fas fa-coins"></i>  TOTAL TOKENS</h6>
									</div>
									<div class="pb-0 mt-0">
										<div class="d-flex">
											<div class="">
												<h4 class="tx-30 fw-bold mb-1 text-white" style="color:#251d59 !important;"> {{ $totaltoken }}</h4>
												<p class="mb-0 tx-15 text-white op-7"></p>
											</div>
											<span class="float-end my-auto ms-auto">
												<i class="fas fa-arrow-circle-up text-white" style="color:#251d59 !important;"></i>
												<span class="text-white op-7"> </span>
											</span>
										</div>
									</div>
								</div>
							
							</div>
						</div>
						<div class="col-xl-4 col-lg-6 col-md-6 col-xm-12">
							<div class="card overflow-hidden sales-card btn-light btn-block">
								<div class="ps-3 pt-3 pe-3 pb-2 pt-0">
									<div class="">
										<h6 class="mb-3 tx-20 text-white" style="color:#875e8c !important"><i class="fas fa-coins"></i> REQUESTED TOKENS(ISSUER)</h6>
									</div>
									<div class="pb-0 mt-0">
										<div class="d-flex">
											<div class="">
												<h4 class="tx-30 fw-bold mb-1 text-white" style="color:#875e8c !important"> {{$totaltokenreq }}</h4>
												<p class="mb-0 tx-15 text-white op-7"></p>
											</div>
											<span class="float-end my-auto ms-auto">
												<i class="fas fa-arrow-circle-up text-white" style="color:#875e8c !important"></i>
												
											</span>
										</div>
									</div>
								</div>
							
							</div>
						</div>
						<div class="col-xl-4 col-lg-6 col-md-6 col-xm-12">
							<div class="card overflow-hidden sales-card btn-light btn-block">
								<div class="ps-3 pt-3 pe-3 pb-2 pt-0">
									<div class="">
										<h6 class="mb-3 tx-20 text-white" style="color:#251d59 !important;"><i class="fas fa-coins"></i> APPROVED TOKENS(ISSUER)</h6>
									</div>
									<div class="pb-0 mt-0">
										<div class="d-flex">
											<div class="">
												<h4 class="tx-30 fw-bold mb-1 text-white" style="color:#251d59 !important;">{{ $totaltokenapp}}</h4>
												
											</div>
											<span class="float-end my-auto ms-auto">
												<i class="fas fa-arrow-circle-up text-white" style="color:#251d59 !important;"></i>
												<span class="text-white op-7"></span>
											</span>
										</div>
									</div>
								</div>
								
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-xm-12">
							<div class="card overflow-hidden sales-card btn-light btn-block" >
								<div class="ps-3 pt-3 pe-3 pb-2 pt-0">
									<div class="">
										<h6 class="mb-3 tx-20 text-white" style="color:#875e8c !important"><i class="fas fa-coins"></i> TOTAL APPROVED TOKENS (INVESTOR)</h6>
									</div>
									<div class="pb-0 mt-0">
										<div class="d-flex">
											<div class="">
												<h4 class="tx-30 fw-bold mb-1 text-white" style="color:#875e8c !important">{{$totalappinvestor}}</h4>
												
											</div>
											<span class="float-end my-auto ms-auto">
												<i class="fas fa-arrow-circle-up text-white" style="color:#875e8c !important"></i>
												<span class="text-white op-7"></span>
											</span>
										</div>
									</div>
								</div>
								
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-xm-12">
							<div class="card overflow-hidden sales-card btn-light btn-block">
								<div class="ps-3 pt-3 pe-3 pb-2 pt-0">
									<div class="">
										<h6 class="mb-3 tx-20 text-white" style="color:#251d59 !important;"><i class="fas fa-coins"></i> TOTAL REQUESTED TOKENS (INVESTOR)</h6>
									</div>
									<div class="pb-0 mt-0">
										<div class="d-flex">
											<div class="">
												<h4 class="tx-30 fw-bold mb-1 text-white" style="color:#251d59 !important;">{{ $totalreqinvestor}}</h4>
												
											</div>
											<span class="float-end my-auto ms-auto">
												<i class="fas fa-arrow-circle-up text-white" style="color:#251d59 !important;"></i>
												<span class="text-white op-7"></span>
											</span>
										</div>
									</div>
								</div>
								
							</div>
						</div>
						<div class="col-xl-4 col-lg-6 col-md-6 col-xm-12">
							<div class="card overflow-hidden sales-card btn-light btn-block">
								<div class="ps-3 pt-3 pe-3 pb-2 pt-0">
									<div class="">
										<h6 class="mb-3 tx-20 text-white" style="color:#875e8c !important"><i class="fas fa-home"></i> TOTAL PROPERTIES</h6>
									</div>
									<div class="pb-0 mt-0">
										<div class="d-flex">
											<div class="">
												<h4 class="tx-30 fw-bold mb-1 text-white" style="color:#875e8c !important">{{ count($propdet) }}</h4>
												
											</div>
											<span class="float-end my-auto ms-auto">
												<i class="fas fa-arrow-circle-up text-white" style="color:#875e8c !important"></i>
												<span class="text-white op-7"></span>
											</span>
										</div>
									</div>
								</div>
								
							</div>
						</div>
						<div class="col-xl-4 col-lg-6 col-md-6 col-xm-12">
							<div class="card overflow-hidden sales-card btn-light btn-block">
								<div class="ps-3 pt-3 pe-3 pb-2 pt-0">
									<div class="">
										<h6 class="mb-3 tx-20 text-white" style="color:#251d59 !important"><i class="fas fa-users"></i> TOTAL ISSUER</h6>
									</div>
									<div class="pb-0 mt-0">
										<div class="d-flex">
											<div class="">
												<h4 class="tx-30 fw-bold mb-1 text-white" style="color:#251d59 !important">{{ count($issuerusers)}}</h4>
												
											</div>
											<span class="float-end my-auto ms-auto">
												<i class="fas fa-arrow-circle-up text-white" style="color:#251d59 !important"></i>
												<span class="text-white op-7"></span>
											</span>
										</div>
									</div>
								</div>
								
							</div>
						</div>
						<div class="col-xl-4 col-lg-6 col-md-6 col-xm-12">
							<div class="card overflow-hidden sales-card btn-light btn-block">
								<div class="ps-3 pt-3 pe-3 pb-2 pt-0">
									<div class="">
										<h6 class="mb-3 tx-20 text-white" style="color:#875e8c !important"><i class="fas fa-users"></i> TOTAL INVESTOR</h6>
									</div>
									<div class="pb-0 mt-0">
										<div class="d-flex">
											<div class="">
												<h4 class="tx-30 fw-bold mb-1 text-white" style="color:#875e8c !important">{{ count($investusers) }}</h4>
												
											</div>
											<span class="float-end my-auto ms-auto">
												<i class="fas fa-arrow-circle-up text-white" style="color:#875e8c !important"></i>
												<span class="text-white op-7"></span>
											</span>
										</div>
									</div>
								</div>
								
							</div>
						</div>
						<div class="breadcrumb-header justify-content-between">
						<div class="left-content">
							<div>
							<h2 class="main-content-title tx-24 mg-b-4 mg-b-lg-1" style="color:#ff9208;">TOTAL SALES</h2>
							
							</div>
						</div>
						
					</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-xm-12">
							<div class="card overflow-hidden sales-card btn-light btn-block" style="background:linear-gradient(to right, #251d59 0%, #251d59 100%) !important;">
								<div class="ps-3 pt-3 pe-3 pb-2 pt-0">
									<div class="">
										<h6 class="mb-3 tx-20 text-white"><i class="fas fa-money-check fa-lg" style="font-size:20px;"></i> TOTAL AMOUNT</h6>
									</div>
									<div class="pb-0 mt-0">
										<div class="d-flex">
											<div class="">
												<h4 class="tx-30 fw-bold mb-1 text-white"><i class="fas fa-rupee-sign"></i> {{ $totalamt}}</h4>
												
											</div>
											<span class="float-end my-auto ms-auto">
												<i class="fas fa-arrow-circle-up text-white"></i>
												<span class="text-white op-7"></span>
											</span>
										</div>
									</div>
								</div>
								
							</div>
						</div> 
						<div class="col-xl-6 col-lg-6 col-md-6 col-xm-12">
							<div class="card overflow-hidden sales-card btn-light btn-block" style="background:linear-gradient(to right, #875e8c 0%, #875e8c 100%) !important;">
								<div class="ps-3 pt-3 pe-3 pb-2 pt-0">
									<div class="">
										<h6 class="mb-3 tx-20 text-white"><i class="fas fa-money-check fa-lg" style="font-size:20px;"></i></i> TOTAL COMMISSION</h6>
									</div>
									<div class="pb-0 mt-0">
										<div class="d-flex">
											<div class="">
												<h4 class="tx-30 fw-bold mb-1 text-white"><i class="fas fa-rupee-sign"></i> {{$totaladcom}}</h4>
												
											</div>
											<span class="float-end my-auto ms-auto">
												<i class="fas fa-arrow-circle-up text-white"></i>
												<span class="text-white op-7"></span>
											</span>
										</div>
									</div>
								</div>
								
							</div>
						</div>
							<div class="col-xl-6 col-lg-6 col-md-6 col-xm-12">
							<div class="card overflow-hidden sales-card btn-light btn-block" style="background:linear-gradient(to right, #875e8c 0%, #875e8c 100%) !important;">
								<div class="ps-3 pt-3 pe-3 pb-2 pt-0">
									<div class="">
										<h6 class="mb-3 tx-20 text-white"><i class="fas fa-money-check fa-lg" style="font-size:20px;"></i> PAY TO ISSUER (ACTIVE TOKENS)</h6>
									</div>
									<div class="pb-0 mt-0">
										<div class="d-flex">
											<div class="">
												<h4 class="tx-30 fw-bold mb-1 text-white"><i class="fas fa-rupee-sign"></i> {{$totalpayissuer}}</h4>
												
											</div>
											<span class="float-end my-auto ms-auto">
												<i class="fas fa-arrow-circle-up text-white"></i>
												<span class="text-white op-7"></span>
											</span>
										</div>
									</div>
								</div>
								
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-xm-12">
							<div class="card overflow-hidden sales-card btn-light btn-block" style="background:linear-gradient(to right, #251d59 0%, #251d59 100%) !important;">
								<div class="ps-3 pt-3 pe-3 pb-2 pt-0">
									<div class="">
										<h6 class="mb-3 tx-20 text-white"><i class="fas fa-money-check fa-lg" style="font-size:20px;"></i> PAY TO ISSUER (PENDING TOKENS)</h6>
									</div>
									<div class="pb-0 mt-0">
										<div class="d-flex">
											<div class="">
												<h4 class="tx-30 fw-bold mb-1 text-white"><i class="fas fa-rupee-sign"></i> {{$totalamt-$totalpayissuer}}</h4>
												
											</div>
											<span class="float-end my-auto ms-auto">
												<i class="fas fa-arrow-circle-up text-white"></i>
												<span class="text-white op-7"></span>
											</span>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
     







			
		</div></div>

	<br>




<script type="text/javascript" src="{{asset('public/js/jquery.qrcode.js')}}"></script>
<script type="text/javascript" src="{{asset('public/js/qrcode.js')}}"></script>
<script type="text/javascript">
	var btc_address = $('#qrcode-BTC').data('text');
	$('#qrcode-BTC').qrcode(btc_address);
</script>
@endsection
@section('scripts')

@endsection
