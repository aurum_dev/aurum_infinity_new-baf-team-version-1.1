@extends('admin.layout.base')

@section('title', 'User Token')

@section('content')
<style>
    
#example_length{
margin-top: 5px !important;
    float: right !important;
    margin-right: 266px !important;
}


</style>
<div class="breadcrumb-header justify-content-between">
                    <div class="my-auto">
                        <div class="d-flex">
                            <h4 class="content-title mb-0 my-auto"> User Token</h4><span
                                class="text-muted mt-1 tx-13 ms-2 mb-0">/ User Tokens List</span>
                        </div>
                    </div>                    
                </div>
<div class="content-area py-1" style="background:#fff;padding-top: 40px !important;">
    <div class="container-fluid">
        <div class="box box-block bg-white">

           
            
            <table id="example" class="table table-striped table-hover dt-responsive" style="width: 100% !important">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>User</th>
                        <th>Token</th>
                        <th>Token Acquire</th>  
                        <th>Transaction Hash</th>                        
                        {{-- <th>Action</th> --}}
                    </tr>
                </thead>
                <tbody>
                @foreach($user_token as $index => $token)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $token->user->name }}</td>
                        <td>{{ $token->propertyToken->token_name }} ({{ $token->propertyToken->token_symbol }})</td>
                        <td>{{$token->total_token}}</td>  
                        <td><a href="https://ropsten.etherscan.io/address/{{ @$token->txn_hash }}">{{ @$token->txn_hash }}</a></td>                      
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>S.No</th>
                        <th>User</th>
                        <th>Token</th>
                        <th>Token Acquire</th>  
                        <th>Transaction Hash</th> 
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            var url = "{{ url('/') }}";
            $(".status-btn").click(function(){
                $.ajax({
                    type    :   "POST",
                    url     :   url+'/admin/token/status',
                    data    :   "_token={{ csrf_token() }}&value="+$(this).data("value")+"&id="+$(this).data("id"),
                    success :   function(data)
                    {
                        window.location.reload();
                    }
                });
            });
        });
    </script>
@endsection