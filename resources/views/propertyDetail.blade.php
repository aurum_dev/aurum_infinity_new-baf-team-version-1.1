@extends('layout.appHome')

@section('content')

<div id="page" class="site">
            <!-- #content -->
            <div id="content" class="site-content">
               <div id="primary" class="content-area">
                  <main id="main" class="site-main">
                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

                       
                     <div class="container page-container">
                        <article id="post-3351" class="post-3351 page type-page status-publish hentry">
                           <header class="entry-header">
                           </header>
                           <!-- .entry-header -->
                           <div class="entry-content">
                          
                          
                    <style>
                    .product-slider { padding: 0; }

.product-slider #carousel { border: 1px solid #ebebeb; margin: 0; }

.product-slider #thumbcarousel { margin: 12px 0 0; padding: 0 0; }

.product-slider #thumbcarousel .item { text-align: left; }

.product-slider #thumbcarousel .item .thumb { border: 4px solid #cecece; width: 20%; margin: 0 0 0 3px; display: inline-block; vertical-align: middle; cursor: pointer; max-width: 98px; }

.product-slider #thumbcarousel .item .thumb:hover { border-color: #1089c0; }

.product-slider .item img { width: 100%; height: auto; }

.carousel-control { color: #0284b8; text-align: center; text-shadow: none; font-size: 30px; width: 30px; height: 30px; line-height: 20px; top: 23%; }

.carousel-control:hover, .carousel-control:focus, .carousel-control:active { color: #333; }

.carousel-caption, .carousel-control .fa { font: normal normal normal 30px/26px FontAwesome; }
.carousel-control { background-color: rgba(0, 0, 0, 0); bottom: auto; font-size: 20px; left: 0; position: absolute; top: 30%; width: auto; }

.carousel-control.right, .carousel-control.left { background-color: rgba(0, 0, 0, 0); background-image: none; }
                    </style>      
                          <div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid home-thirteen-services-upper-space vc_custom_1565259781064 vc_row-has-fill vc_column-gap-30 vc_row-o-content-middle vc_row-flex" style="opacity:9;position: relative; left: -89.5px; box-sizing: border-box; width: 1349px; padding-left: 89.5px; padding-right: 89.5px;padding-top:20px !important;">
   <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12" style="padding:0 !important;">
      <div class="vc_column-inner vc_custom_1565260206953">
         <div class="wpb_wrapper">
        



<div class="product-slider">
  <div id="carousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" >

<?php 

$gallery=explode("#",$property->galleryImage);
?>

     
      <div class="item active"> <img src="../<?php echo $gallery[0]; ?>"> </div>
   
 <?php 
                  
              for($i=1;$i<count($gallery);$i++){
                ?>
              
                <div class="item"> <img src="../<?php echo $gallery[$i]; ?>"> </div>

                <?php
            
              }
             ?>     




    </div>
  </div>
  <div class="clearfix">
    <div id="thumbcarousel" class="carousel slide" data-interval="false">
      <div class="carousel-inner" style="margin-left:16px;">
   <?php 

$gallery=explode("#",$property->galleryImage);

$numb =1;
   $flag =1;

?>
      <div class="item">
         <?php for($i=0;$i<count($gallery);$i++){ ?>
         <div data-target="#carousel" data-slide-to="<?php echo $i ?>" class="thumb"><img src="../<?php echo $gallery[$i] ?>"></div>
         <?php
$tt=count($gallery);

if($numb==5){ 

            ?>
                </div><div class="item">

             <?php
              }
              
               
            $numb++;
            $flag++;
           } ?>
      
      </div></div>
      <!-- /carousel-inner --> 
      <a class="left carousel-control" href="#thumbcarousel" role="button" data-slide="prev"> <i class="fa fa-angle-left" aria-hidden="true"></i> </a> <a class="right carousel-control" href="#thumbcarousel" role="button" data-slide="next"><i class="fa fa-angle-right" aria-hidden="true"></i> </a> </div>
    <!-- /thumbcarousel --> 
    
  </div>
</div>

    <script>
  
$('.carousel-inner>div:first-child').addClass('active');
       $('.carousel-inner>div:first-child').addClass('active');
    </script>    
            
         </div>
      </div>
   </div>
   <div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
      <div class="vc_column-inner vc_custom_1565260220932">
         <div class="wpb_wrapper">
            <h2 style="font-size: 26px !important;
               color: #251d59 !important;
               line-height: 23px !important;
               text-align: left !important;
               font-weight: 600 !important;letter-spacing: 0.03px;" class="vc_custom_heading font-weight-regular title-letter-spacing vc_custom_1564561171454">{{ @$property->propertyName }} <span style="color:#9285a6;font-size:17px;">, {{ @$property->propertyCity }}</span></h2>
           
            <div style="font-size: 15px;color: #000000;line-height: 28px;text-align: left" class="vc_custom_heading font-weight-regular home-thirteen-para-right-align vc_custom_1565157773347">{{ @$property->propertyDesc }}</div>
           <div class="home-thirteen-pricing-table wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12">
   <div class="vc_column-inner vc_custom_1565159240643" style="padding-left:0 !important;padding-right:0 !important">
      <div class="wpb_wrapper">
         <div class="rt-pricing-table element-nine rt1514199466 " style='padding-top:0 !important;'> 
            <div class="holder">
               <div class="spotlight-tag">
                  <p class="spotlight-tag-text">Popular Choice</p>
               </div>
               <div class="heading">
                  <h4 class="title">OVERVIEW</h4>
               </div>
               <div class="pricing">
              <table style="border:#bbb3b3 1px solid;padding: 5px 30px;text-align:left;margin-top:10px;width:100%;">
                                                        <tbody><tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Token Name</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->tokenName }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Token Value</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->tokenValue }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Minimum Investment</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->minimumInvest }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Property Size</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->propertySize }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Target IRR</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->targetIrr }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Average Yield</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->averageYield }}</td>
                                                        </tr>
                                                         <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Property Status</td>
                                                         @if($property->status=="live")
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;text-transform :uppercase;color:#008800;font-size:25px;">{{ @$property->status }}</td>
                                                        @elseif($property->status=="soldout")
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;text-transform :uppercase;color:#ff0000;font-size:25px;">{{ @$property->status }}</td>
                                                        @elseif($property->status=="pending")
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;text-transform :uppercase;color:#ff0000;font-size:25px;">{{ @$property->status }}</td>
                                                        @endif
                                                        </tr>
                                                        
                                                        </tbody></table>
                                                        <a target="_blank" href="https://mumbai.polygonscan.com/address/{{$property->contract_address}}" class="btn btn-primary" style="background-color: #4a3259;border-color: #9285a6;font-size:14px;margin-top:20px;padding:0 10px;"><i class="fa fa-credit-card"></i> | View on Polygon Scan</a>



<?php if($property->status=="live"){ ?>
                             @if (Auth::check())

                                 <a href="{{ url('applyInvest/'.$property->id) }}" class="btn btn-primary" style="background-color: #251d59;border-color: #251d59;margin-left:8px;font-size:14px;padding: 0 10px;margin-top:20px;"><i class="fa fa-money"></i> | Ready to Invest</a>
                             @else
                                <a href="{{('../profile')}}" class="btn btn-primary" style="padding: 0 10px;background-color: #251d59;border-color: #251d59;margin-left:8px;font-size:14px;margin-top:20px;"><i class="fa fa-user"></i> | Become a Member</a>
                                @endif
                                </br>
                                @if (Auth::check())
                                    <small style="color: #b9b9b9">Note: Please update the profile information before investing.</small>
                                @endif
                        <?php } ?>





                                                      
               </div>
               
               
            </div>
         </div>
      </div>
   </div>
</div>  
            <!-- radiantthemes-custom-button -->
         </div>
      </div>
   </div>
</div>
                          
                         <section data-vc-full-width="true" data-vc-full-width-init="true" class="vc_section vc_custom_1525849326254 vc_section-has-fill" style="opacity:9;position: relative; left: -89.5px; box-sizing: border-box; width: 1349px; padding-left: 89.5px; padding-right: 89.5px;">
  
   <div class="vc_row-full-width vc_clearfix"></div>
   <div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid vc_custom_1526899957936" style="position: relative; left: -89.5px; box-sizing: border-box; width: 1349px; padding-left: 89.5px; padding-right: 89.5px;">
      <div class="wpb_column vc_column_container vc_col-sm-12">
         <div class="vc_column-inner vc_custom_1526899963628">
            <div class="wpb_wrapper">
               <div class="rt-tab element-three rt1948670034 ">
                  <ul class="nav-tabs">
                     <li data-tab-icon="true" class="matchHeight active" style=""><a data-toggle="tab" href="#c9018b76-49fa-9" aria-expanded="false"><span><i class="fa fa-home"></i> Property Details</span></a></li>
                     <li data-tab-icon="true" class="matchHeight" style=""><a data-toggle="tab" href="#c9018b76-49fa-10" aria-expanded="false"><span><i class="fa fa-search"></i> Features</span></a></li>
                     <li data-tab-icon="true" class="matchHeight" style=""><a data-toggle="tab" href="#c9018b76-49fa-11" aria-expanded="false"><span><i class="fa fa-credit-card"></i> Financials</span></a></li>
                     <li data-tab-icon="true" class="matchHeight" style=""><a data-toggle="tab" href="#c9018b76-49fa-12" aria-expanded="true"><span><i class="fa fa-file-text-o"></i> Lease Details</span></a></li>
 <li data-tab-icon="true" class="matchHeight" style=""><a data-toggle="tab" href="#c9018b76-49fa-13" aria-expanded="true"><span><i class="fa fa-bar-chart"></i> Return Analysis</span></a></li>
  <li data-tab-icon="true" class="matchHeight" style=""><a data-toggle="tab" href="#c9018b76-49fa-14" aria-expanded="true"><span><i class="fa fa-file"></i> Reports</span></a></li>
                  
                                   
                 </ul>
                  <div class="tab-content">
                   
                     <div id="c9018b76-49fa-9" class="tab-pane fade active in">
                     
                     
                     
 <div class="home-thirteen-pricing-table wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12">
   <div class="vc_column-inner vc_custom_1565159240643" style="padding-left:0 !important;padding-right:0 !important">
      <div class="wpb_wrapper">
         <div class="rt-pricing-table element-nine rt1514199466 " style='padding-top:0 !important;'> 
            <div class="holder">
               <div class="spotlight-tag">
                  <p class="spotlight-tag-text">Popular Choice</p>
               </div>
               <div class="heading">
                  <h4 class="title">Property Overview</h4>
               </div>
               <div class="pricing">
              <table style="border:#bbb3b3 1px solid;padding: 5px 30px;text-align:left;margin-top:10px;width:100%;">
                                                        <tbody>

<tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Property Name</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->propertyName }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Property State / City / District</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->propertyState }} / {{ @$property->propertyCity }} / {{ @$property->propertyDistrict }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Property Type</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->propertyType }} </td>
                                                        </tr>
                                                        
                                                            <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Token Name</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->tokenName }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Token Value</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->tokenValue }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Minimum Investment</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->minimumInvest }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Property Size</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->propertySize }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Target IRR</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->targetIrr }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Average Yield</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->averageYield }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Property Description</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->propertyDesc }} </td>
                                                        </tr>
                                                        
                                                        </tbody></table>
                                                       
               </div>
               
               
            </div>
         </div>
      </div>
   </div>
</div> 



                     
                     
                     </div>
                     <div id="c9018b76-49fa-10" class="tab-pane fade">




<div class="home-thirteen-pricing-table wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12">
   <div class="vc_column-inner vc_custom_1565159240643" style="padding-left:0 !important;padding-right:0 !important">
      <div class="wpb_wrapper">
         <div class="rt-pricing-table element-nine rt1514199466 " style='padding-top:0 !important;'> 
            <div class="holder">
               <div class="spotlight-tag">
                  <p class="spotlight-tag-text">Popular Choice</p>
               </div>
               <div class="heading">
                  <h4 class="title">Features</h4>
               </div>
               <div class="pricing">
              <table style="border:#bbb3b3 1px solid;padding: 5px 30px;text-align:left;margin-top:10px;width:100%;">
                                                        <tbody>

<tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Proponent</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->Proponent }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Property On Map</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"><a style="color:#ee335e;" href="{{ @$property->Onmap }}" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i> Map Link</a></td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Address</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->Address }} </td>
                                                        </tr>
                                                        
                                                            <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Property Status</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->propertyStatus }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Build Year</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->buildYear }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">RERA Number</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->reraNumber }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Property Manager</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->propertyManager }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Custodian</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->Custodian }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Property Amenities</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">
<?php $amenity=explode("#",$property->propertyAmenity);
$amty=implode(",",$amenity);

?>
{{$amty}}


                                                            </td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Property Technical Specification</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"><a style="color:#ee335e;" href='../{{ @$property->propertyTechSpecs }}' target="_blank"><i class="fa fa-file-pdf-o"></i> Download</a> </td>
                                                        </tr>
                                                        
                                                        </tbody></table>
                                                       
               </div>
               
               
            </div>
         </div>
      </div>
   </div>
</div> 











                     </div>
                     <div id="c9018b76-49fa-11" class="tab-pane fade">


<div class="home-thirteen-pricing-table wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12">
   <div class="vc_column-inner vc_custom_1565159240643" style="padding-left:0 !important;padding-right:0 !important">
      <div class="wpb_wrapper">
         <div class="rt-pricing-table element-nine rt1514199466 " style='padding-top:0 !important;'> 
            <div class="holder">
               <div class="spotlight-tag">
                  <p class="spotlight-tag-text">Popular Choice</p>
               </div>
               <div class="heading">
                  <h4 class="title">Financials</h4>
               </div>
               <div class="pricing">
              <table style="border:#bbb3b3 1px solid;padding: 5px 30px;text-align:left;margin-top:10px;width:100%;">
                                                        <tbody>

<tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Cost of Premise</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->costOfPremise }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Stamp Duty Registration</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->stampDutyReg }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Acquisition Fee</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">&#8377; {{ @$property->acquisitionFee }} </td>
                                                        </tr>
                                                        
                                                            <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Other Charges</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">&#8377; {{ @$property->otherCharges }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Legal Fees</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">&#8377; {{ @$property->legalFees }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Maintenance Reserve</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->maintenanceReserve }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Admin Expenses</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">&#8377; {{ @$property->adminExpenses }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Monthly Opex</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">&#8377; {{ @$property->monthlyOpex }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Monthly Property Tax</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">&#8377; 

{{ @$property->monthlyPropertyTax }}


                                                            </td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Other Monthly Regulatory Charges</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">&#8377;  {{ @$property->regulatoryCharges }} </td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Total Acquisition cost</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">&#8377;  {{ @$property->totalAcquisitionCost }} </td>
                                                        </tr>
                                                        </tbody></table>
                                                       
               </div>
               
               
            </div>
         </div>
      </div>
   </div>
</div> 

                     </div>
                     <div id="c9018b76-49fa-12" class="tab-pane fade">


<div class="home-thirteen-pricing-table wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12">
   <div class="vc_column-inner vc_custom_1565159240643" style="padding-left:0 !important;padding-right:0 !important">
      <div class="wpb_wrapper">
         <div class="rt-pricing-table element-nine rt1514199466 " style='padding-top:0 !important;'> 
            <div class="holder">
               <div class="spotlight-tag">
                  <p class="spotlight-tag-text">Popular Choice</p>
               </div>
               <div class="heading">
                  <h4 class="title">Financials</h4>
               </div>
               <div class="pricing">
              <table style="border:#bbb3b3 1px solid;padding: 5px 30px;text-align:left;margin-top:10px;width:100%;">
                                                        <tbody>

<tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Carpet Area (Sq Ft)</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->carpetArea }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Built Up Area (Sq Ft)</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->builtUpArea }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Monthly Rent</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">&#8377; {{ @$property->monthlyRent }} </td>
                                                        </tr>
                                                        
                                                            <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Leased Period</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->leasedPeriod }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Security Deposit</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">&#8377; {{ @$property->securityDeposit }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Escalation</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->escalation }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Tenants</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> {{ @$property->tenants }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Fitted Out</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->fittedOut }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Monthly Maintenance</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">&#8377; 

{{ @$property->monthlyMaintenance }}


                                                            </td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Lease Chart</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> {{ @$property->leaseChart }} </td>
                                                        </tr>
                                                       
                                                        </tbody></table>
                                                       
               </div>
               
               
            </div>
         </div>
      </div>
   </div>
</div> 






                     </div>
                      <div id="c9018b76-49fa-13" class="tab-pane fade">



<div class="home-thirteen-pricing-table wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12">
   <div class="vc_column-inner vc_custom_1565159240643" style="padding-left:0 !important;padding-right:0 !important">
      <div class="wpb_wrapper">
         <div class="rt-pricing-table element-nine rt1514199466 " style='padding-top:0 !important;'> 
            <div class="holder">
               <div class="spotlight-tag">
                  <p class="spotlight-tag-text">Popular Choice</p>
               </div>
               <div class="heading">
                  <h4 class="title">Financials</h4>
               </div>
               <div class="pricing">
              <table style="border:#bbb3b3 1px solid;padding: 5px 30px;text-align:left;margin-top:10px;width:100%;">
                                                        <tbody>

<tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Gross Yield (%)</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->grossYield }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Expected IRR (%)</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->expectedIr }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Token Lockin Period (months)</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->tokenLockinPeriod }} </td>
                                                        </tr>
                                                        
                                                            <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Aurum Facilation fees (%)</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">{{ @$property->aurumFacilationFees }}</td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Performance Fees</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;">&#8377; {{ @$property->performanceFees }}</td>
                                                        </tr>
                                                        
                                                       
                                                       
                                                        </tbody></table>
                                                       
               </div>
               
               
            </div>
         </div>
      </div>
   </div>
</div> 


                     </div>
                      <div id="c9018b76-49fa-14" class="tab-pane fade">

<div class="home-thirteen-pricing-table wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12">
   <div class="vc_column-inner vc_custom_1565159240643" style="padding-left:0 !important;padding-right:0 !important">
      <div class="wpb_wrapper">
         <div class="rt-pricing-table element-nine rt1514199466 " style='padding-top:0 !important;'> 
            <div class="holder">
               <div class="spotlight-tag">
                  <p class="spotlight-tag-text">Popular Choice</p>
               </div>
               <div class="heading">
                  <h4 class="title">Property Reports</h4>
               </div>
               <div class="pricing">
              <table style="border:#bbb3b3 1px solid;padding: 5px 30px;text-align:left;margin-top:10px;width:100%;">
                                                        <tbody>

<tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Investment Deck</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"><a style="color:#ee335e;" href='../{{ @$property->propertyInvestmentDeck }}' target="_blank"><i class="fa fa-file-pdf-o"></i> Download</a></td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Title Report</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"><a style="color:#ee335e;" href='../{{ @$property->propertytitleReport }}' target="_blank"><i class="fa fa-file-pdf-o"></i> Download</a></td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Property Due Diligence</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"><a style="color:#ee335e;" href='../{{ @$property->propertyDueDiligence }}' target="_blank"><i class="fa fa-file-pdf-o"></i> Download</a></td>
                                                        </tr>
                                                        
                                                            <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Micro market Report</td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"><a style="color:#ee335e;" href='../{{ @$property->propertyMicroMarket }}' target="_blank"><i class="fa fa-file-pdf-o"></i> Download</a></td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Property Management Agreement </td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"><a style="color:#ee335e;" href='../{{ @$property->propertyMgmtAgrmt }}' target="_blank"><i class="fa fa-file-pdf-o"></i> Download</a></td>
                                                        </tr>
                                                        <tr style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border:#bbb3b3 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Valuation Report </td>
                                                        <td style="border:#bbb3b3 1px solid;padding: 5px 10px;text-align:left;"><a style="color:#ee335e;" href='../{{ @$property->propertyValReport }}' target="_blank"><i class="fa fa-file-pdf-o"></i> Download</a></td>
                                                        </tr>
                                                       
                                                       
                                                        </tbody></table>
                                                       
               </div>
               
               
            </div>
         </div>
      </div>
   </div>
</div> 


                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="vc_row-full-width vc_clearfix"></div>
</section>
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                           </div>
                           <!-- .entry-content -->
                        </article>
                        <!-- #post-3351 -->             
                     </div>
                  </main>
                  <!-- #main -->
               </div>
               <!-- #primary -->
            </div>
            <!-- #content -->
         </div>

@endsection
