@extends('layout.app')

@section('content')
<!-- Breadcrumb --><br>
<div class="page-content">
    <div class="pro-breadcrumbs">
        <div class="container">
            <a href="{{url('/dashboard')}}" class="pro-breadcrumbs-item">Home</a>
            <span>/</span>
            <a href="#" class="pro-breadcrumbs-item">Privacy Policy</a>
        </div>
    </div><br>
    <!-- End Breadcrumb -->
    <!-- Property Head Starts -->
    <div class="property-head grey-bg pt30">
        <div class="container">
            <div class="property-head-btm row">
                <div class="col-md-12">
                    <h2 class="pro-head-tit" style="text-transform:uppercase;font-size:25px;">Privacy Policy</h2>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- Property Head Ends -->
    <!-- Blog content -->
    <div class="post-wrap">
        <section class="post-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="content-boxed">

                            <?php
$tmp = \App\Privacy::orderBy('created_at','desc')->first();
echo $tmp->privacycontent;
?>

    
</div>

                    </div>                    
                </div>
            </div>
        </section>
    </div>
    <!-- Blog content -->
</div>
@endsection


@section('scripts')

@endsection
