@extends('layout.appHome')

@section('content')
<style>
                       .progress-bar-info{
                        background-color: #875e8c !important;
                       }
.progress
{
   height: 16px !important;
}
                    </style> 
                    <style>
 

/* ================== Badge Overlay CSS ========================*/
.badge-overlay {
    position: absolute;
    left: -10px;
    top: 1px;
    width: 100%;
    height: 100%;
    overflow: hidden;
    pointer-events: none;
    z-index: 100;
    -webkit-transition: width 1s ease, height 1s ease;
    -moz-transition: width 1s ease, height 1s ease;
    -o-transition: width 1s ease, height 1s ease;
    transition: width 0.4s ease, height 0.4s ease
}

/* ================== Badge CSS ========================*/
.badge {
    margin: 0;
    padding: 0;
    color: white;
    padding: 10px 53px;
    font-size: 15px;
    font-family: Arial, Helvetica, sans-serif;
    text-align: center;
    line-height: normal;
    text-transform: uppercase;
    background: #ed1b24;
}

.badge::before, .badge::after {
    content: '';
    position: absolute;
    top: 0;
    margin: 0 -1px;
    width: 100%;
    height: 100%;
    background: inherit;
    min-width: 55px
}

.badge::before {
    right: 100%
}

.badge::after {
    left: 100%
}

/* ================== Badge Position CSS ========================*/


.top-right {
    position: absolute;
    top: 0;
    right: 0;
    -ms-transform: translateX(30%) translateY(0%) rotate(45deg);
    -webkit-transform: translateX(30%) translateY(0%) rotate(45deg);
    transform: translateX(30%) translateY(0%) rotate(45deg);
    -ms-transform-origin: top left;
    -webkit-transform-origin: top left;
    transform-origin: top left;
}

.bottom-full {
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    text-align: center;
}

/* ================== Badge color CSS ========================*/
.badge.red {
    background: #ed1b24;
    border-radius: 0;
}

.badge.orange {
    background: #008800;
     border-radius: 0;
}

.badge.pink {
    background: #ee2b8b;
     border-radius: 0;
}

.badge.blue {
    background: #00adee;
     border-radius: 0;
}

.badge.green {
    background: #b4bd00;
}


</style>
<div id="page" class="site">
            <!-- #content -->
            <div id="content" class="site-content">
               <div id="primary" class="content-area">
                  <main id="main" class="site-main">
                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

                         
                      <div class="wraper_inner_banner">
   <div class="wraper_inner_banner_main">
      <div class="container">
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="inner_banner_main">
                  <p class="title" style="text-align:left;font-size:36px;">
                     FEATURED PROPERTIES
                  </p>
                  <p class="subtitle" style="text-align:left;font-size:16px;color:#fff !important;text-align: left;">
                    @if (Auth::check())
                                
                                    Welcome Member, you now have access to greater financial details related to our offerings,<br> so you can make an informed investment decision. <br>Once you are ready,  click the "Ready To Invest" button to get the process started
                                
                            @else
                               Don't forget, to start investing you must first <strong>"Become A Member"</strong>. All you need to submit is a first/last name, email address and create your own password. Membership will provide you greater financial details related to our offerings, give you instant updates and additional support from our on-boarding customer service team.
                            @endif
                  </p>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>  
                     <div class="container page-container">
                        <article id="post-3351" class="post-3351 page type-page status-publish hentry">
                           <header class="entry-header">
                           </header>
                           <!-- .entry-header -->
                           <div class="entry-content">
                          
                           
                             
                              <div class="vc_row-full-width vc_clearfix"></div>
                              <section data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section background-position-center-top position-relative home-thirteen-tablet-services vc_custom_1565259935698 vc_section-has-fill">
                                
                                 <div class="vc_row-full-width vc_clearfix"></div>
                                
                                
                                 
                                 
                                 
                                 
                                 
                                 <div class="vc_row-full-width vc_clearfix"></div>
                                 <div class="vc_row wpb_row vc_row-fluid background-position-right home-thirteen-responsive home-thirteen-responsive-tablet vc_custom_1565174860866 vc_row-has-fill">
                                    <div class="wpb_column vc_column_container vc_col-sm-2">
                                       <div class="vc_column-inner vc_custom_1565169620144">
                                          <div class="wpb_wrapper"></div>
                                       </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-8">
                                       <div class="vc_column-inner vc_custom_1565169606522">
                                          <div class="wpb_wrapper">
                                             <h2 style="font-size: 35px;
    color: #000000;
    line-height: 48px;
    text-align: center;
    color: #251d59;
    letter-spacing: 0.03px;
" class="vc_custom_heading font-weight-bold vc_custom_1564996682116" ></h2>
                                             <div style="font-size: 15px;color: #000000;line-height: 28px;text-align: center" class="vc_custom_heading font-weight-regular vc_custom_1564996711569" ></div>
                                          </div>
                                       </div>
                                    </div>
                                   
                                 </div>
                                 <div class="vc_row wpb_row vc_row-fluid vc_custom_1533531188784">
   <div class="wpb_column vc_column_container vc_col-sm-12">
      <div class="vc_column-inner vc_custom_1533531180754">
         <div class="wpb_wrapper">
            <div class="wpb_raw_code wpb_content_element wpb_raw_html vc_custom_1608822318395">
               <div class="wpb_wrapper">
                  <div id="content" class="site-content">
                     <div id="primary" class="content-area">
                        <main id="main" class="site-main post-410 post type-post status-publish format-standard has-post-thumbnail hentry category-blogging category-remarketing category-seo category-social-media-marketing tag-business tag-corporate tag-marketing tag-seo">
                           <!-- wraper_blog_main -->
                           <div class="wraper_blog_main style-two">
                              <div class="container" style="padding-top:0 !important;">
                                 <!-- row -->
                                 <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <!-- blog_main -->
                                       <div class="blog_main">
                                          <div class="row isotope-blog-style" style="position: relative; height: 536.562px;">
                                          

 @foreach ($property as $key => $value)

<?php 
$totaltoken=0;
foreach ($wallet as $key => $value1){
   if($value1->property_id==$value->id && $value1->status==1){
      $totaltoken+=$value1->total_tokens;
   }
}

?>
     
                                             <div class="isotope-blog-style-item col-lg-4 col-md-4 col-sm-6 col-xs-12" style="position: absolute; left: 0px; top: 0px;">
                                                <article id="post-410" class="style-two post-410 post type-post status-publish format-standard has-post-thumbnail hentry category-blogging category-remarketing category-seo category-social-media-marketing tag-business tag-corporate tag-marketing tag-seo">
<?php 

$gallery=explode("#",$value->galleryImage);

?>  
<div class="badge-overlay">
   @if($value->status=="live")
   <span class="top-right badge orange">Live</span>
   @else
    <span class="top-right badge red">Soldout</span>
    @endif
            </div>                                
                                                   <div class="post-thumbnail"><a href="<?php echo "propertyDetail/".$value->id; ?>"><img src="{{$gallery[0]}}" class="attachment-full size-full wp-post-image" alt="Blog-Image-001"  sizes="(max-width: 1176px) 100vw, 1176px" style="height:360px;" data-no-retina="" width="1176" height="784">           </a></div>
                                                   <!-- .post-thumbnail -->
                                                   <div class="entry-main">
                                                      <header class="entry-header">
                                                     
                                                         <h3 class="entry-title" style="margin-bottom:0 !important;">
                                                            <a href="<?php echo "propertyDetail/".$value->id; ?>" rel="bookmark" style="font-size: 25px;color: #000000;text-align: left;color:#251d59;letter-spacing:0.03px">{{ @$value->propertyName }}</a></h3>
                                                       <p style="font-size:18px;">{{ @$value->propertyCity }}   </p>
                                                      
                                                        <div class="progress">
    <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="{{ ($totaltoken*100)/$value->tokenSupply}}" aria-valuemin="0" aria-valuemax="100" style="
    width: <?php echo ($totaltoken*100)/$value->tokenSupply;?>%">
    
    </div>
  </div>
                                                     
                                                       <span class="progress-txt"><b style="font-weight:500;">{{ ($totaltoken*100)/$value->tokenSupply}}% FUNDED</b>&nbsp;&nbsp;   {{$totaltoken}} OF {{ @$value->tokenSupply }}</span>
                                                      </header>
                                                      
                                                      <!-- .entry-header -->
                                                      <div class="entry-content">
                                                    <div style="width:100%;height:72px;">
@if($value->trusted==1)
                                                      <a href="javascript:void(0);" class="btn btn-primary" style="background-color: #4a3259;border-color: #9285a6;font-size:14px;padding:0 7px;margin-left:8px;margin-top:8px;"><i class="fa fa-lock"></i> | Trusted</a>
@endif
@if($value->highgrowth==1)
<a href="javascript:void(0);" class="btn btn-primary" style="background-color: #251d59;border-color: #9285a6;margin-left:8px;margin-top:8px;font-size:14px;padding:0 7px;"><i class="fa fa-bar-chart"></i> | High Growth</a>
@endif
@if($value->office==1)
<a href="javascript:void(0);" class="btn btn-primary" style="background-color: #875e8c;border-color: #9285a6;margin-left:8px;margin-top:8px;font-size:14px;padding:0 7px;"><i class="fa fa-building-o"></i> | Office</a>
@endif
@if($value->gradea==1)
<a href="javascript:void(0);" class="btn btn-primary" style="background-color: #73187e;border-color: #9285a6;margin-top:8px;font-size:14px;padding:0 7px;margin-left:8px;"><i class="fa fa-check"></i> | Grade A</a>
@endif</div>

                                                        <table style="border:#ebebeb 1px solid;padding: 5px 30px;text-align:left;margin-top:10px;width:100%;">
                                                        <tr style="border:#ebebeb 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border: #ebebeb 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Asset Type</td>
                                                        <td style="border:#ebebeb 1px solid;padding: 5px 10px;text-align:left;">{{ @$value->propertyType }}</td>
                                                        </tr>
                                                         <tr style="border:#ebebeb 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border: #ebebeb 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Total Tokens</td>
                                                        <td style="border:#ebebeb 1px solid;padding: 5px 10px;text-align:left;">{{ @$value->tokenSupply }}</td>
                                                        </tr>
                                                         <tr style="border:#ebebeb 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border: #ebebeb 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Expected IRR</td>
                                                        <td style="border:#ebebeb 1px solid;padding: 5px 10px;text-align:left;"> {{ @$value->expectedIr }}%</td>
                                                        </tr>
                                                         <tr style="border:#ebebeb 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border: #ebebeb 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;">Min Investment<br>(in Tokens)</td>
                                                        <td style="border:#ebebeb 1px solid;padding: 5px 10px;text-align:left;"> {{ @$value->minimumInvest }}</td>
                                                        </tr>
                                                        <tr style="border:#ebebeb 1px solid;padding: 5px 10px;text-align:left;"> 
                                                        <td style="border: #ebebeb 1px solid; padding: 5px 10px; text-align: left; font-family: 'Poppins';font-size: 15px; color: #251d59; font-weight: 500;"> Amount <br>(1 Token Value) </td>
                                                        <td style="border:#ebebeb 1px solid;padding: 5px 10px;text-align:left;">&#8377; {{ @$value->tokenValue }}</td>
                                                        </tr>
                                                        </table>
                                                      </div>
                                                      <!-- .entry-content -->
                                                      
                                                      <!-- .post-meta -->
                                                   </div>
                                                   <!-- .entry-main -->
                                                </article>
                                                <!-- #post-## -->
                                             </div>
                                            
                                        
                                           @endforeach
                                          
                                          
                                          
                                          
                                          
                                          
                                          
                                          
                                          
                                          
                                          
                                          </div>
                                          
                                       </div>
                                       <!-- blog_main -->
                                    </div>
                                 </div>
                                 <!-- row -->
                              </div>
                           </div>
                           <!-- wraper_blog_main -->
                        </main>
                        <!-- #main -->
                     </div>
                     <!-- #primary -->
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
                                 
                                 
                            
                              </section>
                             
                                 
                                 
                                 
                                  <div class="vc_row-full-width vc_clearfix"></div>
                                
                                 
                                
                                
                                

                    
                    
    

                                   
                                 
                              
                              
                              
                              
                           </div>
                           <!-- .entry-content -->
                        </article>
                        <!-- #post-3351 -->             
                     </div>
                  </main>
                  <!-- #main -->
               </div>
               <!-- #primary -->
            </div>
            <!-- #content -->
         </div>


@endsection
