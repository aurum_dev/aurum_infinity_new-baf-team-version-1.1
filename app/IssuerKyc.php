<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IssuerKyc extends Model
{
    protected $table = 'issuer_kyc';
    
    protected $fillable = [
        'user_id',
        'cname',
        'caddress',
        'city',
        'state',
        'cpan',
        'cgst',
    ];
}
