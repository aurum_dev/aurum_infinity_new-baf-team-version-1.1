<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IssuerTokenRequest extends Model
{
    //

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }   

    public function propertydetails()
    {
        return $this->hasMany('App\PropertyDetails','token_request_id','id');
    }
}
