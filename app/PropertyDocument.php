<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyDocument extends Model
{
    protected $fillable = [
        'property_id',
        'user_id',
        'prospectus',
        'title_report',
        'subscription',
        'due_diligence',
        'updates'
    ];
}
