<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProperty extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'propertyName'                  => 'required|max:200',
            'propertyLocation'              => 'required|max:200',
            'propertyType'                  => 'required',
            'leased_period'                 => 'required|numeric|min:0|max:100',
            'rera_number'                   => 'required|numeric|min:0|max:999999999',
            'dev_name'                      => 'required|max:200',
            'property_age'                  => 'required|numeric|min:0|max:100',
            'tenant_name'                   => 'required|max:200',
            'rent_escalation'               => 'required|numeric|min:0|max:100',
            'notice_period'                 => 'required|numeric|min:0|max:999',
            'carpet_area'                   => 'required|numeric|min:0|max:999999999',
            'built_up_area'                 => 'required|numeric|min:0|max:999999999',
            'gross_yield'                   => 'required|numeric|min:0|max:100',
            'annual_rent'                   => 'required|numeric|min:0|max:999999999',
            'totalDealSize'                 => 'required|numeric|min:0|max:999999999',
            'expectedIrr'                   => 'required|numeric|min:0|max:100',
            'initialInvestment'             => 'required|numeric|min:0|max:999999999',  
            'holdingPeriod'                 => 'required|numeric|min:0|max:100',
            'total_sft'                     => 'required|numeric|min:0|max:999999999',
            'propertyOverview'              => 'required', //Long text
            'propertyLocationOverview'      => 'required', //Long text
            'geo_location'                  => 'required|max:200',
            'street_address'                => 'required|max:200',
            'floor_number'                  => 'required|max:200',
            'area_name'                     => 'required|max:200',
            'pincode'                       => 'required|numeric|digits:6',
            'yearOfConstruction'            => 'required|numeric|digits:4',
            'community'                     => 'required|max:200', 
            'no_of_bed'                     => 'required|numeric|min:0|max:999',
            'facilities'                    => 'required|max:200',
            'facilitation_fees'             => 'required|numeric|min:0|max:100',
            'psf_rate'                      => 'required|numeric|min:0|max:999999999',
            'custodian'                     => 'required|max:200',
            'registration_fees'             => 'required|numeric|min:0|max:999999999',
            'total_cost_psf'                => 'required|numeric|min:0|max:999999999',
            'net_operating_yield_sale'      => 'required|numeric|min:0|max:100',
            'net_realizable_yield_sale'     => 'required|numeric|min:0|max:100',
            'net_realizable_yield_purchase' => 'required|numeric|min:0|max:100',
            'stamp_duty'                    => 'required|numeric|min:0|max:999999999',
            'sale_price'                    => 'required|numeric|min:0|max:999999999',
            'propertyDetailsHighlights'     => 'required', //Long text
            'token_name'                    => 'required|max:100',
            'token_symbol'                  => 'required|max:4',
            'token_value'                   => 'required|numeric|min:0|max:999999999',
            'token_supply'                  => 'required|numeric|min:0', //varchar
            'token_decimal'                 => 'required|numeric|digits_between:1,2',
            'ManagementTeamDescription'     =>  'required',
            'overallarea'                   =>  'required|numeric|min:0|max:999999999',
            'type_of_building'              =>  'required|max:200',
            'airport'                       =>  'required|max:200',
            'hospitals'                     =>  'required|max:200',
            'fire_services'                 =>  'required|max:200',
            'slums'                         =>  'required|max:200',
            'industrial'                    =>  'required|max:200',
            'railway_tracks'                =>  'required|max:200',
            'distance_fm_mainroad'          =>  'required|max:200',
            'build_config'                  =>  'required|max:200',
            'reception_hours'               =>  'required|max:200',
            'no_of_lifts'                   =>  'required|numeric|min:0|max:999999999',
            'capasity_of_lifts'             =>  'required|numeric|min:0|max:999999999',
            'make_of_lifts'                 =>  'required|max:200',
            'amenities_avilable'            =>  'required|max:200',
            'seismic_standard'              =>  'required|max:200',
            'structure_type'                =>  'required|max:200',
            'slabs_design'                  =>  'required|max:200',
            'floor_height'                  =>  'required|max:200',
            'food_court_seating'            =>  'required|numeric|min:0|max:999999999',
            'food_court_working_hours'      =>  'required|max:200',
            'management_operational_hours'  =>  'required|max:200',
            'bms_system'                    =>  'required|max:200',
            'high_side_equipments'          =>  'required|max:200',
            'security_surveillance'         =>  'required|max:200',
            'cctv_installation'             =>  'required|max:200',
            'demand_load'                   =>  'required|max:200',
            'load_calculation'              =>  'required|max:200',
            'transformer_feeding'           =>  'required|max:200',
            'ventilation_transformer'       =>  'required|max:200',
            'dg_power'                      =>  'required|max:200',
            'generators'                    =>  'required|max:200',
            'dg_kick_off_time'              =>  'required|max:200',
            'earthing_pits'                 =>  'required|max:200',
            'lift_ac'                       =>  'required|max:200',
            'facade_glass'                  =>  'required|max:200',
            'chillers_configuration'        =>  'required|max:200',
            'chiller_timing'                =>  'required|max:200',
            'water_supply'                  =>  'required|max:200',
            'floor_capacity'                =>  'required|max:200',
            'state_design_condition'        =>  'required|max:200',
            'ventilation_operational_hours' =>  'required|max:200',
            'floorplan'                     =>  'required|mimes:pdf|max:70000',
            'propertyLogo'                  =>  'required|mimes:jpeg,jpg,bmp,png|max:70000',
            'propertyVideo'                 =>  'required|mimes:mp4,mov|max:70000',
            'investor'                      =>  'required|mimes:pdf|max:70000',
            'titlereport'                   =>  'required|mimes:pdf|max:70000',
            'termsheet'                     =>  'required|mimes:pdf|max:70000',
            'due_diligence_report'          =>  'required|mimes:pdf|max:70000',
            'propertyUpdatesDoc'            =>  'required|mimes:pdf|max:70000',
            'propertyimages'                =>  'required|mimes:jpeg,jpg,bmp,png|max:70000',
            'token_image'                   =>  'required|mimes:jpeg,jpg,bmp,png|max:70000',
        ];
    }
}
