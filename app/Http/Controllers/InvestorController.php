<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\InvestorTypes;
use App\FundTypes;
use App\WorthStatus;

class InvestorController extends Controller
{
    public function index() {
    	$investor_types=InvestorTypes::get();
    	return view('admin.investor.index',compact('investor_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    
    public function create()
    {
        return view('admin.investor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'investor_type' => 'required',
        ]);

        try {

            $investor_type = new InvestorTypes;
            $investor_type->investor_type = $request->investor_type;            
            $investor_type->save();

            return back()->with('flash_success', 'Investor Type Added Successfully');

        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Something went wrong. Please try again');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       try {
            $investor_type = InvestorTypes::find($id);           

            return view('admin.investor.edit', compact('investor_type'));

        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Something went wrong. Please try again');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request);
        $this->validate($request, [
            'investor_type' => 'required',            
            'status' => 'required',            
        ]);

        try {

            $investor_type = InvestorTypes::find($id);
            $investor_type->investor_type = $request->investor_type;
            $investor_type->status = $request->status;
            $investor_type->save();
            
            return back()->with('flash_success', 'Investor Type Updated Successfully');

        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Oops something went wrong. Please try again');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


	// Type of Funds	
    public function indexfund() {
    	$fund_types=FundTypes::get();
    	return view('admin.fund.index',compact('fund_types'));
    }
        
    public function createfund()
    {
        return view('admin.fund.create');
    }
   
    public function storefund(Request $request)
    {
        $this->validate($request, [
            'fund_type' => 'required',
        ]);

        try {

            $fund_type = new FundTypes;
            $fund_type->fund_type = $request->fund_type;            
            $fund_type->save();

            return back()->with('flash_success', 'Fund Type Added Successfully');

        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Something went wrong. Please try again');
        }
    }

    public function editfund($id)
    {
       try {
            $fund_type = FundTypes::find($id);           

            return view('admin.fund.edit', compact('fund_type'));

        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Something went wrong. Please try again');
        }
    }
    
    public function updatefund(Request $request)
    {
        //dd($request);
        $this->validate($request, [
            'fund_type' => 'required',            
            'status' => 'required',            
        ]);

        try {

        	$id=$request->id;

            $fund_type = FundTypes::find($id);
            $fund_type->fund_type = $request->fund_type;
            $fund_type->status = $request->status;
            $fund_type->save();
            
            return back()->with('flash_success', 'Fund Type Updated Successfully');

        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Oops something went wrong. Please try again');
        }
    }

    //WorthStatus    
    public function indexworthstatus() {
    	$worthstatus=WorthStatus::get();
    	return view('admin.worthstatus.index',compact('worthstatus'));
    }
        
    public function createworthstatus()
    {
        return view('admin.worthstatus.create');
    }
   
    public function storeworthstatus(Request $request)
    {
        $this->validate($request, [
            'worth_status' => 'required',
        ]);

        try {

            $worthstatus = new WorthStatus;
            $worthstatus->worth_status = $request->worth_status;            
            $worthstatus->save();

            return back()->with('flash_success', 'Worth Status Added Successfully');

        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Something went wrong. Please try again');
        }
    }

    public function editworthstatus($id)
    {
       try {
            $worthstatus = WorthStatus::find($id);           

            return view('admin.worthstatus.edit', compact('worthstatus'));

        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Something went wrong. Please try again');
        }
    }
    
    public function updateworthstatus(Request $request)
    {
        //dd($request);
        $this->validate($request, [
            'worth_status' => 'required',            
            'status' => 'required',            
        ]);

        try {

        	$id=$request->id;

            $worthstatus = WorthStatus::find($id);
            $worthstatus->worth_status = $request->worth_status;
            $worthstatus->status = $request->status;
            $worthstatus->save();
            
            return back()->with('flash_success', 'Worth Status Updated Successfully');

        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Oops something went wrong. Please try again');
        }
    }


    


    

}