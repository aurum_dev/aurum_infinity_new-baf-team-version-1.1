<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use Setting;
use Auth;
use App\Admin;
use App\Support;
use App\UserTrasaction;
use App\User;
use App\WalletPassbook;
use App\KycDocument;
use App\Mail\UsdWithdraw;
use App\Mail\Kycmail;
use Mail;
use App\Http\Controllers\CommonController;
use App\Mail\KycApproval;
use App\AdminWallet;
use Storage;
use GuzzleHttp\Client;
use App\Http\Controllers\TransactionController;
use App\Mail\Adminbtcotpmail;
use Cache;
use App\Http\Requests\ValidateAdminSecretRequest;
use App\Mail\Userwirealert;
use App\Mail\Userwirealertfailed;
use App\Mail\KycDocRejected;
use App\Mail\CryptoWithdraw;
use App\AdminAddress;
use App\Property;
use App\Http\Controllers\simple_crypt;
use Crypt;
use App\KrakenResult;
use App\Coins;
use App\Privacy;
use App\Mail\AdminApprovalMail;

class AdminController extends Controller
{
    /**
     * Used to get Dashboard Details
     */
    public function dashboard() {
        try{
            $user = Auth::user();
            if(empty($user->eth_address)){
                $eth_address       = ((new NodeController)->eth_address($user->email))->getData();
                $user->eth_address = ($eth_address->success == true) ? $eth_address->response : null;            
                $user->save();
            }
            $wallet = json_decode($this->getETHbalance($user->eth_address),TRUE);
            $balance = $wallet['balance'];
            $title = "Dashboard";
            $coindetails = (new CommonController)->getCryptoPrices();
            return view('admin.home', compact('coindetails', 'user', 'balance','title'));
        }
        catch(Exception $e){
            return back()->with('flash_error', 'Unable to get Dashboard Details');
        }
    }

    /**
     * Get ETH Balance
     */
    public function getETHbalance($address)
    {
        try {

            if(empty($address)){
                return json_encode(['msg' => 'failed', 'balance' => 0]);
            }

            $client     = new Client();
            $headers    = [
                'Content-Type' => 'application/json',
            ];
            $body       = ["jsonrpc" => "2.0", "method" => "eth_getBalance", "params" => [$address, 'latest'], "id" => 1];
            $url        = env('INFURA_URL');
            $res        = $client->post($url, [
                'headers' => $headers,
                'body'    => json_encode($body),
            ]);
            $getbalance = json_decode($res->getBody(), true);
            if (isset($getbalance['result'])) {
                $balance = hexdec($getbalance['result']) / 1000000000000000000;
                return json_encode(['msg' => 'success', 'balance' => $balance]);
            } else {
                return json_encode(['msg' => 'failed', 'balance' => 0]);
            }
        } catch (\Throwable $th) {
            return json_encode(['msg' => 'failed', 'balance' => 0]);
        }
    }

    /**
     * Used to Check User KYC Documents
     */
    public function kycdoc($id)
    {
        try {
            $Doc = KycDocument::getDocuments($id);
            $user = User::getUser($id);
            return view('admin.user.document', compact('Doc','user'));
        } catch (Exception $e) {
            return back()->with('flash_error', trans('api.user.user_not_found'));
        }
    }

    /**
     * Used to Change User Status
     */
    public function userStatus($id, $status){
        try {
            if($status == 'approve'){
                $Kyc = KycDocument::getDocuments($id, 'APPROVED');
                if(count($Kyc) >= 1)
                    return back()->with('flash_error',trans('api.kyc_not_verified'));

                User::getUser($id)->update(['kyc' => 1]);
                return back()->with('flash_success', trans('api.approved'));
            }
            else{
                User::getUser($id)->update(['kyc' => 0]);
                return back()->with('flash_error',trans('api.disapproved'));
            }
        } catch (Exception $e) {
            return back()->with('flash_error',  trans('api.something_went_wrong'));
        }
    }

    public function userApprovalStatus($id, $status){
        try {
            if($status == 'approve'){
                User::getUser($id)->update(['approved' => 1]);

                $user = User::getUser($id);
                if($_SERVER['REMOTE_ADDR'] != '127.0.0.1')
                 Mail::to($user->email)->send(new AdminApprovalMail($user->name));

                return back()->with('flash_success', trans('api.approved'));
            }
            else{
                User::getUser($id)->update(['approved' => 0]);
                return back()->with('flash_error',trans('api.disapproved'));
            }
        } catch (Exception $e) {
            return back()->with('flash_error',  trans('api.something_went_wrong'));
        }
    }



    /**
     * Used to Check User Status
     */
    public function history()
    {
        try{

        	$History = UserTrasaction::orderBy('id','desc')->get();

        	return view('admin.history.index',compact('History'));

        }
        catch(Exception $e){
            return back()->with('flash_error', trans('api.something_went_wrong'));
        }
    }



    public function settings()
    {

        try{

            if(Auth::guard('admin')->user()->role == 1){
                return view('errors.404');
            }
           return view('admin.settings.settings');
        }
        catch(Exception $e){
          return back()->with('flash_error', trans('api.something_went_wrong'));
        }
    }

    public function settings_store(Request $request)
    {

        try{

            $this->validate($request,[
                'site_title' => 'required',
                'site_icon' => 'mimes:jpeg,jpg,bmp,png|max:70000',
                'site_logo' => 'mimes:jpeg,jpg,bmp,png|max:70000',
            ]);

            if($request->hasFile('site_icon')) {
                $site_icon = $request->site_icon->store('settings');
                Setting::set('site_icon', "app/public/".$site_icon);
            }

            if($request->hasFile('site_logo')) {
                $site_logo = $request->site_logo->store('settings');
                Setting::set('site_logo', "app/public/".$site_logo);
            }

            if($request->hasFile('site_email_logo')) {
                $site_email_logo = $request->site_email_logo->store('settings');
                Setting::set('site_email_logo', $site_email_logo);
            }

            Setting::set('site_title', $request->site_title);
            Setting::set('site_copyright', $request->site_copyright);
            Setting::set('kyc_approval', $request->kyc_approval == 'on' ? 1 : 0 );


            Setting::set('ripple_address', $request->ripple_address);
            Setting::set('support_mail', $request->support_mail);
            Setting::set('enquiry_mail', $request->enquiry_mail);
            Setting::set('number', $request->number);
            Setting::set('instagram', $request->instagram);
            Setting::set('twitter', $request->twitter);
            Setting::set('facebook', $request->facebook);
            Setting::set('default_currency', $request->default_currency);
            Setting::set('purchase_commission', $request->purchase_commission);
            Setting::set('faq', $request->faq);
            Setting::save();

            return back()->with('flash_success',trans('api.setting_status'));

        }
        catch(Exception $e){
            return back()->with('flash_error', trans('api.something_went_wrong'));
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function settings_payment()
    {
        try{

            if(Auth::guard('admin')->user()->role == 1){
                return view('errors.404');
            }

            return view('admin.payment.settings');

        }
        catch(Exception $e){
            return back()->with('flash_error', trans('api.something_went_wrong'));
        }
    }

    /**
     * Save payment related settings.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function settings_payment_store(Request $request)
    {
        $this->validate($request, [
            'CARD' => 'in:on',
            'CASH' => 'in:on',
            'PAYPAL' => 'in:on',

            'stripe_secret_key' => 'required_if:CARD,on|max:255',
            'stripe_publishable_key' => 'required_if:CARD,on|max:255',
            'currency' => 'required',
            'bit_coin_price' => 'required',
            'ethereum_price' => 'required',
            'ripple_price' => 'required',
        ]);

        try{

            if(Auth::guard('admin')->user()->role == 1){
                return view('errors.404');
            }

            Setting::set('CARD', $request->has('CARD') ? 1 : 0 );
            Setting::set('CASH', $request->has('CASH') ? 1 : 0 );
            Setting::set('PAYPAL', $request->has('PAYPAL') ? 1 : 0 );

            Setting::set('stripe_secret_key', $request->stripe_secret_key ? : '');
            Setting::set('stripe_publishable_key', $request->stripe_publishable_key ? : '');
            Setting::set('increase_percentage', $request->increase_percentage ? : '');
            Setting::set('admin_gaslimit', $request->admin_gaslimit);
            Setting::set('user_gaslimit', $request->user_gaslimit);
            Setting::set('gas', $request->gas);
            Setting::set('eth_alert', $request->eth_alert);

            Setting::set('currency', $request->currency);
            Setting::set('referral', $request->referral);
            Setting::set('withdraw_time', $request->withdraw_time);
            Setting::set('withdraw_comission', $request->withdraw_comission);

            Setting::set('account_name', $request->account_name);
            Setting::set('bank_address', $request->bank_address);
            Setting::set('bank_number', $request->bank_number);
            Setting::set('account_number', $request->account_number);
            Setting::set('sort_code', $request->sort_code);
            Setting::set('iban', $request->iban);
            Setting::set('swift_code', $request->swift_code);
            Setting::set('routing_code', $request->routing_code);
            Setting::set('bank_name', $request->bank_name);
            Setting::set('reply_message', $request->reply_message);

            Setting::set('lgc_commission', $request->lgc_commission);
            Setting::set('btc_commission', $request->btc_commission);
            Setting::set('eth_commission', $request->eth_commission);
            Setting::set('xrp_commission', $request->xrp_commission);
            Setting::set('bch_commission', $request->bch_commission);
            Setting::set('ltc_commission', $request->ltc_commission);

            Setting::set('lgc_withdrawl', $request->lgc_withdrawl);
            Setting::set('btc_withdrawl', $request->btc_withdrawl);
            Setting::set('eth_withdrawl', $request->eth_withdrawl);
            Setting::set('xrp_withdrawl', $request->xrp_withdrawl);
            Setting::set('bch_withdrawl', $request->bch_withdrawl);
            Setting::set('ltc_withdrawl', $request->ltc_withdrawl);
            Setting::set('bit_coin_price', $request->bit_coin_price);
            Setting::set('ethereum_price', $request->ethereum_price);
            Setting::set('ripple_price', $request->ripple_price);
            Setting::set('WIRE', $request->WIRE == 'on' ? 1 : 0 );
            Setting::set('FIAT', $request->FIAT == 'on' ? 1 : 0 );
            Setting::set('BTC', $request->BTC == 'on' ? 1 : 0 );
            Setting::set('ETH', $request->ETH == 'on' ? 1 : 0 );
            Setting::set('XRP', $request->XRP == 'on' ? 1 : 0 );
            Setting::set('BCH', $request->BCH == 'on' ? 1 : 0 );
            Setting::set('LTC', $request->LTC == 'on' ? 1 : 0 );

            Setting::set('eth_api_key', $request->eth_api_key);



            Setting::save();

            return back()->with('flash_success',trans('api.setting_status'));
        }
        catch(Exception $e){
            return back()->with('flash_error', trans('api.something_went_wrong'));
        }
    }

    public function historySuccess($id)
    {
        try{

            $History = WithdrawHistory::findOrFail($id);

            $History->status = "SUCCESS";
            $History->save();

            $User = User::where('id',$History->user_id)->first();

            $maildata= $History;
            $email = $User->email;

            Mail::to($email)->send(new UsdWithdraw($maildata));

            return back()->with('flash_success',trans('api.success_status'));

        }
        catch(Exception $e){
            return back()->with('flash_error', trans('api.something_went_wrong'));
        }
    }

    public function historyFailed($id)
    {
        try{

            $History = WithdrawHistory::findOrFail($id);

            $History->status = "FAILED";
            $History->save();

            $User = User::where('id',$History->user_id)->first();
            $User->wallet += $History->amount;
            $User->save();

            $User = User::where('id',$History->user_id)->first();

            $maildata= $History;
            $email = $User->email;

            Mail::to($email)->send(new UsdWithdraw($maildata));


            WalletPassbook::create([
                'user_id' => $History->user_id,
                'amount' => $History->amount,
                'status' => 'CREDITED',
                'via' => "WITHDRAW - FAILED",
            ]);



            return back()->with('flash_success',trans('api.success_status'));
        }
        catch(Exception $e){
            return back()->with('flash_error', trans('api.something_went_wrong'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        try{
            return view('admin.account.profile');
        }
        catch(Exception $e){
            return back()->with('flash_error', trans('api.something_went_wrong'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function profile_update(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|max:255',
            'email' => 'required|max:255|email|unique:admins,email,'.Auth::guard('admin')->user()->id,
            'picture' => 'mimes:jpeg,jpg,bmp,png|max:70000',
        ]);

        try{
            $admin = Auth::guard('admin')->user();
            $admin->name = $request->name;
            $admin->email = $request->email;

            if($request->hasFile('picture')){
                $admin->picture = $request->picture->store('admin/profile');
            }
            $admin->save();

            return redirect()->back()->with('flash_success','Profile Updated');
        }

        catch (Exception $e) {
           return back()->with('flash_error', trans('api.something_went_wrong'));
       }

   }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function password()
    {
        try{

            if(Auth::guard('admin')->user()->role == 1){
                return view('errors.404');
            }

            return view('admin.account.change-password');
        }
        catch(Exception $e){
            return back()->with('flash_error', trans('api.something_went_wrong'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function password_update(Request $request)
    {

        $this->validate($request,[
            'old_password' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);

        try {

         $Admin = Admin::find(Auth::guard('admin')->user()->id);
         
         if(password_verify($request->old_password, $Admin->password))
         {
            $Admin->password = bcrypt($request->password);
            $Admin->save();

            return back()->with('flash_success','Password Updated');
        }else{
            return back()->with('flash_error','Please enter valid old password');
        }
        } catch (Exception $e) {
           return back()->with('flash_error', trans('api.something_went_wrong'));
       }
    }

    public function privacy(){
        try{

            if(Auth::guard('admin')->user()->role == 1){
                return view('errors.404');
            }
$privacy = Privacy::where('id','1')->first();
            return view('admin.pages.privacy',compact('privacy'))
            ->with('title',"Privacy Page")
            ->with('page', "privacy");

        }
        catch(Exception $e){
            return back()->with('flash_error', trans('api.something_went_wrong'));
        }
    }
    public function adminwallet(){
        try{

            if(Auth::guard('admin')->user()->role == 1){
                return view('errors.404');
            }
       
$contracts = AdminWallet::orderBy('wallet_id','desc')->join('property', 'property.id', '=', 'property_id')->get();

       // return view('admin.tokenizer.request',compact('contracts'));
            return view('admin.pages.adminwallet',compact('contracts'))
            ->with('title',"Admin Wallet")
            ->with('page', "Wallet");

        }
        catch(Exception $e){
            return back()->with('flash_error', trans('api.something_went_wrong'));
        }
    }
    public function walletdetails($id){       
        try{
            $contracts = AdminWallet::where('wallet_id',$id)->get();        
         
            $property= Property::where('id',$contracts[0]['property_id'])->get();
            $issuer= User::where('id',$contracts[0]['issuer_id'])->get();          
            $investor= User::where('id',$contracts[0]['investor_id'])->get();  
           return view('admin.pages.wallet_detail',compact('contracts','property','issuer','investor'));
           
        }
        catch (Exception $e) { dd($e->getMessage());
          //  return back()->with('flash_error','Something Went Wrong!');
        }
    }


    public function about(){

        try{

            if(Auth::guard('admin')->user()->role == 1){
                    return view('errors.404');
                }

            return view('admin.pages.aboutus')
            ->with('title',"About Page")
            ->with('page', "about");

        }
        catch(Exception $e){
            return back()->with('flash_error', trans('api.something_went_wrong'));
        }
    }


    public function terms(){

        try{

            if(Auth::guard('admin')->user()->role == 1){
                    return view('errors.404');
                }

            return view('admin.pages.terms')
            ->with('title',"Terms Page")
            ->with('page', "terms");

        }
        catch(Exception $e){
            return back()->with('flash_error', trans('api.something_went_wrong'));
        }
    }

    public function termspages(Request $request){
        try {
            Setting::set('page_terms', $request->content);
            Setting::save();
            return back()->with('flash_success', 'Terms and Condition has been updated');
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to update terms and condition');
        }
    }
     public function privacypages(Request $request){
        try {
            
            $id=1;
             Privacy::where('id',$id)->update(['privacycontent' => $request->content]);
          //  Setting::set('page_terms', $request->content);
          //  Setting::save();
            return back()->with('flash_success', 'Privacy Policy has been updated successfully');
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to update Privacy Policy');
        }
    }


public function support(){

    try{

        $support = Support::orderBy('id','desc')->get();

        return view('admin.support.index',compact('support'));

    }
    catch(Exception $e){
        return back()->with('flash_error', trans('api.something_went_wrong'));
    }
}



    public function sendmailforkycrejection(){

        $User = User::get();

        foreach($User as $user){
            $doc = KycDocument::where('user_id',$user['id'])
                                ->where('status','REJECTED')
                                ->where( 'updated_at', '>', Carbon::now()->subDays(3))
                                ->first();

            if($doc){
                $email = $user['email'];
                $maildata = $user;
                Mail::to($email)->send(new Kycmail($maildata));
            }
        }

    }

    public function getValidateToken(){

        try{

            $url_param=$_GET['route_url'];
            $user=Auth::user();
            if ($user->google2fa_secret) {

                //$request->session()->put('2fa:admin:id', $user->id);
                session(['2fa:admin:id' => $user->id]);
                //\Session::set('2fa:admin:id', $user->id);

                if (session('2fa:admin:id')) {
                    return view('admin.2fa.urlform',compact('url_param'));
                }
            }
            else{
                $url=str_replace("'", "", $url_param);
                return redirect()->route($url);
                //return redirect()->route($url_param);
           }
            //return view('admin.home');

        }
        catch(Exception $e){
            return back()->with('flash_error', trans('api.something_went_wrong'));
        }
    }

    // kycmail
    public function kycmail(){
        (new AdminController)->sendmailforkycrejection();

        return back()->with('flash_success','Mail Alert with send to all users whose document is rejected and not updated again more than 72 hours');
    }



}
