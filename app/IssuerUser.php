<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IssuerUser extends Model
{
    protected $table = 'bankdetail';
    
    protected $fillable = [
        'user_id',
        'accountholdername',
        'accno',
        'bankname',
        'ifsccode',
    ];
}
