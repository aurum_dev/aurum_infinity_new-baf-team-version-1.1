<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $table='property';
    protected $fillable = [
        'user_id', 
        'propertyName', 
        'propertyState',
        'propertyCity', 
        'propertyDistrict', 
        'propertyType',
        'tokenName',
        'tokenSymbol',
        'tokenValue',
        'tokenSupply',
        'tokenDecimal',
        'tokenLogo',
        'Divident',
        'minimumInvest',
        'propertySize',
        'targetIrr',
        'averageYield',
        'propertyDesc',
        'Proponent',
        'Onmap',
        'Address',
       'propertyStatus',
        'buildYear',
        'reraNumber',
       'propertyManager',
        'Custodian',
        'propertyAmenity',
        'propertyTechSpecs',
        'costOfPremise',
        'stampDutyReg',
        'acquisitionFee',
        'otherCharges',
        'legalFees',    
        'maintenanceReserve',  
        'adminExpenses',
        'monthlyOpex',
        'monthlyPropertyTax',
        'regulatoryCharges',
        'totalAcquisitionCost',
        'carpetArea',
        'builtUpArea',
        'monthlyRent',
        'leasedPeriod',
        'securityDeposit',
        'escalation',
        'tenants',
        'fittedOut',
        'monthlyMaintenance',
        'leaseChart', 
        'grossYield',
        'expectedIr',
        'tokenLockinPeriod',
        'aurumFacilationFees',
        'performanceFees',
        'propertyInvestmentDeck',
         'propertytitleReport',
         'propertyDueDiligence',
         'propertyMicroMarket',
         'propertyMgmtAgrmt',
         'propertyValReport',
         'galleryImage',
         'status',
         'tokenStatus',
       /*'leased_period', 
        'rera_number', 
        'dev_name',
        'property_age',
        'tenant_name',
        'rent_escalation',
        'notice_period',
        'carpet_area', 
        'built_up_area',
        'gross_yield',
        'annual_rent',
        'totalDealSize',
        'expectedIrr',
        'initialInvestment',
        'propertyEquityMultiple',
        'holdingPeriod',
        'total_sft',
        'dividend'*/
    ];
    // protected $fillable = [
    //    'investor','propertyUpdatesDoc','termsheet','titlereport','fundedMembers'
    // ];
 public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
   /* public function propertyImages()
    {
        return $this->hasMany('App\PropertyImage', 'property_id', 'id');
    }

    public function propertyDetails(){
        return $this->belongsTo('App\PropertyDetail', 'id', 'property_id');
    }
    
    public function propertyDocuments(){
        return $this->belongsTo('App\PropertyDocument', 'id', 'property_id');
    }
    
    public function propertyTeams(){
        return $this->hasMany('App\PropertyTeam', 'property_id', 'id');
    }

    public function propertyInformation(){
        return $this->belongsTo('App\PropertyInformation', 'id', 'property_id');
    }

    public function propertyTokens(){
        return $this->belongsTo('App\PropertyToken', 'id', 'property_id');
    }*/

    // public function propertyBulider()
    // {
    //     return $this->hasMany('App\PropertyBuilder', 'property_id', 'id');
    // }

    // public function propertyComparable()
    // {
    //     return $this->hasMany('App\PropertyComparable', 'property_id', 'id');
    // }

    // public function propertyLandmark()
    // {
    //     return $this->hasMany('App\PropertyLandmark', 'property_id', 'id');
    // }

    // public function issuerToken()
    // {
    //     return $this->belongsTo('App\IssuerTokenRequest', 'id', 'property_id');
    // }

  /*  public function members()
    {
        return $this->hasMany('App\ManagementMembers', 'property_id', 'id');
    }

    // /**
    //  * Used to Get Token Details
    //  */
  /*  public function userContract()
    {
        return $this->belongsTo('App\UserContract', 'id', 'property_id');
    }*/

    // /**
    //  * Used to Get Property
    //  * {$feature} - Used to get Feature Property
    //  * {$id} - Used to get Particular Property Details
    //  */
    // public function getProperty($feature = 0, $id = 0, $user_id = 0, $token_type = null)
    // {
    //     $query = Property::where('status', '<>', 'pending')->orderBy('created_at', 'desc');

    //     if ($feature == 1) {
    //         $query->where('feature', 1);
    //     }

    //     if ($user_id > 0) {
    //         $query->where('user_id', $user_id);
    //     }

    //     if (isset($token_type)) {
    //         $query->where('token_type', $token_type);
    //         if ($token_type == 1) {
    //             $query->orWhereNull('token_type');
    //         }
    //     }

    //     if ($id != 0)
    //         $property = $query->find($id);
    //     else
    //         $property = $query->get();

    //     return $property;
    // }
}
