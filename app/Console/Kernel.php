<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Schema;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\ETHTransactions::class,
        \App\Console\Commands\BTCConfirmation::class,
        \App\Console\Commands\BTCTransactions::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->command('btc:transactions')
                 ->everyMinute()->withoutOverlapping(5)->runInBackground();

        $schedule->command('btc:checkstatus')
                 ->everyMinute()->withoutOverlapping(5)->runInBackground();

        $schedule->command('ethtransaction:apistatus')
                 ->everyFiveMinutes()->withoutOverlapping(10)->runInBackground();

        $schedule->command('token:investortransfer')
                 ->everyMinute()->withoutOverlapping(5)->runInBackground();

        // $schedule->command('eth:transactions')
        //         ->everyFiveMinutes(); 

        Schema::defaultStringLength(191);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
