<?php

namespace App\Console\Commands;

use App\User;
use App\UserContract;
use App\UserTokenTransaction;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class TokenTransferToInvestor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'token:investortransfer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Token transfer to investor';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $user_tokens = UserTokenTransaction::where('status', 2)->get();
            foreach ($user_tokens as $token) {
                $user = User::where('id', $token->user_id)->first();
                if (!$user) {
                    continue;
                }

                $user_contract = (new UserContract)->getUserContract($token->user_contract_id);
                if (!$user_contract) {
                    continue;
                }

                $issuer_user = User::where('id', $user_contract->user_id)->first();
                if (!$issuer_user) {
                    continue;
                }

                $client  = new Client();
                $headers = ['Content-Type' => 'application/x-www-form-urlencoded',];
                $url     = 'http://localhost:1337/getkey';
                $res     = $client->post($url, [
                    'headers'     => $headers,
                    'form_params' => [
                        'add'    => $issuer_user->eth_address,
                        'string' => $issuer_user->email,
                    ],
                ]);
                $res     = json_decode($res->getBody(), true);

                if (isset($res['status']) && $res['status'] == true) {
                    $eth_pvt_key = $res['key'];

                    $url     = 'http://localhost:3000/transfer';
                    $client  = new Client();
                    $headers = ['Content-Type' => 'application/json',];
                    $body    = [
                        'contract_address' => $user_contract->contract_address,
                        'to'               => $user->eth_address,
                        'amount'           => $token->total_token,
                        'senderAddress'    => $issuer_user->eth_address,
                        'senderPrivateKey' => $eth_pvt_key,
                        'decimal'          => $user_contract->decimal,
                    ];
                    $res     = $client->post($url, [
                        'headers' => $headers,
                        'body'    => json_encode($body),
                    ]);

                    $details = json_decode($res->getBody(), true);
                    logger()->info($details);

                    if (isset($details['status']) && $details['status'] == 'success') {
                        $token->token_txn_hash = $details['txHash'];
                        $token->status         = 1;
                        $token->save();
                    }
                }
            }

        } catch (\Exception $e) {
            logger()->info($e->getMessage());
            DB::connection()->disconnect();
        } finally {
            DB::connection()->disconnect();
        }
    }
}
