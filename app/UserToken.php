<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserToken extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function usercontract()
    {
        return $this->belongsTo('App\UserContract','user_contract_id');
    }

    /**
	 * Used to Get UserContract Details
	 */
	public static function getUserToken($id = 0){
        $sql = UserToken::orderBy('created_at', 'desc');
        if($id !=0)
            $sql->where('user_contract_id', $id);
		
        return $sql;
	}
}
