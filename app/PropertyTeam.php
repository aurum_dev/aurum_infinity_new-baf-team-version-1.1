<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyTeam extends Model
{
    protected $fillable = [
        'property_id',
        'user_id',
        'name',
        'position',
        'image',
        'description'
    ];
}
