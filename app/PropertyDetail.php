<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyDetail extends Model
{
    protected $fillable = [
        'property_id',
        'user_id',
        'propertyOverview',
        'propertyLocationOverview',
        'propertyVideo',
        'geo_location',
        'street_address',
        'floor_number',
        'area_name',
        'pincode',
        'yearOfConstruction',
        'community',
        'no_of_bed',
        'facilities',
        'facilitation_fees',
        'psf_rate',
        'custodian',
        'registration_fees',
        'total_cost_psf',
        'net_operating_yield_sale',
        'net_realizable_yield_sale',
        'net_realizable_yield_purchase',
        'stamp_duty',
        'sale_price',
        'propertyDetailsHighlights',
        //'propertyimages',
        'floorplan',
        'ManagementTeamDescription'
    ];
}
